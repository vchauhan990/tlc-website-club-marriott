package com.tlc.web.platform.core.models.dynamic;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.OutletModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;

/**
 * 
 * Sling Implementation of Dynamic Experiences Card Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class DynamicExperiencesCardModel {

	private static final Logger log = LoggerFactory.getLogger(DynamicExperiencesCardModel.class);

	@Inject
	SlingHttpServletRequest request;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	Page currentPage;

	@Inject
	PromotionalContentFetchService promotionalContentFetchService;

	LinkedHashMap<String, List<OutletModel>> expOutletMap;

	@PostConstruct
	public void init() {

		log.info("inside init of DynamicExperiencesCardModel");
		try {
			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo-12 :: {} ", requestPathInfo.getSuffix());
			String pagePath = currentPage.getParent().getPath();
			ValueMap vm = currentPage.getContentResource().getValueMap();
			
			String cityPath = vm.get("cityPath", String.class);
			String[] segments = requestPathInfo.getSuffix().split("/");
			String propertyName = segments[segments.length - 1];
			String city = segments[segments.length - 2];

			expOutletMap = promotionalContentFetchService.getExperiencesOutletElements(city, propertyName, cityPath,
					pagePath);
			log.debug("expOutletMap keys :: {}", expOutletMap.keySet());
		
		} catch (Exception e) {
			log.error("exception in DynamicExperiencesCardModel ::{}", e);
		}
	}

	public Map<String, List<OutletModel>> expOutletMap() {
		return expOutletMap;
	}

}