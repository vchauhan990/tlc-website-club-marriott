package com.tlc.web.platform.core.models;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.PageVO;

/**
 * 
 * Sling Implementation of Benefits Listing Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class)
public class BenefitsListingModel {
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(BenefitsListingModel.class);

	@Self
	@Inject
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	@Optional
	private String ctaText;

	@Inject
	@Optional
	@Default(values = "{}")
	private String[] benefitsListingItems;

	@Inject
	@Optional
	@Default(values = "/content/tlc/home/hotels")
	private String hotelStartPagePath;

	private List<PageVO> hotelPageList;
	PageVO ls = null;

	@PostConstruct
	public void init() {

		try {
			Gson gson = new Gson();
			hotelPageList = new LinkedList<>();

			if (StringUtils.isNotBlank(hotelStartPagePath)) {

				Resource hotelPageResource = resourceResolver.getResource(hotelStartPagePath);
				log.debug("hotelStartPagePath" + hotelStartPagePath);

				if ((hotelPageResource != null) && (hotelPageResource instanceof Resource)) {
					Page hotelpage = hotelPageResource.adaptTo(Page.class);
					if (hotelpage != null) {
						Iterable<Resource> hotelpageResources = hotelPageResource.getChildren();
						for (Resource hotelpageResource : hotelpageResources) {

							if (hotelpageResource != null
									&& !hotelpageResource.getName().equalsIgnoreCase("jcr:content")) {

								Page hotelResourcePage = hotelpageResource.adaptTo(Page.class);

								if (hotelResourcePage != null) {
									ValueMap pageProperties = hotelResourcePage.getProperties();
									ls = new PageVO();
									ls.setTitle(pageProperties.get("jcr:title", String.class));
									ls.setDescription(pageProperties.get("jcr:description",
											pageProperties.get("description", String.class)));
									ls.setPageUrl(hotelResourcePage.getPath());

								}
								hotelPageList.add(ls);
								log.debug("hotelPageList :: {} ", gson.toJson(hotelPageList));
							}
						}
					}
				}

			}

		} catch (Exception e) {
			log.error("Exception in Benefits Listing model" , e);
		}

	}

	public List<PageVO> getHotelPageList() {
		return hotelPageList;
	}
}
