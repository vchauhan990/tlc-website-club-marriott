package com.tlc.web.platform.core.servlets;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.OfferEventsModel;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;

@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "FeaturedEvents servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/featuredEvents"
		})
public class FeaturedEventsServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
		private static final Logger log = LoggerFactory.getLogger(FeaturedEventsServlet.class);
		
		@Reference
		PromotionalContentFetchService promotionalContentService;
		
		private List<OfferEventsModel> featuredEventsList = null;
		
		@Activate
		protected void activate() {
			log.debug("Featured Events Servlet activated!");
		}

		@Override
		protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
				throws ServletException, IOException {
			
	       PrintWriter out = response.getWriter();
		
			try {
				
				JSONObject js=new JSONObject();
				
				String outletType =null;
				String cityName = request.getParameter("city");
				String city = cityName.toLowerCase();
				String offersEventPath = request.getParameter("resourcePath");
				String pagePath = request.getParameter("pagePath");
				log.debug("signatureCollectionPath :: {} ",offersEventPath);
				log.debug("pagePath :: {} ",pagePath);
				featuredEventsList = promotionalContentService.getOffersEventsList("featuredEvents",city,null,null,null,offersEventPath,pagePath,null);
			
				js.put("data", featuredEventsList);
				js.put("status", "200");
				
				out.print(js);
			
			} catch (NullPointerException ne) {
				log.debug("NullPointerException in PromotionContentServlet :: {}", ne);
			} catch (JSONException jse) {
				log.debug("JSONException in PromotionContentServlet :: {}", jse);
			} catch(Exception e) {
				log.debug("Exception in FeaturedEventsServlet :: {}" , e);
			}
		}

}
