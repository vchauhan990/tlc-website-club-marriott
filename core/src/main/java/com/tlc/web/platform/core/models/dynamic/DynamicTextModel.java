package com.tlc.web.platform.core.models.dynamic;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Dynamic Text Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},defaultInjectionStrategy = OPTIONAL)
public class DynamicTextModel {

	private static final Logger log = LoggerFactory.getLogger(DynamicTextModel.class);

	@Inject
	PromotionalContentFetchService promoContentFetchService ;
	
	@Inject
	SlingHttpServletRequest request;
	 
	@Inject
	Page currentPage;
	@Inject
	RequestPathInfo requestPathInfo;
	
	List<DetailPageLayout1Model> promoContentList = null;
		
	@Inject
	SearchService searchservice;
	
	int promoContentListSize ;
	
	@PostConstruct
	public void init() {
		
		requestPathInfo = request.getRequestPathInfo();
		log.debug("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());
		String[] segments = requestPathInfo.getSuffix().split("/");
		
		try {
			String promotionType = currentPage.getName() ;
			log.info("promotionType :: {}" , promotionType);
			String promotionName = segments[segments.length - 1];

			ValueMap vm = currentPage.getContentResource().getValueMap();
			String uniqueExperiencePath = vm.get("uniqueExperiencePath", String.class);
			String signatureCollectionPath = vm.get("signatureCollectionPath", String.class);

			if(StringUtils.isNotBlank(promotionType) && StringUtils.equals(promotionType,  tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE)) {
				
				promoContentList = promoContentFetchService.getUniqueExperiencesContent(promotionName ,  tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE,uniqueExperiencePath );
				promoContentListSize = promoContentList.size();
				log.debug(" promoContentList size::{}" , promoContentListSize);
				
			}else if(StringUtils.isNotBlank(promotionType) && StringUtils.equals(promotionType, tlcConstants.SIGNATURE_CATALOGUE)) {
				
				promoContentList = promoContentFetchService.getUniqueExperiencesContent(promotionName, tlcConstants.SIGNATURE_CATALOGUE,signatureCollectionPath);
				
				promoContentListSize = promoContentList.size();
				log.debug(" promoContentList size::{}" , promoContentListSize);
			}	
		}catch(Exception e){
			log.error("exception in Dynamic Text Model ::{}", e );
		}
	}

	public List<DetailPageLayout1Model> getPromoContentList() {
		return promoContentList;
	}

	public int getPromoContentListSize() {
		return promoContentListSize;
	}
	
}
