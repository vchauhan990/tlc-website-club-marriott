package com.tlc.web.platform.core.vo;

import com.tlc.web.platform.core.util.LinkUtil;

public class PropertyVO {

	private String outletType;
	private String descriptionSmall;
	private String propertyName;
	private String descriptionLarge;
	private String bgImage;	
	private String bgImageAltText;	
	private String propertyPageUrl;
	private String webfacade;
	
	public String getPropertyPageUrl() {
		return LinkUtil.getFormattedURL(propertyPageUrl);
	}

	public void setPropertyPageUrl(String propertyPageUrl) {
		this.propertyPageUrl = propertyPageUrl;
	}

	public String getBgImageAltText() {
		return bgImageAltText;
	}

	public void setBgImageAltText(String bgImageAltText) {
		this.bgImageAltText = bgImageAltText;
	}

	public String getBgImage() {
		return bgImage;
	}

	public void setBgImage(String bgImage) {
		this.bgImage = bgImage;
	}

	public String getDescriptionSmall() {
		return descriptionSmall;
	}

	public void setDescriptionSmall(String descriptionSmall) {
		this.descriptionSmall = descriptionSmall;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDescriptionLarge() {
		return descriptionLarge;
	}

	public void setDescriptionLarge(String descriptionLarge) {
		this.descriptionLarge = descriptionLarge;
	}

	public String getOutletType() {
		return outletType;
	}

	public void setOutletType(String outletType) {
		this.outletType = outletType;
	}

	public String getWebfacade() {
		return webfacade;
	}

	public void setWebfacade(String webfacade) {
		this.webfacade = webfacade;
	}
	

	

}
