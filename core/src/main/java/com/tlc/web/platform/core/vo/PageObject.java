package com.tlc.web.platform.core.vo;

import com.tlc.web.platform.core.util.LinkUtil;

public class PageObject {

	private String pageName;
	private String pagePath;
	private String pageUrl;
	private String title;
	private String subtitle ;
	private String heading;
	private String altText;
	private String description;
	private String thumbnailImagePath;
	private String thumbnailImage;
	private String thumbnailImageAltText;
	private String[] tags;
	private String Date;
	private String category;
	private String ctaText ;

	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPagePath() {
		return pagePath;
	}
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	public String getPageUrl() {
		return LinkUtil.getFormattedURL(pageUrl);
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = LinkUtil.getFormattedURL(pageUrl);
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getThumbnailImagePath() {
		return thumbnailImagePath;
	}
	public void setThumbnailImagePath(String thumbnailImagePath) {
		this.thumbnailImagePath = thumbnailImagePath;
	}
	public String getThumbnailImage() {
		return thumbnailImage;
	}
	public void setThumbnailImage(String thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}
	public String getThumbnailImageAltText() {
		return thumbnailImageAltText;
	}
	public void setThumbnailImageAltText(String thumbnailImageAltText) {
		this.thumbnailImageAltText = thumbnailImageAltText;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCtaText() {
		return ctaText;
	}
	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
}
