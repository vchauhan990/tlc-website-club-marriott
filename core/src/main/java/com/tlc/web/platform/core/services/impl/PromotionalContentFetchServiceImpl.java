package com.tlc.web.platform.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.DetailPageLayout2Model;
import com.aem.tlc.core.models.DetailPageLayout3Model;
import com.aem.tlc.core.models.DetailPageLayout4Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.aem.tlc.core.models.OutletModel;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.Replicator;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.tlc.web.platform.core.services.MemberBenefitsListingService;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PageVO;

import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * Service Implementation of Promotional Content Fetch Service
 * 
 * @author DWAO
 *
 */

@Component(immediate = true, service = { PromotionalContentFetchService.class }, enabled = true, property = {
		Constants.SERVICE_DESCRIPTION + "= Promotional Content Fetch Service Implementation" })
public class PromotionalContentFetchServiceImpl implements PromotionalContentFetchService {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	Session session;

	@Reference
	Replicator replicationService;

	List<DetailPageLayout1Model> expList = null;
	public List<SignatureCollectionVO> uniqueExpList = null;

	@Reference
	SearchService searchservice;

	@Reference
	MemberBenefitsListingService memberBenefitsListingService;

	PageVO pageVo = null;

	Map<String, String> cityHotelNameList;

	HashMap<String, Object> outletTypeMap = null;

	List<OfferEventsModel> offersEventsList;

	List<OfferEventsModel> featuredOffersWithCityList;

	int countD1 = 0;
	int countD2 = 0;

	private static final Logger log = LoggerFactory.getLogger(PromotionalContentFetchServiceImpl.class);

	@Activate
	protected final void activate(final Map<Object, Object> config) {
		log.debug("Activated Promotional Content Fetch Service");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "contentJsonReader");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			log.debug("Resolver :: {}", resourceResolver);
			session = resourceResolver.adaptTo(Session.class);
			log.debug("Got Resource Resolver and Session for PromotionalContentFetchServiceImpl :: {} ",
					session.getUserID());
		} catch (LoginException le) {
			log.error("LoginExcepion in activate method of PromotionalContentFetchServiceImpl :: {} ", le.getMessage());
		} catch (Exception e) {
			log.error("Excepion in activate method of PromotionalContentFetchServiceImpl :: {}", e.getMessage());
		}

	}

	@Deactivate
	protected void deactivate(ComponentContext ctx) {
		session.logout();
		log.debug("Deactivated PromotionalContentFetchServiceImpl");
	}

	@Override
	public List<SignatureCollectionVO> getExperiencesbyTag(String city, String property, String modeType,
			String pagePath, String membershipTypePath) {
		Gson gson = new Gson();
		try {

			uniqueExpList = new LinkedList<SignatureCollectionVO>();
			Iterator<Page> childrenPages = null;
			String cityHotel = null;

			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page rootPage = pageManager.getPage(membershipTypePath);
				if (rootPage instanceof Page) {
					childrenPages = rootPage.listChildren();

					while (childrenPages.hasNext()) {
						Page childPage = childrenPages.next();

						String childPageName = childPage.getName();
						log.info("childPageName is " + childPageName);
						// ExperiencePage has path of all children
						Resource expJCr = childPage.getContentResource();
						SignatureCollectionVO signatureCollectionVO = new SignatureCollectionVO();
						log.debug("activated");
						if (expJCr instanceof Resource) {
							TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
							if (tagManager != null) {
								Tag[] tags = tagManager.getTags(expJCr);

								if (tags.length != 0) {
									
									switch (modeType) {
									case "get-experiences-by-city":
										if (StringUtils.isNotBlank(city)) {

											cityHotel = tlcConstants.CITY_TAG + tlcConstants.SLASH + city;
											log.info("cityHotwl :: {} ", cityHotel);

										}
										break; // optional
									case "get-experiences-by-city-property":
										if (StringUtils.isNotBlank(city) && StringUtils.isNotBlank(property)) {
											cityHotel = tlcConstants.CITY_TAG + tlcConstants.SLASH + city
													+ tlcConstants.SLASH + property;
										}
										break; // optional

									default:
										log.debug("switch case default");
									}
									

									for (Tag tag : tags) {
										log.info("tag.getTagID() :: {}" , tag.getTagID());
										log.info("cityHotel :: {}" , cityHotel);
										
										/* unique experiences from property detail page*/
										if (tag.getTagID().equals(cityHotel)) {
										
											Iterable<Resource> dataResources = expJCr.getChild(expJCr.getPath()
													+ tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
													.getChildren();

											for (Resource dataResource : dataResources) {

												if (dataResource instanceof Resource) {

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_1)) {

														DetailPageLayout1Model detailPageLayout1Model = dataResource
																.adaptTo(DetailPageLayout1Model.class);

														detailPageLayout1Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout1(detailPageLayout1Model);
														log.info("log detail page 1 "+signatureCollectionVO.getDetailPageLayout1().getDescriptionMedium());
															

													}
													
													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_2)) {

														DetailPageLayout2Model detailPageLayout2Model = dataResource
																.adaptTo(DetailPageLayout2Model.class);
														detailPageLayout2Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout2(detailPageLayout2Model);
														log.info("log detail page 2 "+signatureCollectionVO.getDetailPageLayout2().getDescriptionMedium());
													}
													// log.info("log detail page 2
													// ---400"+signatureCollectionVO.getDetailPageLayout2().getDescriptionMedium());
													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_3)) {

														DetailPageLayout3Model detailPageLayout3Model = dataResource
																.adaptTo(DetailPageLayout3Model.class);
														detailPageLayout3Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);
														signatureCollectionVO
																.setDetailPageLayout3(detailPageLayout3Model);

													}

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_4)) {

														DetailPageLayout4Model detailPageLayout4Model = dataResource
																.adaptTo(DetailPageLayout4Model.class);
														detailPageLayout4Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);
														signatureCollectionVO
																.setDetailPageLayout4(detailPageLayout4Model);

													}

													
													
												}

											}
											
											uniqueExpList.add(signatureCollectionVO);
										}
										/* unique experiences from experiences page */
										String tagId = tag.getTagID();

										if (StringUtils.isBlank(city) && tagId.equals(tlcConstants.FEATURE_TAG)) {

											Iterable<Resource> dataResources = expJCr.getChild(expJCr.getPath()
													+ tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
													.getChildren();
											
											for (Resource dataResource : dataResources) {
												
												if (dataResource instanceof Resource) {

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_1)) {

														DetailPageLayout1Model detailPageLayout1Model = dataResource
																.adaptTo(DetailPageLayout1Model.class);
														detailPageLayout1Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout1(detailPageLayout1Model);

													}

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_2)) {

														DetailPageLayout2Model detailPageLayout2Model = dataResource
																.adaptTo(DetailPageLayout2Model.class);
														detailPageLayout2Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout2(detailPageLayout2Model);

													}

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_3)) {

														DetailPageLayout3Model detailPageLayout3Model = dataResource
																.adaptTo(DetailPageLayout3Model.class);
														detailPageLayout3Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout3(detailPageLayout3Model);

													}

													if (StringUtils.equalsIgnoreCase(dataResource.getName(),
															tlcConstants.DETAIL_PAGE_4)) {

														DetailPageLayout4Model detailPageLayout4Model = dataResource
																.adaptTo(DetailPageLayout4Model.class);
														detailPageLayout4Model.setPageUrl(pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES +
																tlcConstants.SLASH  + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE
																+ tlcConstants.HTML_EXT + tlcConstants.SLASH +  childPageName);

														signatureCollectionVO
																.setDetailPageLayout4(detailPageLayout4Model);

													}

												}

												

											}
											
											uniqueExpList.add(signatureCollectionVO);

										}
										/* unique experiences new */
									}
								}
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			log.error("Exception in getExperiencesbyTag method :: {}", e);
		}
		log.info("unique experience list {}",uniqueExpList);
		return uniqueExpList;
	}

	@Override
	public LinkedHashMap<String, List<OutletModel>> getExperiencesOutletElements(String city, String propertyName,
			String cityPath, String pagePathUrl) {

		String propertyPath = cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + propertyName;
		log.debug("city path :: {} ", propertyPath);
		log.debug("page path :: {} ", pagePathUrl);

		LinkedHashMap<String, List<OutletModel>> expOutletTypeMap = new LinkedHashMap<>();

		try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page hotelPage = pageManager.getPage(propertyPath);

			Iterator<Page> outletTypeResource = hotelPage.listChildren();
			while (outletTypeResource.hasNext()) {
				Page outletTypePage = outletTypeResource.next();
				if (outletTypePage instanceof Page) {
					String outletTypeTitle = outletTypePage.getTitle();
					log.debug("outletTypeCategory :: {}", outletTypeTitle);

					List<OutletModel> outletList = new ArrayList<>();

					// OutletType
					Iterator<Page> dataResources = outletTypePage.listChildren();
					while (dataResources.hasNext()) {
						Page dataResource = dataResources.next();
						if (dataResource instanceof Page) {

							Resource dataRes2 = dataResource.getContentResource(tlcConstants.RESPONSIVE_GRID_PATH
									+ tlcConstants.SLASH + tlcConstants.DETAIL_PAGE_1);

							// OutletName
							Resource dataRes = dataResource.getContentResource(
									tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH + tlcConstants.OUTLET);

							OutletModel outletModel = dataRes.adaptTo(OutletModel.class);
							if (outletModel != null) {
								outletModel.setItems2(dataRes2);

								outletModel.setPageUrl(pagePathUrl + tlcConstants.SLASH + tlcConstants.OUTLET_CATALOGUE
										+ tlcConstants.HTML_EXT + tlcConstants.SLASH + city + tlcConstants.SLASH
										+ propertyName + tlcConstants.SLASH + dataResource.getName()
										+ tlcConstants.SLASH + outletTypePage.getName().toLowerCase());

								outletList.add(outletModel);
							}
						}
					}
					expOutletTypeMap.put(outletTypeTitle, outletList);
					log.debug("expOutletTypeMap ::{}", expOutletTypeMap.get(outletTypeTitle));

				}
			}
		} catch (Exception e) {
			log.error("Exception in getExperiencesOutlet method :: {}", e);
		}
		return expOutletTypeMap;
	}

	// Get Promotional Content for Detail Page -> for now (Overview Text) -
	// Description Large only
	@Override
	public List<DetailPageLayout1Model> getUniqueExperiencesContent(String promotionName, String promotionType,
			String promotionContentPath) {
		log.debug("promotion Name :: {}", promotionName);
		String promotionPath = "";

		switch (promotionType) {

		case tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE:
			promotionPath = promotionContentPath + tlcConstants.SLASH + promotionName;
			break;
		case tlcConstants.SIGNATURE_CATALOGUE:
			promotionPath = promotionContentPath + tlcConstants.SLASH + promotionName;
			break;
		default:

		}

		log.debug("promotionPath :: {} ", promotionPath);
		expList = new LinkedList<>();

		try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page uniqueExpPage = pageManager.getPage(promotionPath);
			Resource dataRes = uniqueExpPage.getContentResource(
					tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH + tlcConstants.DETAIL_PAGE_1);
			log.debug("dataRes :: {}", dataRes.getName());

			DetailPageLayout1Model detailPageLayout1Model = dataRes.adaptTo(DetailPageLayout1Model.class);
			expList.add(detailPageLayout1Model);

		} catch (Exception e) {
			log.error("Exception in getPromotionalContent method :: {}", e);

		}
		return expList;
	}

	@Override
	public Map<String, String> getExperienceHotelLocations(String promotionName, String promotionType,
			String promotionContentPath) {

		TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
		cityHotelNameList = new HashMap<>();
		String promotionPath = "";

		switch (promotionType) {

		case tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE:
			promotionPath = promotionContentPath + tlcConstants.SLASH + promotionName;
			break;
		case tlcConstants.SIGNATURE_CATALOGUE :
			promotionPath = promotionContentPath + tlcConstants.SLASH + promotionName;
			break;
		default:

		}
		log.debug("promotionPath :: {} ", promotionPath);

		try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page uniqueExpPage = pageManager.getPage(promotionPath);
			if (uniqueExpPage instanceof Page) {
				Resource dataRes = uniqueExpPage.getContentResource();

				log.debug("dataRes :: {}", dataRes.getName());

				if (dataRes instanceof Resource && tagManager != null) {
					Tag[] tags = tagManager.getTags(dataRes);
					for (Tag tag : tags) {
						if (tag.getTagID().contains(tlcConstants.CITY_TAG)) {
							log.debug("tag ID ::{}", tag.getTagID());
							String str = tag.getTagID();
							String[] arrOfTags = str.split("/", 3);
							if (arrOfTags.length == 3) {
								String city = arrOfTags[1];
								String hotelName = arrOfTags[2];
								log.debug("city :: {}", city);

								cityHotelNameList.put(city, hotelName);
								log.debug("cityHotelNameList ::{}", cityHotelNameList);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("Exception in getExperienceHotelLocations method :: {}", e);
		}
		return cityHotelNameList;
	}

	@Override
	public List<OfferEventsModel> getOffersEventsList(String modeType, String city, String outletType,
			String propertyName, String outletName, String offersEventPath, String pagePath, String propertyPath) {
		log.debug("property :: {}", propertyName);
		log.debug("city :: {}", city);
		log.debug("outletName :: {}", outletName);
		log.debug("outletType :: {}", outletType);
		log.debug("offerPage :: {} ", pagePath);
		String offerPageUrl = pagePath.replace("/outlet-catalogue", "/offer-event-catalogue");
		log.debug("offerPageUrl :: {} ", offerPageUrl);
		offersEventsList = new ArrayList<>();
		featuredOffersWithCityList = new ArrayList<>();
		PropertyDetailsModel propertyModel = null;
		String outletTypeTag = null;
		try {
			log.debug("offersevents path :: {}  ", offersEventPath);
			Iterator<Page> childrenPages = null;

			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

			if (pageManager != null) {

				Page rootPage = pageManager.getPage(offersEventPath);

				log.debug("root :: {} ", rootPage.getName());
				if (rootPage instanceof Page) {

					childrenPages = rootPage.listChildren();

				}

			}
			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			while (childrenPages.hasNext()) {
				Page childPage = childrenPages.next();
				if (childPage instanceof Page) {
					Resource childPageResource = childPage.getContentResource();

					if (childPageResource instanceof Resource && tagManager != null) {

						List<String> tagsArray = new ArrayList<>();
						Tag[] tags = tagManager.getTags(childPageResource);

						if (tags.length > 0) {
							for (Tag tag : tags) {
								String tagId = tag.getTagID();
								tagsArray.add(tagId);
							}

							if (StringUtils.isBlank(city) && StringUtils.equals(modeType, "featuredOffers")) {
								log.debug("mode type is featured offers");
								if (tagsArray.contains(
										tlcConstants.OFFERS_TAG + tlcConstants.SLASH + tlcConstants.FEATURED_TAG)) {

									Resource offerResource = resourceResolver.getResource(childPage.getPath());
									Page page = offerResource.adaptTo(Page.class);
									log.debug("log offer resource name :: {} ", page.getName());

									String property = "";
									String cityTag = "";
									for (String tagId : tagsArray) {
										log.debug("tagId ::{}", tagId);
										if (tagId.contains(tlcConstants.OUTLET_TYPE_TAG)) {
											String[] arrOfOutletTagId = tagId.split("/");
											outletTypeTag = arrOfOutletTagId[1];
											log.debug("outletTypeTag ::{}", outletTypeTag);
										}

										if (tagId.contains(tlcConstants.CITY_TAG)) {
											log.debug("tagId with city ::{}", tagId);
											String[] arrOfTagId = tagId.split("/");

											if (arrOfTagId.length == 3) {
												cityTag = arrOfTagId[1];
												property = arrOfTagId[2];
												log.debug("property is:: {}", property);

												String propertyTagPath = propertyPath + tlcConstants.SLASH + cityTag
														+ tlcConstants.SLASH + property;
												log.debug("propertyTagPath is :: {}", propertyTagPath);
												propertyModel = searchservice.getPropertyDetails(propertyTagPath);
												/*log.debug("propertyModel.getPropertyName()::{}",
														propertyModel.getPropertyName());*/

												if ( propertyModel != null && offerResource instanceof Resource) {

													OfferEventsModel offersEventsDetail = offerResource
															.adaptTo(OfferEventsModel.class);
													offersEventsDetail.setPageUrl(pagePath + tlcConstants.SLASH
															+ tlcConstants.EXPERIENCES + tlcConstants.SLASH
															+ tlcConstants.OFFER_EVENT_CATALOGUE + tlcConstants.HTML_EXT
															+ tlcConstants.SLASH + offerResource.getName());
													offersEventsDetail.getImage();
													offersEventsDetail.getImageAltText();

													offersEventsDetail.setPropertyName(propertyModel.getPropertyName());
													offersEventsDetail.setTagDisplay(outletTypeTag);
													offersEventsList.add(offersEventsDetail);

												}else if (propertyModel == null) {
													log.info("propertyModel is null");
												}
											}
										}
									}

								}
							} else if (StringUtils.isBlank(city)
									&& StringUtils.equalsIgnoreCase(modeType, "featuredEvents") && tagsArray.contains(
											tlcConstants.EVENT_TAG + tlcConstants.SLASH + tlcConstants.FEATURED_TAG)) {
								log.debug("mode type is featured events");
								log.debug("child page path is::{}", childPage.getPath());
								Resource eventResource = resourceResolver.getResource(childPage.getPath());
								if (eventResource instanceof Resource) {

									OfferEventsModel offersEventsDetail = eventResource.adaptTo(OfferEventsModel.class);
									offersEventsDetail.setPageUrl(pagePath + tlcConstants.SLASH
											+ tlcConstants.EXPERIENCES + tlcConstants.SLASH
											+ tlcConstants.OFFER_EVENT_CATALOGUE + tlcConstants.HTML_EXT
											+ tlcConstants.SLASH + eventResource.getName());
									offersEventsDetail.getImage();
									offersEventsDetail.getImageAltText();
									offersEventsList.add(offersEventsDetail);

								}
							} else if (StringUtils.equalsIgnoreCase(modeType, "outlet")
									&& tagsArray.contains(tlcConstants.CITY_TAG + tlcConstants.SLASH + city)
									&& tagsArray.contains(tlcConstants.OUTLET_TYPE_TAG + tlcConstants.SLASH + outletType
											+ tlcConstants.SLASH + outletName)
									&& tagsArray.contains(tlcConstants.CITY_TAG + tlcConstants.SLASH + city
											+ tlcConstants.SLASH + propertyName)) {

								Resource outletResource = resourceResolver.getResource(childPage.getPath());
								log.debug("outletRes :: {} ", outletResource.getName());

								if (outletResource instanceof Resource) {
									OfferEventsModel offersEventsDetail = outletResource
											.adaptTo(OfferEventsModel.class);

									offersEventsDetail.setPageUrl(offerPageUrl +  tlcConstants.SLASH +  
											tlcConstants.EXPERIENCES +  tlcConstants.SLASH +  
											tlcConstants.OFFER_EVENT_CATALOGUE + tlcConstants.HTML_EXT
											+ tlcConstants.SLASH + outletResource.getName());
									offersEventsDetail.getImage();
									offersEventsDetail.getImageAltText();
									offersEventsList.add(offersEventsDetail);

								}
							} else if ((StringUtils.isNotBlank(city))
									&& (StringUtils.equals(modeType, "featuredOffers"))) {

								String cityTagId = tlcConstants.CITY_TAG + tlcConstants.SLASH + city;
								if (tagsArray.contains(cityTagId) && tagsArray.contains(tlcConstants.OFFERS_TAG)) {
									log.debug("city with offers tag::::::city is not null in offers");
									Resource offerResource = resourceResolver.getResource(childPage.getPath());
									log.debug("offerResource :: {} ", offerResource.getName());

									String property = "";
									for (String tagId : tagsArray) {
										if (tagId.contains(tlcConstants.OUTLET_TYPE_TAG)) {
											String[] arrOfOutletTagId = tagId.split("/");
											outletTypeTag = arrOfOutletTagId[1];
											log.debug("outletTypeTag::{}", outletTypeTag);
										}

										
										if (tagId.contains(cityTagId)) {
											
											String[] arrOfTagId = tagId.split("/");
											if (arrOfTagId.length == 3) {
												property = arrOfTagId[2];
												String propertyTagPath = propertyPath + tlcConstants.SLASH + city
														+ tlcConstants.SLASH + property;
												log.debug("before property model");
												propertyModel = searchservice.getPropertyDetails(propertyTagPath);
												log.debug("outletTypeTag 1::{}", outletTypeTag);
												if (offerResource instanceof Resource ) {

													OfferEventsModel offersEventsDetail = offerResource
															.adaptTo(OfferEventsModel.class);
													offersEventsDetail.getImage();
													offersEventsDetail.getImageAltText();
													if(propertyModel != null) {
														offersEventsDetail.setPropertyName(propertyModel.getPropertyName());
													}
													offersEventsDetail.setPageUrl(pagePath + tlcConstants.SLASH
															+ tlcConstants.EXPERIENCES + tlcConstants.SLASH
															+ tlcConstants.OFFER_EVENT_CATALOGUE + tlcConstants.HTML_EXT
															+ tlcConstants.SLASH + offerResource.getName());
													offersEventsDetail.setTagDisplay(outletTypeTag);
													featuredOffersWithCityList.add(offersEventsDetail);

												}

											}
										}
									}

								}
							} else if (StringUtils.isNotBlank(city) && StringUtils.equals(modeType, "featuredEvents")) {
								String cityTagId = tlcConstants.CITY_TAG + tlcConstants.SLASH + city;
								if (tagsArray.contains(cityTagId) && tagsArray.contains(tlcConstants.EVENT_TAG)) {
									log.debug("city with event tag");
									Resource eventResource = resourceResolver.getResource(childPage.getPath());
									if (eventResource instanceof Resource) {

										OfferEventsModel offersEventsDetail = eventResource
												.adaptTo(OfferEventsModel.class);
										
										offersEventsDetail.setPageUrl(pagePath + tlcConstants.SLASH
												+ tlcConstants.EXPERIENCES + tlcConstants.SLASH
												+ tlcConstants.OFFER_EVENT_CATALOGUE + tlcConstants.HTML_EXT
												+ tlcConstants.SLASH + eventResource.getName());
										offersEventsDetail.getImage();
										offersEventsDetail.getImageAltText();

										offersEventsList.add(offersEventsDetail);

									}

								}
							}

						}

					}

				}

			}

		} catch (Exception e) {

			log.error("Exception in getBenefitsList method :: ", e);
		}

		return offersEventsList;

	}

	@Override
	public List<OfferEventsModel> getFeaturedOffersWithCityList() {

		return featuredOffersWithCityList;
	}

	@Override
	public List<PropertyDetailsModel> getFeaturedHotelsList(String cityPath, String parentPagePath, String modetag,
			String city) {

		List<PropertyDetailsModel> propertyList = new ArrayList<>();
		PropertyDetailsModel propertyModel = null;

		try {
			log.debug("parentPagePath path :: {}  ", parentPagePath);
			Iterator<Page> childrenPages = null;
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page rootPage = pageManager.getPage(cityPath);
				log.info("root :: {} ", rootPage.getName());
				if (rootPage instanceof Page) {
					childrenPages = rootPage.listChildren();
				}
			}
			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			while (childrenPages.hasNext()) {
				Page childPage = childrenPages.next();
				if (childPage instanceof Page) {
					Resource childPageResource = childPage.getContentResource();
					if (childPageResource instanceof Resource && tagManager != null) {
						List<String> tagsArray = new ArrayList<>();
						Tag[] tags = tagManager.getTags(childPageResource);
						if (tags.length > 0) {
							for (Tag tagelement : tags) {
								String tagId = tagelement.getTagID();
								tagsArray.add(tagId);
							}
							if (StringUtils.equals(modetag, "participatingHotel")) {

								if (tagsArray.contains(tlcConstants.PARTICIPATING_HOTEL_TAG)) {
									log.info("city with event tag");
									log.info("ChildPage :: {} ", childPage.getPath());
									Resource propertyResource = resourceResolver.getResource(childPage.getPath());
									if (propertyResource instanceof Resource) {
										log.info("propertyResource :: {} ", propertyResource.getPath());
										Iterable<Resource> dataResources = propertyResource
												.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH
														+ tlcConstants.RESPONSIVE_GRID_PATH)
												.getChildren();

										for (Resource dataResource : dataResources) {
											log.info(" dataResource :: {} ", dataResource.getPath());
											if (dataResource instanceof Resource && StringUtils.containsIgnoreCase(
													dataResource.getName(), tlcConstants.PROPERTY)) 
											{
												
												propertyModel = dataResource.adaptTo(PropertyDetailsModel.class);
												propertyModel.setPageUrl(parentPagePath + tlcConstants.SLASH
														+ tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH
														+ tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT
														+ tlcConstants.SLASH + city + tlcConstants.SLASH
														+ propertyResource.getName());
											/*	log.info("property model :: {} ", propertyModel.getDescriptionSmall());
												log.info("property model URL :: {} ", propertyModel.getPageUrl());*/
												propertyList.add(propertyModel);
											}
										}

									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in getBenefitsList method :: ", e);
		}
		return propertyList;

	}
}