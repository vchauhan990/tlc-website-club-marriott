package com.tlc.web.platform.core.vo;

public class SectionVO {

	private String sectionType;
	private String sectionTypeLarge;
	private String sectionTypeXLarge;
	private String parsysId;
	private String customClass;
	private String verticalAlign;
	private String horizontalAlign;

	public String getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(String verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	public String getHorizontalAlign() {
		return horizontalAlign;
	}

	public void setHorizontalAlign(String horizontalAlign) {
		this.horizontalAlign = horizontalAlign;
	}

	public String getCustomClass() {
		return customClass;
	}

	public void setCustomClass(String customClass) {
		this.customClass = customClass;
	}

	public String getSectionType() {
		return sectionType;
	}

	public void setSectionType(String sectionType) {
		this.sectionType = sectionType;
	}

	public String getParsysId() {
		return parsysId;
	}

	public void setParsysId(String parsysId) {
		this.parsysId = parsysId;
	}

	public String getSectionTypeLarge() {
		return sectionTypeLarge;
	}

	public void setSectionTypeLarge(String sectionTypeLarge) {
		this.sectionTypeLarge = sectionTypeLarge;
	}

	public String getSectionTypeXLarge() {
		return sectionTypeXLarge;
	}

	public void setSectionTypeXLarge(String sectionTypeXLarge) {
		this.sectionTypeXLarge = sectionTypeXLarge;
	}

}
