package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OutletModel;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class OutletDetailModel {

	private static Logger log = LoggerFactory.getLogger(OutletDetailModel.class);

	@Inject

	private SlingHttpServletRequest request;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	@Self
	Resource resource;

	PageManager pageManager;

	@Inject
	public SearchService searchService;

	DetailPageLayout1Model detailpagelayout;

	OutletModel outlet;

	@Inject
	Page currentPage;

	@PostConstruct
	public void init() {

		try {

			String pagePath = currentPage.getParent().getPath();
			String propertyPath = pagePath.replace( tlcConstants.SLASH + tlcConstants.OUTLET_CATALOGUE, tlcConstants.PROPERTY_PAGE);

			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());

			ValueMap vm = currentPage.getContentResource().getValueMap();
			String cityPath = vm.get("cityPath", String.class);
			String[] segments = requestPathInfo.getSuffix().split("/");
			String city = segments[segments.length - 4];
			String propertyName = segments[segments.length - 3];
			String outletName = segments[segments.length - 2];
			String outletType = segments[segments.length - 1];

			Resource outletResource = searchService
					.outletResource(cityPath + "/" + city + "/" + propertyName + "/" + outletType + "/" + outletName);
			detailpagelayout = searchService.getDetailPageLayout1Card(outletResource);
			outlet = searchService.getOutletCard(outletResource);
			outlet.setPageUrl(propertyPath + "/property-page.html/" + city + "/" + propertyName);

		} catch (Exception e) {
			log.error("Exception in Outlet Detail Model ::{}", e);
		}
	}

	public DetailPageLayout1Model getDetailpagelayout() {
		return detailpagelayout;
	}

	public OutletModel getOutlet() {
		return outlet;
	}

}
