package com.tlc.web.platform.core.models;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.Banner;
import com.tlc.web.platform.core.vo.CarouselVO;

/**
 * 
 * Sling Implementation of Carousel Model
 * 
 * @author DWAO
 *
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CarouselModelNew {

	/** Logger */
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Inject
	@Self
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	@Default(values = "{}")
	private String[] carouselItems;

	private List<Banner> bannerList;

	@PostConstruct
	protected void init() {
		try {

			Gson gson = new Gson();
			if (carouselItems.length > 0) {
				bannerList = new LinkedList<>();
				for (String eachString : carouselItems) {
					
					// Parsing each string (json) into java class
					CarouselVO carousel = gson.fromJson(eachString, CarouselVO.class);
					PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

					Page bannerPage = pageManager.getPage(carousel.getBannerPagePath());
					Resource jcrResource = bannerPage.getContentResource();

					if (bannerPage instanceof Page) {
						Resource bannerMenuResource = resourceResolver.getResource(jcrResource.getPath() + tlcConstants.SLASH + tlcConstants.BANNER_PATH);

						if (bannerMenuResource instanceof Resource) {
							
							Banner banner = new Banner();
							log.info("Path of the Menu Resource :: {} ", bannerMenuResource.getPath());
							BannerModel bannerModel = bannerMenuResource.adaptTo(BannerModel.class);
							if (bannerModel != null) {
								banner.setAltText(bannerModel.getAltText());
								banner.setCtaText(bannerModel.getCtaText());
								banner.setCtaUrl(bannerModel.getCtaUrl());
								banner.setDesktopImage(bannerModel.getDesktopImage());
								banner.setHeading(bannerModel.getHeading());
								banner.setSubHeading(bannerModel.getSubHeading());
								banner.setDescription(bannerModel.getDescription());
								banner.setCtaList(bannerModel.getCtaList());
								banner.setOpenInNewWindow(bannerModel.getOpenInNewWindow());
								banner.setAlign(bannerModel.getAlign());
								banner.setHorizontalAlign(bannerModel.getHorizontalAlign());
								banner.setBackgroundType(bannerModel.getBackgroundType());
								banner.setHeadingType(bannerModel.getHeadingType());
								banner.setBackgroundVideo(bannerModel.getBackgroundVideo());
								banner.setBackgroundType(bannerModel.getBackgroundType());
								banner.setImgPath(bannerModel.getImgPath());
								banner.setSrcPath(bannerModel.getSrcPath());
								banner.setBannerType(bannerModel.getBannerType());
								banner.setImgCopyrightText(bannerModel.getImgCopyrightText());

								bannerList.add(banner);
							}
						}
					}

				}
			}
		} catch (Exception e) {
			log.error("Exception in CarouselModelImpl :: ", e);

		}

	}

	public List<Banner> getBannerList() {
		return bannerList;
	}
}