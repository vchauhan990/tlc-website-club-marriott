package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.Socialitem;

/**
 * 
 * Sling Implementation of Social Link Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class SocialLinkModel {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Inject
	@Optional
	@Default(values = "{}")
	private String[] listofItems;

	private List<Socialitem> socialLinksList;

	@PostConstruct
	public void init() {

		Gson gson = new Gson();
		try {
			if (listofItems.length > 0) {
				socialLinksList = new LinkedList<>();

				for (String eachString : listofItems) {
					Socialitem eachObj = gson.fromJson(eachString, Socialitem.class);
					socialLinksList.add(eachObj);
				}
			}
		} catch (Exception e) {
			log.error("Exception in init method of SocialLinksModelImpl ::  ", e);
		}
	}

	public List<Socialitem> getSocialLinksList() {
		return socialLinksList;
	}

}
