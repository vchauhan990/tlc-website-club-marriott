package com.tlc.web.platform.core.services.impl;

import static com.day.cq.commons.jcr.JcrConstants.JCR_TITLE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tlc.web.platform.core.services.TagService;
import com.tlc.web.platform.core.vo.TagVO;
import com.day.cq.tagging.Tag;

/**
 * Service Implementation of Tag Service
 * 
 * @author DWAO
 *
 */

@Component(immediate = true, service = TagService.class)
public class TagServiceImpl implements TagService{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	ResourceResolverFactory resourceResolverFactory;

	private ResourceResolver resourceResolver;

	private Session session;

	@Activate
	protected void activate(final Map<Object, Object> config) {

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "contentJsonReader");

		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			log.debug("TagService Activated");
		} catch (Exception e) {
			log.error("Exception in activate method of TagService :: ", e);
		}
	}

	@Deactivate
	protected void deactivate(ComponentContext ctx){
		if (session != null) {
			session.logout();
		}
		if (resourceResolver != null) {
			resourceResolver.close();
		}
	}
	@Override
	public List<TagVO> getTagList(String tagCategoryPath) {

		List<TagVO> tagList = new ArrayList<>();
		Resource tagRes = resourceResolver.getResource(tagCategoryPath);

		if (tagRes != null) {

			Iterator<Resource> tags = tagRes.listChildren();

			tags.forEachRemaining(res -> {
				TagVO tagVo = new TagVO();
				ValueMap properties = res.getValueMap();
				tagVo.setTitle(properties.get(JCR_TITLE, StringUtils.EMPTY));
				Tag tag = res.adaptTo(Tag.class);
				if (tag != null) {
					tagVo.setTagValue(tag.getTagID());
				}				   
				tagList.add(tagVo);
			});
		}
		return tagList;
	}

}