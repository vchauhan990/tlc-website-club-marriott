package com.tlc.web.platform.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.CertificateModel;
import com.aem.tlc.core.models.TermsConditionsModel;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Membership Terms Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class,
		Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MembershipTermsModel {

	private static Logger log = LoggerFactory.getLogger(MembershipTermsModel.class);

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	Page currentPage;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	SlingHttpServletRequest request;

	Page rootPage;
	TermsConditionsModel termsConditionsModel;

	CertificateModel certificateDetail;

	@PostConstruct
	public void init() {

		requestPathInfo = request.getRequestPathInfo();
		log.debug("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());

		String[] segments = requestPathInfo.getSuffix().split("/");
		ValueMap vm = currentPage.getContentResource().getValueMap();

		String membershipTypePath = vm.get("membershipTypePath", String.class);

		try {

			String propertyName = segments[segments.length - 3];
			String parentCertificateName = segments[segments.length - 2];
			String certificateName = segments[segments.length - 1];

			String membershipTypeCertificatePath = membershipTypePath + tlcConstants.SLASH + propertyName
					+ tlcConstants.LEVEL_1 + tlcConstants.SLASH + tlcConstants.CERTIFICATES + tlcConstants.SLASH
					+ parentCertificateName + tlcConstants.SLASH + certificateName;
			log.debug("membershipTypeCertificatePath is :: {}", membershipTypeCertificatePath);
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

			if (pageManager != null) {
				rootPage = pageManager.getPage(membershipTypeCertificatePath);
			}

			Resource certificateResource = rootPage.getContentResource()
					.getChild(tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH + tlcConstants.CERTIFICATE);
			certificateDetail = certificateResource.adaptTo(CertificateModel.class);

			Resource membershipTermsResource = rootPage.getContentResource()
					.getChild(tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH + tlcConstants.MEMBERSHIPTERMS);

			log.debug("membershipTermsResource page path :: {}", membershipTermsResource.getPath());

			termsConditionsModel = membershipTermsResource.adaptTo(TermsConditionsModel.class);
			termsConditionsModel.getGroup1Features();
			termsConditionsModel.getGroup2Features();

		} catch (Exception e) {
			log.error("Exception in Membership Terms Model  :: {}", e);
		}
	}

	public TermsConditionsModel getTermsAndConditions() {
		return termsConditionsModel;
	}

	public CertificateModel getCertificateDetail() {
		return certificateDetail;
	}

}
