package com.tlc.web.platform.core.models;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tlc.web.platform.core.services.TagService;
import com.tlc.web.platform.core.util.LinkUtil;
import com.tlc.web.platform.core.vo.PageObject;
import com.tlc.web.platform.core.vo.PageVO;
import com.tlc.web.platform.core.vo.TagVO;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListingModel {

	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(ListingModel.class);

	@Self
	@Inject
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@Inject	@Optional
	private String modeType;

	@Inject @Optional
	private String ctaText;

	@Inject @Optional
	@Default(values = "{}")
	private String[] listingItems;

	@Inject @Optional
	@Default(values = "{}")
	private String[] pageItems;

	@Inject @Optional
	@Default(values = "/content")
	private String parentPagePath;

	private List<PageObject> listingPageList;
	
	PageObject ls = null;
	
	List<TagVO> sectionTags ;
	
	TagService tagService ;
	
	@Inject @Optional
	@Default(values = "{}")
	String[] tags;
	
	private String pageListJson;
	
	@Inject	
	private String viewType;
	
	@Inject	@Optional
	private String viewAllctaText;
	
	@Inject	@Optional
	private String ctaUrl;
	
	@PostConstruct
	public void init() {
		
		Gson gson = new Gson();
	
		try {
			if (modeType.equalsIgnoreCase("auto")) {
				log.debug(".........modeType Auto invoked .....");

				if (StringUtils.isNotBlank(parentPagePath)) {										
					
					listingPageList = new LinkedList<>();

					Resource pageResource = resourceResolver.getResource(parentPagePath);

					log.debug("Resource associated with Parent page Path :: {}" , pageResource);

					if (pageResource instanceof Resource) {
						Page hotelpage = pageResource.adaptTo(Page.class);
						if (hotelpage instanceof Page) {
							Iterable<Resource> hotelpageResources = pageResource.getChildren();
							for (Resource hotelpageResource : hotelpageResources) {
								if (!hotelpageResource.getName().equalsIgnoreCase(JcrConstants.JCR_CONTENT)) {
									Page hotelResourcePage = hotelpageResource.adaptTo(Page.class);
									if (hotelResourcePage instanceof Page) {
										ValueMap pageProperties = hotelResourcePage.getProperties();
										ls = new PageObject();
										ls.setTitle(pageProperties.get("jcr:title", String.class));
										ls.setSubtitle(pageProperties.get("subtitle", String.class));
										ls.setDescription(pageProperties.get("jcr:description",pageProperties.get("description", "")));
										ls.setPageUrl(hotelResourcePage.getPath());
										ls.setThumbnailImage(pageProperties.get("thumbnailImage", String.class));
										ls.setTags(pageProperties.get("cq:tags", String[].class));
										ls.setAltText(pageProperties.get("altText", String.class));
										ls.setCtaText(pageProperties.get("ctaText", String.class));
									}
									    listingPageList.add(ls);
								}
							}

						}

					}

				}
				pageListJson = gson.toJson(listingPageList);
				log.debug("blogPagesJson : {}" , pageListJson);
				 

			} else if (modeType.equalsIgnoreCase("manual")   && listingItems.length > 0){
				log.debug(".........modeType manual invoked .....");
				log.debug("length of listing items ::{}" , listingItems.length);

					listingPageList = new LinkedList<>();
					for (String eachString : listingItems) {
						log.info(eachString);
						PageObject eachItem = gson.fromJson(eachString, PageObject.class);
						listingPageList.add(eachItem);
					}
			}
			
			else if(modeType.equalsIgnoreCase("singlePage")) {	
				log.info(".........modeType single Page Path invoked .....");				
				
				listingPageList = new LinkedList<>();
				
				for(String itemString : pageItems) {
					PageVO pageObj = gson.fromJson(itemString, PageVO.class);
					if(pageObj != null) {
						Resource pageResource = resourceResolver.getResource(pageObj.getPagePath());
						 Page page =pageResource.adaptTo(Page.class);
							 if(page instanceof Page) {
									Resource r=	 page.getContentResource();
									
									ValueMap pageProperties = r.getValueMap();
									ls = new PageObject();
									ls.setTitle(pageProperties.get("jcr:title", String.class));
									ls.setDescription(pageProperties.get("jcr:description",pageProperties.get("description", "")));
									ls.setPageUrl(page.getPath());
									ls.setThumbnailImage(pageProperties.get("thumbnailImage", String.class));
									ls.setTags(pageProperties.get("cq:tags", String[].class));
									ls.setAltText(pageProperties.get("altText", String.class));
									ls.setCtaText(pageProperties.get("ctaText", String.class));

								 }
						
						}
					listingPageList.add(ls);
				}
				
				pageListJson = gson.toJson(listingPageList);
				log.info("page List Json : {}" , pageListJson);
			}
							
		}catch (Exception e) {
			log.error("the exception occured in Listing Model ", e);
		}
	}

	public List<PageObject> getListingPageList() {
		return listingPageList;
	}

	public String[] getTags() {
		return tags;
	}

	public String getPageListJson() {
		return pageListJson;
	}

	public String getViewType() {
		return viewType;
	}

	public String getViewAllctaText() {
		return viewAllctaText;
	}

	public String getCtaUrl() {
		return LinkUtil.getFormattedURL(ctaUrl);
	}

}
