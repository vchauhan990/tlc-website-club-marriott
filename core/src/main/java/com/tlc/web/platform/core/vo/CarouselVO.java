package com.tlc.web.platform.core.vo;

/**
 * CarouselItem DTO
 * 
 * @author
 *
 */

public class CarouselVO {
	
	private String bannerPagePath;

	public String getBannerPagePath() {
		return bannerPagePath;
	}

}
