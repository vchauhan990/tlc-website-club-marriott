package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.AnimationVO;

/**
 * 
 * Sling Implementation of Download App Animation Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class DownloadAppAnimationModel {
	
	private static Logger log = LoggerFactory.getLogger(DownloadAppAnimationModel.class);
	
	@Inject @Optional
	@Default(values = "{}")
	private String[] animationItems;
	
	@Inject @Optional
	@Default(values = "{}")
	private String[] animation2Items;
	
	@Inject
	private String animationType;

	private List<AnimationVO> animationItemsList;
	private List<AnimationVO> animation2ItemsList;
	
	@PostConstruct
	protected void init() {

		Gson gson = new Gson();
		animationItemsList = new LinkedList<>();
		animation2ItemsList = new LinkedList<>();
		log.info("inside init of teasers");
		for (String itemString : animationItems) {
			AnimationVO animationItem = gson.fromJson(itemString, AnimationVO.class);
			animationItemsList.add(animationItem);
		}
		
		for (String itemString : animation2Items) {
			AnimationVO animation2Item = gson.fromJson(itemString, AnimationVO.class);
			animation2ItemsList.add(animation2Item);
		}
	}
	
	
	public List<AnimationVO> getAnimationItems() {
		return animationItemsList;
	}
	
	public List<AnimationVO> getAnimation2Items() {
		return animation2ItemsList;
	}
	
	
	public String getAnimationType() {
		return animationType;
	}
	
}