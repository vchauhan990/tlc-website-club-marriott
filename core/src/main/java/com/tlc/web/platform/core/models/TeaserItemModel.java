package com.tlc.web.platform.core.models;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tlc.web.platform.core.util.LinkUtil;

/**
 * 
 * Sling Implementation of Teaser Item Model
 * 
 * @author DWAO
 *
 */

public interface TeaserItemModel {
	
	
	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class First {
		private static Logger log = LoggerFactory.getLogger(TeaserItemModel.class);
		@Inject
		public String align;
		
		@Inject
		public String title;
		
		@Inject
		public String description;
		
		@Inject
		public String imagePath;
		
		@Inject
		public String imageAltText;
		
		@Inject
		public String ctaText;
		
		@Inject
		public String ctaLink;
		
		@Inject
		public String ctaOpenInNewWindow;
		
		@Inject
		public String mobileImagePath;
		
		@Inject
		public String mobileImageAltText;
		
		@PostConstruct
		protected void init() {
			log.debug("inside init of first");
			ctaLink = LinkUtil.getFormattedURL(ctaLink);
		}
		public String getAlign() {
			return align;
		}

		public String getTitle() {
			return title;
		}

		public String getDescription() {
			return description;
		}

		public String getImagePath() {
			return imagePath;
		}

		public String getImageAltText() {
			return imageAltText;
		}

		public String getCtaText() {
			return ctaText;
		}

		public String getCtaLink() {
			return LinkUtil.getFormattedURL(ctaLink);
		}

		public String getCtaOpenInNewWindow() {
			return ctaOpenInNewWindow;
		}

		public String getMobileImagePath() {
			return mobileImagePath;
		}

		public String getMobileImageAltText() {
			return mobileImageAltText;
		}

		public void setCtaLink(String ctaLink) {
			this.ctaLink = ctaLink;
		}

	}	

}
