package com.tlc.web.platform.core.util;


public class tlcConstants {
	
	public static final String JCR_CONTENT = "jcr:content";
	public static final String BANNER_PATH = "banner";
	public static final String BLOG_DETAIL_PATH = "/jcr:content/blogslayout";
	public static final String CITY_TAG ="tlc:city";
	public static final String FEATURE_TAG ="tlc:feature";
	public static final String OFFERS_TAG ="tlc:offers";
	public static final String SUBBRAND_TAG ="tlc:sub-brand";
	public static final String FEATURED_TAG ="featured";
	public static final String EVENT_TAG ="tlc:event";
	public static final String OUTLET_TYPE_TAG ="tlc:outlet-type";
	public static final String OUTLET_NAME_TAG ="outlet-name";
	public static final String CITY ="city";
	public static final String REP_POLICY ="rep:policy";
	public static final String HOME ="home";
	public static final String HOTELS_AND_RESORTS = "hotels-and-resorts";
	public static final String TERMS_CONDITIONS = "terms-condition-catalogue";
	public static final String PROPERTY_PAGE = "property-page";
	public static final String  PARTICIPATING = "participating";
	public static final String  RESTAURANTS = "restaurants"; 
	public static final String  EXPERIENCE = "experience";  
	public static final String  EXPERIENCES = "experiences";  
	public static final String  UNIQUE_EXPERIENCES = "unique-experiences";
	public static final String  UNIQUE_EXPERIENCE = "unique-Experience"; 
	public static final String SIGNATURE_COLLECTIONS = "signature-Collections";
	public static final String BENEFITS = "benefits" ;
	public static final String OUTLET_CATALOGUE = "outlet-catalogue" ; 
	public static final String HTML_EXT = ".html";
	public static final String PARAM_EXT = "?";
	public static final String SLASH = "/";
	public static final String PARTICIPATING_HOTEL_TAG ="tlc:participating-hotel";
		
	//MemberBenefitsListingService
	public static final String MEMBERSHIP_TYPE = "membership-type";
	public static final String LEVEL_1 = "-level-1";
	public static final String CERTIFICATES = "certificates";
	public static final String CERTIFICATE = "certificate";
	public static final String CARD_BENEFITS = "card-benefits";
	public static final String CARDBENEFIT = "cardbenefit";
	public static final String TERMS_AND_CONDITIONS= "terms-and-conditions";
	public static final String TERMS_AND_CONDITIONS_COMPONENT= "termsandconditions";
	public static final String MEMBERSHIPTERMS= "membershipterms";
	public static final String EXPERIENCE_SIGNATURE_CATALOGUE = "experience-signature-catalogue";
	public static final String SIGNATURE_CATALOGUE = "signature-catalogue";
	public static final String UNIQUE_EXPERIENCE_CATALOGUE = "unique-experience-catalogue";

	//PromotionalcontentFetchService
	public static final String UNIQUE_EXPERIENCES_PATH = "/promotional-content/unique-experiences";
	public static final String SIGNATURE_COLLECTIONS_PATH = "/promotional-content/signature-collections";
	public static final String 	RESPONSIVE_GRID_PATH = "root/responsivegrid";
	public static final String 	LANGUAGE_MASTERS_PATH = "/content/tlc/language-masters/en";
	public static final String BRAND = "marriott";
	public static final String SUB_BRAND= "club-marriott";
	public static final String SUB_BRAND_COUNTRY= "club-marriott-india";

	public static final String OFFERS_EVENTS_PATH = "/promotional-content/offers-and-events";
	public static final String FEATURED_EVENTS = "featuredEvents" ;
	public static final String OFFER_EVENT_CATALOGUE = "offer-event-catalogue" ; 
	//SearchServiceImpl
	public static final String OFFERS_EVENTS_BANNER_PATH = "/promotional-content/offers-and-events-banner";
	public static final String UNIQUE_EXPERIENCES_BANNER_PATH = "/promotional-content/unique-experiences-banner";
	public static final String SIGNATURE_COLLECTIONS_BANNER_PATH = "/promotional-content/signature-collections-banner";
	//components 
	public static final String DETAIL_PAGE_1 = "detailpage1";
	public static final String DETAIL_PAGE_2 = "detailpage2";
	public static final String DETAIL_PAGE_3 = "detailpage3";
	public static final String DETAIL_PAGE_4 = "detailpage4";
	public static final String PROPERTY = "property";
	public static final String BECOME_A_MEMBER = "becomeamember";
	public static final String OUTLET = "outlet";

}