package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.DetailPageLayout2Model;
import com.aem.tlc.core.models.DetailPageLayout3Model;
import com.aem.tlc.core.models.DetailPageLayout4Model;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PageVO;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * 
 * Sling Implementation of Dynamic Listing Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class DynamicListingModel {

	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(DynamicListingModel.class);

	@Self
	@Inject
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@ValueMapValue
	private String componentElement;

	@Inject
	Page currentPage;

	@ValueMapValue
	@Default(values = "{}")
	private String[] pageItems;

	List<PropertyDetailsModel> propertyList = null;
	List<SignatureCollectionVO> dinningList = null;
	List<SignatureCollectionVO> experienceList = null;

	
	String pagePath = "";
	
	@PostConstruct
	public void init() {
		try {

			propertyList = new LinkedList<>();
			experienceList = new LinkedList<>();
			dinningList = new LinkedList<>();
			log.info("inside dynamic listing component");
			Gson gson = new Gson();

			if (StringUtils.equals(currentPage.getName(), tlcConstants.HOME)) {

				pagePath = currentPage.getPath();

			}else {
				pagePath = currentPage.getParent().getPath();
			}
			
			for (String itemString : pageItems) {
				PageVO pageObj = gson.fromJson(itemString, PageVO.class);

				log.debug("Page Obj :: {} ", pageObj.getPagePath());

				if (StringUtils.equals(componentElement, tlcConstants.PARTICIPATING) && !StringUtils.isBlank(pageObj.getPagePath())) {

						Resource propertyResources = resourceResolver.getResource(pageObj.getPagePath());
						Iterable<Resource> childResources = propertyResources.getChild(
								JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
								.getChildren();
						for (Resource childResource : childResources) {

							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.PROPERTY)) {
								String[] page = pageObj.getPagePath().split(tlcConstants.SLASH + tlcConstants.CITY + tlcConstants.SLASH);
								String[] page2 = page[1].split(tlcConstants.SLASH);

								PropertyDetailsModel property = childResource.adaptTo(PropertyDetailsModel.class);
								String pagePathLink = pagePath + tlcConstants.SLASH+  tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH 
										+ tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT ;
										/*"/hotels-and-resorts/property-page.html"*/
								property.setPageUrl(
										pagePathLink + tlcConstants.SLASH + page2[0] + tlcConstants.SLASH + page2[1]);
								log.debug("Property Url :: {} ", property.getPageUrl());
								propertyList.add(property);
							}
						}				
				}
				
				if (StringUtils.equals(componentElement, tlcConstants.RESTAURANTS) && !StringUtils.isBlank(pageObj.getPagePath()) ) {

					String[] page = pageObj.getPagePath().split(tlcConstants.SLASH + tlcConstants.CITY + tlcConstants.SLASH);
					String[] page2 = page[1].split("/", 2);
					String[] page3 = page2[1].split("/", 2);
					String[] page4 = page3[1].split("/");

					Resource outletResources = resourceResolver.getResource(pageObj.getPagePath());
						Iterable<Resource> childResources = outletResources.getChild(
								JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
								.getChildren();
						for (Resource childResource : childResources) {
							SignatureCollectionVO signatureCollectionVO = new SignatureCollectionVO();
							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_1)) {

								DetailPageLayout1Model restaurant = childResource.adaptTo(DetailPageLayout1Model.class);
								restaurant.setPageUrl(pagePath + tlcConstants.SLASH+  tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH +
										tlcConstants.OUTLET_CATALOGUE + tlcConstants.HTML_EXT + tlcConstants.SLASH + page2[0] + tlcConstants.SLASH + page3[0] + tlcConstants.SLASH + page4[1] + tlcConstants.SLASH + page4[0]);
								/*"/hotels-and-resorts/outlet-catalogue.html/"*/ 
								signatureCollectionVO.setDetailPageLayout1(restaurant);
								dinningList.add(signatureCollectionVO);
							}
							
							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_2)) {

								DetailPageLayout2Model restaurant = childResource.adaptTo(DetailPageLayout2Model.class);
								restaurant.setPageUrl(pagePath + tlcConstants.SLASH+  tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH +
										tlcConstants.OUTLET_CATALOGUE + tlcConstants.HTML_EXT + tlcConstants.SLASH + page2[0] + tlcConstants.SLASH + page3[0] + tlcConstants.SLASH + page4[1] + tlcConstants.SLASH + page4[0]);
								/*"/hotels-and-resorts/outlet-catalogue.html/"*/ 
								signatureCollectionVO.setDetailPageLayout2(restaurant);
								dinningList.add(signatureCollectionVO);
							}
							
							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_3)) {

								DetailPageLayout3Model restaurant = childResource.adaptTo(DetailPageLayout3Model.class);
								restaurant.setPageUrl(pagePath + tlcConstants.SLASH+  tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH +
										tlcConstants.OUTLET_CATALOGUE + tlcConstants.HTML_EXT + tlcConstants.SLASH+ page2[0] + tlcConstants.SLASH + page3[0] + tlcConstants.SLASH + page4[1] + tlcConstants.SLASH + page4[0]);
								/*"/hotels-and-resorts/outlet-catalogue.html/"*/ 
								signatureCollectionVO.setDetailPageLayout3(restaurant);
								dinningList.add(signatureCollectionVO);
							}
							
							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_4)) {

								DetailPageLayout4Model restaurant = childResource.adaptTo(DetailPageLayout4Model.class);
								restaurant.setPageUrl(pagePath + tlcConstants.SLASH+  tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH +
										tlcConstants.OUTLET_CATALOGUE + tlcConstants.HTML_EXT + tlcConstants.SLASH + page2[0] + tlcConstants.SLASH + page3[0] + tlcConstants.SLASH + page4[1] + tlcConstants.SLASH + page4[0]);
								/*"/hotels-and-resorts/outlet-catalogue.html/"*/ 
								signatureCollectionVO.setDetailPageLayout4(restaurant);
								dinningList.add(signatureCollectionVO);
							}
						}

				}
				if (StringUtils.equals(componentElement, tlcConstants.EXPERIENCE) && !StringUtils.isBlank(pageObj.getPagePath()) ) {

					Resource experienceResource = resourceResolver.getResource(pageObj.getPagePath());
					
					
						String[] page = pageObj.getPagePath().split(tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCES + tlcConstants.SLASH);

						Iterable<Resource> childResources = experienceResource.getChild(
								JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
								.getChildren();
						for (Resource childResource : childResources) {
							SignatureCollectionVO signatureCollectionVO = new SignatureCollectionVO();
							if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_1)) {

								DetailPageLayout1Model detailPageLayout1 = childResource.adaptTo(DetailPageLayout1Model.class);
								detailPageLayout1.setPageUrl(pagePath
										+ tlcConstants.SLASH + tlcConstants.EXPERIENCES + tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE + 
										tlcConstants.HTML_EXT + tlcConstants.SLASH + page[1]);
										/*"/hotels-and-resorts/experience-signature-catalogue.html/unique-Experience/"*/
										
								signatureCollectionVO.setDetailPageLayout1(detailPageLayout1);
							
							}else if(StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_2)){
								log.info("inside detail page layput 2");
								DetailPageLayout2Model detailPageLayout2 = childResource.adaptTo(DetailPageLayout2Model.class);
								detailPageLayout2.setPageUrl(pagePath
										+ tlcConstants.SLASH + tlcConstants.EXPERIENCES + tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE + 
										tlcConstants.HTML_EXT + tlcConstants.SLASH
										+ page[1]);
								signatureCollectionVO.setDetailPageLayout2(detailPageLayout2);
								log.info("signatureCollectionVO 2 ::{}" ,signatureCollectionVO.getDetailPageLayout2() );
								
							}else if(StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_3)) {
								
								DetailPageLayout3Model detailPageLayout3 = childResource.adaptTo(DetailPageLayout3Model.class);
								detailPageLayout3.setPageUrl(pagePath
										+ tlcConstants.SLASH + tlcConstants.EXPERIENCES + tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE + 
										tlcConstants.HTML_EXT + tlcConstants.SLASH
										+ page[1]);
								signatureCollectionVO.setDetailPageLayout3(detailPageLayout3);
								
								
							}else if(StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.DETAIL_PAGE_4)) {
								
								DetailPageLayout4Model detailPageLayout4 = childResource.adaptTo(DetailPageLayout4Model.class);
								detailPageLayout4.setPageUrl(pagePath
										+ tlcConstants.SLASH + tlcConstants.EXPERIENCES + tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE + 
										tlcConstants.HTML_EXT + tlcConstants.SLASH
										+ page[1]);
								signatureCollectionVO.setDetailPageLayout4(detailPageLayout4);
								
							}
							experienceList.add(signatureCollectionVO);
						}
				}

			}
		} catch (Exception e) {
			log.error("Exception in Dynamic Listing Model :: {} ", e);
		}
	}

	public String getComponentElement() {
		return componentElement;
	}

	public List<PropertyDetailsModel> getPropertyList() {
		return propertyList;
	}

	public List<SignatureCollectionVO> getDinningList() {
		return dinningList;
	}

	public List<SignatureCollectionVO> getExperienceList() {
		return experienceList;
	}
}
