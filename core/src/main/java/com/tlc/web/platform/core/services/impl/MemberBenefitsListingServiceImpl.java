package com.tlc.web.platform.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.CardBenefitsModel;
import com.aem.tlc.core.models.CertificateModel;
import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.DetailPageLayout2Model;
import com.aem.tlc.core.models.DetailPageLayout3Model;
import com.aem.tlc.core.models.DetailPageLayout4Model;
import com.aem.tlc.core.models.TermsConditionsModel;
import com.day.cq.replication.Replicator;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.tlc.web.platform.core.services.MemberBenefitsListingService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * Service Implementation of Member Benefits Listing Service
 * 
 * @author DWAO
 *
 */

@Component(immediate = true, service = { MemberBenefitsListingService.class }, enabled = true, property = {
		Constants.SERVICE_DESCRIPTION + "= Member Benefits Listing Service Implementation" })
public class MemberBenefitsListingServiceImpl implements MemberBenefitsListingService {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Reference
	private SearchService searchService;

	ResourceResolver resourceResolver;

	@Reference
	Replicator replicationService;

	Session session;

	List<CertificateModel> certificateList;
	List<CardBenefitsModel> cardBenefitsList;
	List<SignatureCollectionVO> signatureCollectionList;
	List<TermsConditionsModel> membershipTermsList;

	private static final Logger log = LoggerFactory.getLogger(MemberBenefitsListingServiceImpl.class);

	@Activate
	protected final void activate(final Map<Object, Object> config) {
		log.debug("Activated Member Benefits Listing Service");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "contentJsonReader");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			log.debug("Resource Resolver :: {}", resourceResolver);
			session = resourceResolver.adaptTo(Session.class);
			log.debug("Got Resource Resolver and Session for MemberBenefitsListingServiceImpl :: {} ",
					session.getUserID());
		} catch (LoginException le) {
			log.error("LoginExcepion in activate method of MemberBenefitsListingServiceImpl :: {} ", le.getMessage());
		} catch (Exception e) {
			log.error("Excepion in activate method of MemberBenefitsListingServiceImpl :: {}", e.getMessage());
		}

	}

	@Deactivate
	protected void deactivate(ComponentContext ctx) {
		session.logout();
		log.debug("Deactivated MemberBenefitsListingServiceImpl");
		if (resourceResolver != null) {
			resourceResolver.close();
		}
	}

	@Override
	public void benefitsList(String propertyName, String pagePath, String membershipPath) {

		certificateList = new ArrayList<>();
		cardBenefitsList = new ArrayList<>();
		String membershipTypePath = membershipPath + tlcConstants.SLASH + propertyName + tlcConstants.LEVEL_1;
		try {
			Iterator<Page> childrenPages = null;

			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page rootPage = pageManager.getPage(membershipTypePath);
				log.debug("rootPage :: {} ", rootPage.getName());
				if (rootPage instanceof Page) {
					childrenPages = rootPage.listChildren();
				}
			}

			while (childrenPages.hasNext()) {

				Page childPage = childrenPages.next();
				String childPageName = childPage.getName();
				log.debug(" chile page name is:: {}", childPageName);
				if (StringUtils.equalsIgnoreCase(childPage.getName(), tlcConstants.CERTIFICATES)
						|| StringUtils.equalsIgnoreCase(childPage.getName(), tlcConstants.CARD_BENEFITS)) {

					Iterator<Page> subChildrenPages = childPage.listChildren();
					while (subChildrenPages.hasNext()) {

						Page subChildPage = subChildrenPages.next();

						Iterator<Page> childPagesHoldingTags = subChildPage.listChildren();

						while (childPagesHoldingTags.hasNext()) {

							Page childPageHoldingTags = childPagesHoldingTags.next();

							if (childPageHoldingTags instanceof Page) {

								if (StringUtils.equalsIgnoreCase(childPage.getName(), tlcConstants.CERTIFICATES)) {
									Resource certificateResource = childPageHoldingTags.getContentResource()
											.getChild(tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH
													+ tlcConstants.CERTIFICATE);

									if (certificateResource instanceof Resource) {

										CertificateModel certificateDetail = certificateResource
												.adaptTo(CertificateModel.class);
										String parentCertificatePage = childPageHoldingTags.getParent().getName();

										String termsConditionsPath = pagePath + tlcConstants.SLASH
												+ tlcConstants.TERMS_CONDITIONS + tlcConstants.HTML_EXT
												+ tlcConstants.SLASH + propertyName + tlcConstants.SLASH
												+ parentCertificatePage + tlcConstants.SLASH
												+ childPageHoldingTags.getName();
										certificateDetail.setTermsConditionsUrl(termsConditionsPath);
										certificateList.add(certificateDetail);
									}
								}

								if (StringUtils.equalsIgnoreCase(childPage.getName(), tlcConstants.CARD_BENEFITS)) {

									Resource cardBenefitResource = childPageHoldingTags.getContentResource()
											.getChild(tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH
													+ tlcConstants.CARDBENEFIT);

									if (cardBenefitResource instanceof Resource) {
										CardBenefitsModel cardBenefitsDetail = cardBenefitResource
												.adaptTo(CardBenefitsModel.class);
										cardBenefitsList.add(cardBenefitsDetail);
									}
								}

							}

						}
					}

				}

			}

		} catch (Exception e) {
			log.error("Exception in getBenefitsList method :: {}", e);
		}

	}

	@Override
	public List<SignatureCollectionVO> signatureCollectionsList(String propertyName, String city,
			String signatureCollectionPath, String pagePath, String pageParentName) {

		String subBrandTag = null;
		String pageUrl = "";
		signatureCollectionList = new ArrayList<>();
		if (StringUtils.equals(pageParentName, tlcConstants.HOME)
				|| StringUtils.equals(pageParentName, tlcConstants.EXPERIENCE)) {
			pageUrl = pagePath + tlcConstants.SLASH
					+ tlcConstants.EXPERIENCES +tlcConstants.SLASH
					+ tlcConstants.SIGNATURE_CATALOGUE + tlcConstants.HTML_EXT;
			log.debug("pageUrl:: {} ", pageUrl);
		}
		if (StringUtils.equals(pageParentName, tlcConstants.HOTELS_AND_RESORTS)) {
			pageUrl = pagePath + tlcConstants.SLASH + tlcConstants.EXPERIENCES + tlcConstants.SLASH + 
					tlcConstants.SIGNATURE_CATALOGUE + tlcConstants.HTML_EXT;
			log.debug("pageUrl:: {} ", pageUrl);
		}

		try {
			Iterator<Page> childrenPages = null;
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page rootPage = pageManager.getPage(signatureCollectionPath);

				if (rootPage instanceof Page) {
					childrenPages = rootPage.listChildren();
				}
			}

			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			while (childrenPages.hasNext()) {

				Page childPage = childrenPages.next();
				if (childPage instanceof Page) {

					SignatureCollectionVO signatureCollectionVO = new SignatureCollectionVO();
					Resource subChildPageResource = childPage.getContentResource();

					if (subChildPageResource instanceof Resource && tagManager != null) {
						Tag[] tags = tagManager.getTags(subChildPageResource);
						for (Tag tag : tags) {
							String tagId = tag.getTagID();
							if (tagId.contains(tlcConstants.SUBBRAND_TAG)) {
								subBrandTag = tag.getTitle();
								log.debug("subBrandTagId :: {}", subBrandTag);

							}

						}

						for (Tag tag : tags) {
							String tagId = tag.getTagID();

							if (StringUtils.isNotBlank(propertyName) && StringUtils.isBlank(city)
									&& tagId.contains(propertyName)) {

								Resource responsiveGrid = subChildPageResource
										.getChild(tlcConstants.RESPONSIVE_GRID_PATH);
								Iterable<Resource> childrenResponsiveGrid = responsiveGrid.getChildren();

								for (Resource childResponsiveGrid : childrenResponsiveGrid) {

									if (childResponsiveGrid instanceof Resource) {

										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_1)) {

											DetailPageLayout1Model detailPageLayout1Model = childResponsiveGrid
													.adaptTo(DetailPageLayout1Model.class);
											detailPageLayout1Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("getpageUrl :{}", detailPageLayout1Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout1(detailPageLayout1Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}
										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_2)) {

											DetailPageLayout2Model detailPageLayout2Model = childResponsiveGrid
													.adaptTo(DetailPageLayout2Model.class);
											detailPageLayout2Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											signatureCollectionVO.setDetailPageLayout2(detailPageLayout2Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_3)) {

											DetailPageLayout3Model detailPageLayout3Model = childResponsiveGrid
													.adaptTo(DetailPageLayout3Model.class);
											detailPageLayout3Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("getpageUrl :{}", detailPageLayout3Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout3(detailPageLayout3Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_4)) {

											DetailPageLayout4Model detailPageLayout4Model = childResponsiveGrid
													.adaptTo(DetailPageLayout4Model.class);
											detailPageLayout4Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("getpageUrl :{}", detailPageLayout4Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout4(detailPageLayout4Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

										break;

									}

								}

								break;
							}

							if (StringUtils.isBlank(propertyName) && StringUtils.isBlank(city)
									&& tagId.equals(tlcConstants.FEATURE_TAG)) {
								log.debug("city and property name both are null");
								log.debug("pageUrl::{}", pageUrl);
								Resource responsiveGrid = subChildPageResource
										.getChild(tlcConstants.RESPONSIVE_GRID_PATH);
								Iterable<Resource> childrenResponsiveGrid = responsiveGrid.getChildren();
								for (Resource childResponsiveGrid : childrenResponsiveGrid) {
									if (childResponsiveGrid instanceof Resource) {
										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_1)) {

											DetailPageLayout1Model detailPageLayout1Model = childResponsiveGrid
													.adaptTo(DetailPageLayout1Model.class);
											detailPageLayout1Model.setPageUrl(
													pageUrl	+ tlcConstants.SLASH + childPage.getName());
											log.debug("detailPageLayout1Model.getPageUrl - exp page::{}",
													detailPageLayout1Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout1(detailPageLayout1Model);

											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}
										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_2)) {

											DetailPageLayout2Model detailPageLayout2Model = childResponsiveGrid
													.adaptTo(DetailPageLayout2Model.class);
											detailPageLayout2Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("page Path :: {} ", detailPageLayout2Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout2(detailPageLayout2Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_3)) {

											DetailPageLayout3Model detailPageLayout3Model = childResponsiveGrid
													.adaptTo(DetailPageLayout3Model.class);
											detailPageLayout3Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("page Path :: {} ", detailPageLayout3Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout3(detailPageLayout3Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

										if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
												tlcConstants.DETAIL_PAGE_4)) {

											DetailPageLayout4Model detailPageLayout4Model = childResponsiveGrid
													.adaptTo(DetailPageLayout4Model.class);
											detailPageLayout4Model.setPageUrl(
													pageUrl + tlcConstants.SLASH + childPage.getName());
											log.debug("page Path :: {} ", detailPageLayout4Model.getPageUrl());
											signatureCollectionVO.setDetailPageLayout4(detailPageLayout4Model);
											signatureCollectionVO.setSubBrandTag(subBrandTag);
											signatureCollectionList.add(signatureCollectionVO);

										}

									} else {
										break;
									}
								}
							}

							if (StringUtils.isNotBlank(city)) {
								log.debug("city is not blank");
								log.debug("pageUrl city!=null:{}", pageUrl);
								String cityTag = tlcConstants.CITY_TAG + "/" + city;

								if (tag.getTagID().equals(cityTag)) {
									Resource responsiveGrid = subChildPageResource
											.getChild(tlcConstants.RESPONSIVE_GRID_PATH);
									Iterable<Resource> childrenResponsiveGrid = responsiveGrid.getChildren();
									for (Resource childResponsiveGrid : childrenResponsiveGrid) {
										if (childResponsiveGrid instanceof Resource) {

											if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
													tlcConstants.DETAIL_PAGE_1)) {
												DetailPageLayout1Model detailPageLayout1Model = childResponsiveGrid
														.adaptTo(DetailPageLayout1Model.class);
												detailPageLayout1Model.setPageUrl(pageUrl + tlcConstants.SLASH
													+ childPage.getName());
												log.debug("Page Path when city!=null:: {} ",
														detailPageLayout1Model.getPageUrl());
												signatureCollectionVO.setDetailPageLayout1(detailPageLayout1Model);
												signatureCollectionVO.setSubBrandTag(subBrandTag);
												signatureCollectionList.add(signatureCollectionVO);

											}

											if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
													tlcConstants.DETAIL_PAGE_2)) {

												DetailPageLayout2Model detailPageLayout2Model = childResponsiveGrid
														.adaptTo(DetailPageLayout2Model.class);
												detailPageLayout2Model.setPageUrl(pageUrl + tlcConstants.SLASH
														+ childPage.getName());
												log.debug("Page Path when city!=null:: {} ",
														detailPageLayout2Model.getPageUrl());

												signatureCollectionVO.setDetailPageLayout2(detailPageLayout2Model);
												signatureCollectionVO.setSubBrandTag(subBrandTag);
												signatureCollectionList.add(signatureCollectionVO);

											}

											if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
													tlcConstants.DETAIL_PAGE_3)) {
												DetailPageLayout3Model detailPageLayout3Model = childResponsiveGrid
														.adaptTo(DetailPageLayout3Model.class);
												detailPageLayout3Model.setPageUrl(pageUrl + tlcConstants.SLASH
														+ childPage.getName());
												log.debug("Page Path when city!=null:: {} ",
														detailPageLayout3Model.getPageUrl());
												signatureCollectionVO.setDetailPageLayout3(detailPageLayout3Model);
												signatureCollectionVO.setSubBrandTag(subBrandTag);
												signatureCollectionList.add(signatureCollectionVO);

											}

											if (StringUtils.equalsIgnoreCase(childResponsiveGrid.getName(),
													tlcConstants.DETAIL_PAGE_4)) {
												DetailPageLayout4Model detailPageLayout4Model = childResponsiveGrid
														.adaptTo(DetailPageLayout4Model.class);
												detailPageLayout4Model.setPageUrl(pageUrl + tlcConstants.SLASH
														+ childPage.getName());
												log.debug("Page Path when city!=null:: {} ",
														detailPageLayout4Model.getPageUrl());
												signatureCollectionVO.setDetailPageLayout4(detailPageLayout4Model);
												signatureCollectionVO.setSubBrandTag(subBrandTag);
												signatureCollectionList.add(signatureCollectionVO);

											}

										}

										break;

									}

								}

							}

						}

					}

					continue;
				}
			}

		} catch (Exception e) {
			log.error("Exception in getBenefitsList method :: ", e);
		}
		log.debug("signature Collection List size :: {}", signatureCollectionList.size());
		return signatureCollectionList;

	}

	@Override
	public List<TermsConditionsModel> getMembershipTermsList(String propertyName, String membershipPath) {
		try {
			membershipTermsList = new ArrayList<>();

			String membershipTypePath = membershipPath + tlcConstants.SLASH + propertyName + tlcConstants.LEVEL_1;

			Iterator<Page> childrenPages = null;
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page rootPage = pageManager.getPage(membershipTypePath);

				if (rootPage instanceof Page) {
					// certificates,card-benefits,terms and conditions etc
					childrenPages = rootPage.listChildren();
					while (childrenPages.hasNext()) {

						Page childPage = childrenPages.next();
						if (StringUtils.equalsIgnoreCase(childPage.getName(), tlcConstants.TERMS_AND_CONDITIONS)) {

							Iterator<Page> subChildrenPages = childPage.listChildren();

							while (subChildrenPages.hasNext()) {

								Page subChildPage = subChildrenPages.next();

								if (StringUtils.equalsIgnoreCase(subChildPage.getName(),
										tlcConstants.TERMS_AND_CONDITIONS_COMPONENT)) {

									Resource membershipTermsResource = subChildPage.getContentResource()
											.getChild(tlcConstants.RESPONSIVE_GRID_PATH + tlcConstants.SLASH
													+ tlcConstants.MEMBERSHIPTERMS);
									TermsConditionsModel termsConditionsModel = membershipTermsResource
											.adaptTo(TermsConditionsModel.class);
									termsConditionsModel.getGroup1Features();
									membershipTermsList.add(termsConditionsModel);
								}
							}

						}
					}

				}

			}
		} catch (Exception e) {
			log.error("Exception in getBenefitsList method :: ", e);
		}

		return membershipTermsList;

	}

	@Override
	public List<CertificateModel> getCertificateList() {
		return certificateList;
	}

	@Override
	public List<CardBenefitsModel> getCardBenefitsList() {
		return cardBenefitsList;
	}

}
