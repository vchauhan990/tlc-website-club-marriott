package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.AccordianVO;
import com.tlc.web.platform.core.vo.PageVO;

/**
 * 
 * Sling Implementation of Sitemap Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class SiteMapModel {

	private static Logger log = LoggerFactory.getLogger(SiteMapModel.class);

	@Inject
	@Optional
	@Default(values = "{}")
	private String[] pageItems;

	private List<PageVO> pageList;

	@PostConstruct
	public void init() {

		Gson gson = new Gson();

		try {
			if (pageItems.length > 0) {

				pageList = new ArrayList<>();
				
				for (String eachString : pageItems) {

					PageVO eachItem = gson.fromJson(eachString, PageVO.class);
					
					
					pageList.add(eachItem);
				}
			}

		} catch (Exception e) {
			log.error("Exception in accordian model:::{}", e);
		}
	}

	public List<PageVO> getFaqList() {
		return pageList;
	}

}