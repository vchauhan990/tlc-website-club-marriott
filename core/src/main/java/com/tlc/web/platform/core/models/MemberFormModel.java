package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tlc.web.platform.core.models.ListOfLinksItemModel.ListOfLinksFirst;

/**
 * 
 * Sling Implementation of Member Form Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class MemberFormModel {

	private static Logger log = LoggerFactory.getLogger(MemberFormModel.class);

	@Inject
	List<ListOfLinksFirst> countryCode;
	
	@Inject
	List<ListOfLinksFirst> gender;
	
	@Inject
	List<ListOfLinksFirst> contactHow;
	
	@Inject
	List<ListOfLinksFirst> natureOfQuery;
	
	@PostConstruct
	public void init() {
		log.info("Member form model Invoked");
	}
	
	public List<ListOfLinksFirst> getCountryCode() {
		return countryCode;
	}

	public List<ListOfLinksFirst> getGender() {
		return gender;
	}
	
	public List<ListOfLinksFirst> getContactHow() {
		return contactHow;
	}

	public List<ListOfLinksFirst> getNatureOfQuery() {
		return natureOfQuery;
	}
}