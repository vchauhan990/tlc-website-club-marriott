package com.tlc.web.platform.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.Banner;

/**
 * 
 * Sling Implementation of BannerModel
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BannerModel {

	/** Logger */
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Inject
	@Optional
	private String bannerType;
	
	@Inject
	@Optional
	@Default(values = "image")
	private String backgroundType;
	
	@Inject
	@Optional
	private String altText;
	
	@Inject
	@Optional
	private String desktopImage;
	
	@Inject
	@Optional
	private String headingType;

	@Inject
	@Optional
	private String heading;
	
	@Inject
	@Optional
	private String subHeading;
	
	@Inject
	@Optional
	private String description;
	
	@Inject
	@Optional
	private String ctaUrl;

	@Inject
	@Optional
	private String ctaText;

	@Inject
	@Optional
	private boolean openInNewWindow;

	@Inject
	@Optional @Default(values = "center")
	private String align;

	@Inject
	@Optional @Default(values = "bottomAlign")
	private String horizontalAlign;
	
	@Inject
	@Optional
	private String backgroundVideo;

	@Inject
	@Optional
	private String imgPath;

	@Inject
	@Optional
	private String srcPath;
	
	@Inject
	@Optional
	private String imgCopyrightText;
	
	private Banner banner;
	
	@Inject
	@Optional
	@Default(values = "{}")
	private String[] ctaItems;

	private List<Banner> ctaList;

	@PostConstruct
	protected void init() {

		try {
			Gson gson = new Gson();
			if (ctaItems.length > 0) {
				ctaList = new ArrayList<>();
				// for CTA Items
				for (String eachString : ctaItems) {
					Banner eachItem = gson.fromJson(eachString, Banner.class);
					ctaList.add(eachItem);
				}
			}

		} catch (Exception e) {
			log.error("Exception in init method of BannerModelImpl :: {}", e);
		}

	}

	public List<Banner> getCtaList() {
		return ctaList;
	}

	public Banner getBanner() {
		return banner;
	}

	public String getAltText() {
		return altText;
	}

	public String getHeading() {
		return heading;
	}

	public String getDesktopImage() {
		return desktopImage;
	}

	public String getCtaUrl() {
		return ctaUrl;
	}

	public String getCtaText() {
		return ctaText;
	}

	public boolean getOpenInNewWindow() {
		return openInNewWindow;
	}

	public String getAlign() {
		return align;
	}

	public String getHeadingType() {
		return headingType;
	}

	public String getBackgroundVideo() {
		return backgroundVideo;
	}

	public String getImgPath() {
		return imgPath;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public String getBackgroundType() {
		return backgroundType;
	}

	public String getDescription() {
		return description;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public String getBannerType() {
		return bannerType;
	}

	public String getImgCopyrightText() {
		return imgCopyrightText;
	}

	public String getHorizontalAlign() {
		return horizontalAlign;
	}


}