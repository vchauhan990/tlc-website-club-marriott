package com.tlc.web.platform.core.models.dynamic;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Dynamic Owl Carousel Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class DynamicOwlCarouselModel {

	private static final Logger log = LoggerFactory.getLogger(DynamicOwlCarouselModel.class);

	@Inject
	PromotionalContentFetchService promoContentFetchService;

	@Inject
	SearchService searchService;

	@Inject
	SlingHttpServletRequest request;

	Map<String, String> promotionalHotelLocationsList = null;

	PropertyDetailsModel propertyDetailsModel;
	List<PropertyDetailsModel> propertyDetailsList;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	Page currentPage;

	@Inject
	ResourceResolver resourceResolver;

	@PostConstruct
	public void init() {

		
		String pagePath = "" ;
		
		if(StringUtils.equals(currentPage.getParent().getName(), tlcConstants.HOME)) {
			 pagePath = currentPage.getPath();
		} else {
			 pagePath = currentPage.getParent().getParent().getPath();
		}
		ValueMap vm = currentPage.getContentResource().getValueMap();

		String cityPath = vm.get("cityPath", String.class);
		String uniqueExperiencePath = vm.get("uniqueExperiencePath", String.class);
		String signatureCollectionPath = vm.get("signatureCollectionPath", String.class);

		requestPathInfo = request.getRequestPathInfo();
		log.debug("requestPathInfo-12 :: {} ", requestPathInfo.getSuffix());

		String[] segments = requestPathInfo.getSuffix().split("/");

		String promotionType = currentPage.getName();
		log.info("promotionType::{}" , promotionType);
		String promotionName = segments[segments.length - 1];

		if (StringUtils.isNotBlank(promotionType) && StringUtils.equals(promotionType, tlcConstants.UNIQUE_EXPERIENCE_CATALOGUE)) {
			try {
				propertyDetailsList = new LinkedList<>();
				promotionalHotelLocationsList = promoContentFetchService.getExperienceHotelLocations(promotionName,
						promotionType, uniqueExperiencePath);

				for (Map.Entry<String, String> entry : promotionalHotelLocationsList.entrySet()) {

					String city = entry.getKey();
					String property = entry.getValue();

					Resource propertyResource = resourceResolver.getResource(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + property);

					if (propertyResource instanceof Resource) {
						propertyDetailsModel = searchService.getPropertyDetails(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + property);
						propertyDetailsModel.setPageUrl(pagePath + tlcConstants.SLASH +  tlcConstants.HOTELS_AND_RESORTS +  tlcConstants.SLASH+ tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT + tlcConstants.SLASH + city + tlcConstants.SLASH + property);
						propertyDetailsList.add(propertyDetailsModel);
					} else {
						continue;
					}
					log.debug("size:: {}", propertyDetailsList.size());
				}

			} catch (Exception e1) {
				log.error("Exception in Unique experiences DynamicOwlCarouselModel ::{}", e1);
			}

		} else if (StringUtils.isNotBlank(promotionType)
				&& StringUtils.equals(promotionType, tlcConstants.SIGNATURE_CATALOGUE )) {
			try {
				propertyDetailsList = new LinkedList<>();
				promotionalHotelLocationsList = promoContentFetchService.getExperienceHotelLocations(promotionName,
						promotionType, signatureCollectionPath);

				for (Map.Entry<String, String> entry : promotionalHotelLocationsList.entrySet()) {

					String city = entry.getKey();
					String property = entry.getValue();

					Resource propertyResource = resourceResolver.getResource(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + property);

					if (propertyResource instanceof Resource) {
						propertyDetailsModel = searchService.getPropertyDetails(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + property);
						propertyDetailsModel.setPageUrl(pagePath + tlcConstants.SLASH +  tlcConstants.HOTELS_AND_RESORTS +tlcConstants.SLASH + tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT + tlcConstants.SLASH + city + tlcConstants.SLASH + property);
						log.info("propertyDetailsModel :: {}", propertyDetailsModel.getPropertyName());
						propertyDetailsList.add(propertyDetailsModel);

					} else {
						continue;
					}
				}
			} catch (Exception e2) {
				log.error("Exception in signature-Collections DynamicOwlCarouselModel ::{}", e2);
			}

		}
	}

	public PropertyDetailsModel getPropertyDetailsModel() {
		return propertyDetailsModel;
	}

	public List<PropertyDetailsModel> getPropertyDetailsList() {
		return propertyDetailsList;
	}

}