package com.tlc.web.platform.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.tlc.web.platform.core.services.SearchService;

@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "PropertySearch  servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/form/hotelSearch"
		})
public class PropertySearchServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PropertySearchServlet.class);

	@Reference
	SearchService searchservice;
	
	private Map<String, String> cityList = new LinkedHashMap<>();
	
/*	private String cityMap = null;
	
	Gson gson;*/
	
	@Activate
	protected void activate() {
		log.info("Property search servlet activated!");
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		/*response.setContentType("application/json");*/
		try {

			JSONObject js=new JSONObject();
			String modeType = request.getParameter("modeType");
			if(StringUtils.equalsIgnoreCase(modeType, "cities")) {
				String cityPath = request.getParameter("cityPath");
				cityList = searchservice.fetchCityList(cityPath);
				cityList.entrySet().forEach(entry->{
				    log.info(entry.getKey() + " " + entry.getValue());  
				 });
				log.info("cityList.entrySet() :: {}", cityList.entrySet());
			}
			/*cityMap = gson.toJson(cityList);*/
			js.put("message", cityList);
			js.put("status", "200");
			response.setStatus(200);
			out.println(js);
			out.close();
			
		} catch (NullPointerException ne) {
			log.debug("NullPointerException in PromotionContentServlet :: {}", ne);
		} catch (JSONException jse) {
			log.debug("JSONException in PromotionContentServlet :: {}", jse);
		} catch(Exception e) {
			log.info("Exception in Property search :: {}" , e);
		}
	}

}
