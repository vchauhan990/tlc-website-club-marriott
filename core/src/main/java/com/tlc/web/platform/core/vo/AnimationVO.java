package com.tlc.web.platform.core.vo;

public class AnimationVO {
	
	private String imagePath;
	private String imageAltText;
	private String title;
	private String description;
	private String iconImagePath;
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getImageAltText() {
		return imageAltText;
	}
	public void setImageAltText(String imageAltText) {
		this.imageAltText = imageAltText;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconImagePath() {
		return iconImagePath;
	}
	public void setIconImagePath(String iconImagePath) {
		this.iconImagePath = iconImagePath;
	}
	

}
