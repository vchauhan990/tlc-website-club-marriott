package com.tlc.web.platform.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import com.tlc.web.platform.core.util.LinkUtil;

/**
 * 
 * Sling Implementation of Navigation Item Model
 * 
 * @author DWAO
 *
 */

public interface NavigationItemModel {

	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class NavigationItemFirst {

		@Inject
		public String navigationTitle;

		@Inject
		public String navigationLink;

		@PostConstruct
		public void init() {
			
			navigationLink = LinkUtil.getFormattedURL(navigationLink);
		}
		public String getNavigationTitle() {
			return navigationTitle;
		}

		public String getNavigationLink() {
			return navigationLink;
		}

	}

}
