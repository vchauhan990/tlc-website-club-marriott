package com.tlc.web.platform.core.vo;

import java.util.List;

import com.tlc.web.platform.core.util.LinkUtil;

/**
 * Banner VO
 *
 * @author DWAO
 *
 */

public class Banner {
	
	private String backgroundType;
	private String bannerType;
	private String backgroundImagePath; 
	private String backgroundImageAltText;
	private String bgVideoSrcPath;
	private String backgroundVideo;
	private String heading;
	private String headingType;
	private String subHeading;
	private String description;
	private String imagePath;
	private List<CTA> ctaItems;
	private String ctaText;
	private String ctaLink;
	private String desktopImage;
	private String tabImage;
	private String mobileImage;
	private String imageAltText;
	private String customStyleClass;
	private String includeForm;
	private boolean openInNewWindow;
	private List<Banner> ctaList;
	private String altText;
	private String ctaUrl;
	private String bannerPagePath;
	private String align;
	private String horizontalAlign;
	private String text;
	private String videoPath;
	private String imgPath;
	private String srcPath;
	private String imgCopyrightText;
	
	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public String getImpPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	private String ctaType;

	public String getAltText() {
		return altText;
	}

	public void setAltText(String altText) {
		this.altText = altText;
	}

	public String getBannerPagePath() {
		return bannerPagePath;
	}

	public void setBannerPagePath(String bannerPagePath) {
		this.bannerPagePath = bannerPagePath;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCtaType() {
		return ctaType;
	}

	public void setCtaType(String ctaType) {
		this.ctaType = ctaType;
	}
	
	public String getBannerType() {
		return bannerType;
	}
	
	public void setBannerType(String bannerType) {
		this.bannerType = bannerType;
	}
	
	public String getBackgroundType() {
		return backgroundType;
	}
	
	public void setBackgroundType(String backgroundType) {
		this.backgroundType = backgroundType;
	}
	
	public String getBackgroundImagePath() {
		return backgroundImagePath;
	}
	
	public void setBackgroundImagePath(String backgroundImagePath) {
		this.backgroundImagePath = backgroundImagePath;
	}
	
	public String getBackgroundImageAltText() {
		return backgroundImageAltText;
	}
	
	public void setBackgroundImageAltText(String backgroundImageAltText) {
		this.backgroundImageAltText = backgroundImageAltText;
	}
	
	public String getBackgroundVideo() {
		return backgroundVideo;
	}
	
	public void setBackgroundVideo(String backgroundVideo) {
		this.backgroundVideo = backgroundVideo;
	}
	
	public String getHeading() {
		return heading;
	}
	
	public void setHeading(String heading) {
		this.heading = heading;
	}
	
	public String getHeadingType() {
		return headingType;
	}
	
	public void setHeadingType(String headingType) {
		this.headingType = headingType;
	}
	
	public String getSubHeading() {
		return subHeading;
	}
	
	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public List<CTA> getCtaItems() {
		return ctaItems;
	}
	
	public void setCtaItems(List<CTA> ctaItems) {
		this.ctaItems = ctaItems;
	}
	
	public String getCustomStyleClass() {
		return customStyleClass;
	}
	
	public void setCustomStyleClass(String customStyleClass) {
		this.customStyleClass = customStyleClass;
	}

	public String getCtaText() {
		return ctaText;
	}
	
	public void setCtaText(String ctaText) {
		this.ctaText = ctaText;
	}
	
	public String getCtaLink() {
		return LinkUtil.getFormattedURL(ctaLink);
	}
	
	public void setCtaLink(String ctaLink) {
		this.ctaLink = ctaLink;
	}
	
	public String getDesktopImage() {
		return desktopImage;
	}
	
	public void setDesktopImage(String desktopImage) {
		this.desktopImage = desktopImage;
	}
	
	public String getTabImage() {
		return tabImage;
	}
	
	public void setTabImage(String tabImage) {
		this.tabImage = tabImage;
	}
	
	public String getMobileImage() {
		return mobileImage;
	}
	
	public void setMobileImage(String mobileImage) {
		this.mobileImage = mobileImage;
	}

	public String getImageAltText() {
		return imageAltText;
	}
	
	public void setImageAltText(String imageAltText) {
		this.imageAltText = imageAltText;
	}

	public String getIncludeForm() {
		return includeForm;
	}
	
	public void setIncludeForm(String includeForm) {
		this.includeForm = includeForm;

	}
	
	public String getBgVideoSrcPath() {
		return bgVideoSrcPath;
	}
	
	public void setBgVideoSrcPath(String bgVideoSrcPath) {
		this.bgVideoSrcPath = bgVideoSrcPath;
	}

	public String getCtaUrl() {
		return LinkUtil.getFormattedURL(ctaUrl);
	}

	public void setCtaUrl(String ctaUrl) {
		this.ctaUrl = ctaUrl;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public List<Banner> getCtaList() {
		return ctaList;
	}

	public void setCtaList(List<Banner> ctaList) {
		this.ctaList = ctaList;
	}

	public boolean isOpenInNewWindow() {
		return openInNewWindow;
	}

	public void setOpenInNewWindow(boolean openInNewWindow) {
		this.openInNewWindow = openInNewWindow;
	}

	public String getImgCopyrightText() {
		return imgCopyrightText;
	}

	public void setImgCopyrightText(String imgCopyrightText) {
		this.imgCopyrightText = imgCopyrightText;
	}

	public String getHorizontalAlign() {
		return horizontalAlign;
	}

	public void setHorizontalAlign(String horizontalAlign) {
		this.horizontalAlign = horizontalAlign;
	}
			
}