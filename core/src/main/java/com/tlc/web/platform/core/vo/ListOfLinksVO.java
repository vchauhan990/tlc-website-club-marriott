package com.tlc.web.platform.core.vo;

import java.util.List;

import com.tlc.web.platform.core.util.LinkUtil;

public class ListOfLinksVO {

	private String text;
	private String url;
	private String openInNewWindow;
	private String heading;
	private String subHeading;
	private String description;
	private String imageAltText;
	private String srcPath;
	private String backgroundImage;
	private String includeForm;
	private  List<ListOfLinksVO> ctaList;
	private String logoPath;

	public List<ListOfLinksVO> getCtaList() {
		return ctaList;
	}

	public void setCtaList(List<ListOfLinksVO> ctaList) {
		this.ctaList = ctaList;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getImageAltText() {
		return imageAltText;
	}

	public void setImageAltText(String imageAltText) {
		this.imageAltText = imageAltText;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return LinkUtil.getFormattedURL(url);
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOpenInNewWindow() {
		return openInNewWindow;
	}

	public void setOpenInNewWindow(String openInNewWindow) {
		this.openInNewWindow = openInNewWindow;
	}

	public String getIncludeForm() {
		return includeForm;
	}

	public void setIncludeForm(String includeForm) {
		this.includeForm = includeForm;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

}
