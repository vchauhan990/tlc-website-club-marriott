package com.tlc.web.platform.core.models;

import org.apache.sling.api.resource.Resource;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import javax.inject.Inject;

import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * Sling Model Implementation of Heading Model
 *
 * @author DWAO
 *
 */

@Model(adaptables=Resource.class, defaultInjectionStrategy = OPTIONAL)
public class HeadingModel {
	
	@Inject @Optional
	private String headingText;
	
	@Inject @Optional
	private String headingType;
	
	@Inject @Optional
	private String subHeadingType;
	
	@Inject @Optional
	private String align;
	
	@Inject @Optional
	private String subHeadingText;
	
	@Inject @Optional
	private String descText;
	
	@Inject @Optional
	@Default(values = "excludeContainer")
	
	private String container;

	public String getHeadingText() {
		return headingText;
	}

	public void setHeadingText(String headingText) {
		this.headingText = headingText;
	}

	public String getHeadingType() {
		return headingType;
	}

	public void setHeadingType(String headingType) {
		this.headingType = headingType;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}
	
	public String getSubHeadingText() {
		return subHeadingText;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public String getDescText() {
		return descText;
	}

	public void setDescText(String descText) {
		this.descText = descText;
	}

	public String getSubHeadingType() {
		return subHeadingType;
	}

	public void setSubHeadingType(String subHeadingType) {
		this.subHeadingType = subHeadingType;
	}
	
}