package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tlc.web.platform.core.models.ListOfLinksItemModel.ListOfLinksFirst;

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class ListofLinksModel {

	private static Logger log = LoggerFactory.getLogger(ListofLinksModel.class);

	@Inject
	public List<ListOfLinksFirst> listoflinks;

	@PostConstruct
	public void init() {
		log.debug("List of Links Invoked");	
	}

	public List<ListOfLinksFirst> getListitems() {
		
		return listoflinks;
	}

}
