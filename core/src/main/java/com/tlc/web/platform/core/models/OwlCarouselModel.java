package com.tlc.web.platform.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.MemberBenefitsListingService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * 
 * Sling Implementation of Owl Carousel Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = {SlingHttpServletRequest.class,Resource.class},defaultInjectionStrategy =DefaultInjectionStrategy.OPTIONAL)
public class OwlCarouselModel {
	
	private static Logger log = LoggerFactory.getLogger(OwlCarouselModel.class);

	@Inject
	MemberBenefitsListingService memberBenefitsListingService;
	
	@Inject
	Page currentPage;
	private List<SignatureCollectionVO> signatureCollectionList;
	
	String signatureCollectionPath="";
	String pagePath="";
	
	@PostConstruct
	public void init() {
		try {
			
			String propertyName = null;
			String city = null;
			pagePath = currentPage.getParent().getPath();
			
			ValueMap vm = currentPage.getContentResource().getValueMap();
		
			signatureCollectionPath = vm.get("signatureCollectionPath", String.class);
			log.debug("signaturecollectionPath :: {} ",signatureCollectionPath);

			signatureCollectionList = memberBenefitsListingService.signatureCollectionsList(propertyName,city,signatureCollectionPath,pagePath,tlcConstants.HOME);
			log.debug("signatureCollectionList :: {} ",signatureCollectionList.get(0).getDetailPageLayout1().getPageUrl());

		} catch (Exception e) {
			log.error("Exception in Owl Carousel Model  :: {}", e);
		}
	}
	
	public String getSignatureCollectionPath() {
		return signatureCollectionPath;
	}
	
	public List<SignatureCollectionVO> getSignatureCollectionList() {
		return signatureCollectionList;
	}
		
	public String getPagePath() {
		return pagePath;
	}

}
