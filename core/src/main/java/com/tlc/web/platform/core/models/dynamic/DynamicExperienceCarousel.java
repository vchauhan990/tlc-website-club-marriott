package com.tlc.web.platform.core.models.dynamic;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * 
 * Sling Implementation of Dynamic Experience Carousel
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class DynamicExperienceCarousel {

	private static final Logger log = LoggerFactory.getLogger(DynamicExperienceCarousel.class);

	@Inject
	PromotionalContentFetchService promotionalContentFetchService;

	@Inject
	SlingHttpServletRequest request;
	
	@Inject
	RequestPathInfo requestPathInfo;
	
	@Inject
	Page currentPage;
	
	private List<SignatureCollectionVO> expCarousel = null;

	private int expCarouselSize;
	String pagePath = null;

	@PostConstruct
	public void init() {

		try {
			
			pagePath=currentPage.getParent().getParent().getPath();
						
			ValueMap vm = currentPage.getContentResource().getValueMap();
			String uniqueExperiencePath = vm.get("uniqueExperiencePath", String.class);

			requestPathInfo = request.getRequestPathInfo();
			log.info("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());
			
			String[] segments = requestPathInfo.getSuffix().split("/");
			String propertyName = segments[segments.length - 1];
			String city = segments[segments.length - 2];
			
			expCarousel = promotionalContentFetchService.getExperiencesbyTag(city, propertyName,
					"get-experiences-by-city-property",pagePath,uniqueExperiencePath);
			expCarouselSize = expCarousel.size();
			log.debug(" expCarouselSize ::{}", expCarousel.size());

		} catch (Exception e) {
			log.error("exception in Dynamic Experience Carousel ::{}", e);
		}
	}

	public List<SignatureCollectionVO> getExpCarousel() {
		return expCarousel;
	}

	public int getExpCarouselSize() {
		return expCarouselSize;
	}
}