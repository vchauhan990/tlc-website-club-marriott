package com.tlc.web.platform.core.models.dynamic;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.aem.tlc.core.models.OutletModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Offers Events Listing Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class OffersEventsListingModel {

	private static final Logger log = LoggerFactory.getLogger(OffersEventsListingModel.class);

	@Inject
	private SlingHttpServletRequest request;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	Page currentPage;

	@Inject
	PromotionalContentFetchService promoContentFetchService;

	@Inject
	SearchService searchservice;

	List<OfferEventsModel> offerEventsList = null;

	OutletModel outletModel;

	DetailPageLayout1Model detailpagelayout;

	@PostConstruct
	public void init() {
		try {
			String pagePath = currentPage.getParent().getParent().getName() ;
			if(StringUtils.equals(pagePath, tlcConstants.HOME)) {
				 pagePath = currentPage.getParent().getParent().getPath();
			} else {
				 pagePath = currentPage.getParent().getPath();
			}
		/*	String pagePath = currentPage.getPath();*/

			
			requestPathInfo = request.getRequestPathInfo();
			log.info("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());
			ValueMap vm = currentPage.getContentResource().getValueMap();

			String offersEventPath = vm.get("offersEventPath", String.class);
			String[] segments = requestPathInfo.getSuffix().split("/");
			String city = segments[segments.length - 4];
			String propertyName = segments[segments.length - 3];
			String outletName = segments[segments.length - 2];
			String outletType = segments[segments.length - 1];
			String cityPath = vm.get("cityPath", String.class);

			Resource outletResource = searchservice
					.outletResource(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + propertyName + tlcConstants.SLASH + outletType + tlcConstants.SLASH + outletName);
			log.debug("outletResource ::{}", outletResource);
			
			if (StringUtils.equalsIgnoreCase(outletType, "dining")) {
				outletType = "dine";
			}

			offerEventsList = new ArrayList<>();
			offerEventsList = promoContentFetchService.getOffersEventsList("outlet", city, outletType, propertyName,
					outletName, offersEventPath, pagePath, null);

			outletModel = searchservice.getOutletCard(outletResource);
			detailpagelayout = searchservice.getDetailPageLayout1Card(outletResource);
			log.debug("Outlet Name :: {} ", outletModel.getOutletName());
		} catch (Exception e) {
			log.error("Exception in init method of offerEventsList :: {} ", e);
		}
	}

	public DetailPageLayout1Model getDetailpagelayout() {
		return detailpagelayout;
	}

	public OutletModel getOutletModel() {
		return outletModel;
	}

	public List<OfferEventsModel> getOfferEventsList() {
		return offerEventsList;
	}
}
