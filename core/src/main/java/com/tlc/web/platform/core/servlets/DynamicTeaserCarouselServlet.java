package com.tlc.web.platform.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.OfferEventsModel;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;

@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Dynamic Teaser Carousel Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/dynamicTeaserCarousel"
		})
public class DynamicTeaserCarouselServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(DynamicTeaserCarouselServlet.class);
	
	@Reference
	PromotionalContentFetchService promotionalService;
	
	private List<OfferEventsModel> featuredOffersList = null;
	
	@Activate
	protected void activate() {
		log.debug("Dynamic Teaser Carousel Servlet activated!");

	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		 PrintWriter out = response.getWriter();
				
			try {
				JSONObject js=new JSONObject();
				String cityName = request.getParameter("city");
				String offerEventPath = request.getParameter("offersEventPath");
				String cityPath=request.getParameter("cityPath");
				String pagePath = request.getParameter("pagePath");
				String city = cityName.toLowerCase();
		          
		         promotionalService.getOffersEventsList("featuredOffers",city,null,null,null,offerEventPath,pagePath,cityPath);
		         featuredOffersList = promotionalService.getFeaturedOffersWithCityList();
		          js.put("data", featuredOffersList);
				  js.put("status", "200");
				  out.print(js);
			
			} catch (NullPointerException ne) {
				log.debug("NullPointerException in PromotionContentServlet :: {}", ne);
			} catch (JSONException jse) {
				log.debug("JSONException in PromotionContentServlet :: {}", jse);
			} catch(Exception e) {
				log.debug("Exception in DynamicTeaserCarouselServlet :: {}" , e);
			}

	}

}
