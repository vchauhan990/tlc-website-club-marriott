package com.tlc.web.platform.core.servlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tlc.web.platform.core.services.MemberBenefitsListingService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.ResponseDTO;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "OwlCarousel servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/owlCarousel"
		})

public class OwlCarouselServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(OwlCarouselServlet.class);
		
		@Reference
		MemberBenefitsListingService memberBenefitsListingService;
		
		private List<SignatureCollectionVO> signatureCollectionList; 
		
		@Activate
		protected void activate() {
			log.debug("OwlCarouselServlet activated!");
		}
		
		@Override
		protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
				throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().disableHtmlEscaping().create();
			ResponseDTO responseDTO = new ResponseDTO();
			try {
				//JSONObject js=new JSONObject();
				String propertyName = null;
				String cityName = request.getParameter("city");
				String signatureCollectionsPath = request.getParameter("resourcePath");
				String pagePath = request.getParameter("pagePath");
				String city = cityName.toLowerCase();
				log.debug("pagePath::{}", pagePath);
				
				signatureCollectionList = memberBenefitsListingService.signatureCollectionsList(propertyName,city,signatureCollectionsPath,pagePath,tlcConstants.EXPERIENCE);
				responseDTO.setCode(200);
				responseDTO.setStatus("OK");
				responseDTO.setData(signatureCollectionList);
//				js.put("data", signatureCollectionList);
//				js.put("status", 200);
				String json = gson.toJson(responseDTO);
				//out.print(js);
				out.print(json);
				//out.print(js);
			
			} catch (NullPointerException ne) {
				log.error("NullPointerException in PromotionContentServlet :: {}", ne);
			} catch(Exception e) {
				log.error("Exception in OwlCarouselServlet :: {}" , e);
			}
			
		}

}
