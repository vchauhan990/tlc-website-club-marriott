package com.tlc.web.platform.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PropertyVO;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "=" + "City Search  servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/form/citySearch" })
public class CitySearchServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(CitySearchServlet.class);
	
	@Reference
	SearchService searchservice;
	
	
	@Reference
	PromotionalContentFetchService promotionContent;

	private List<PropertyVO> searchResultList = null;
	private List<PropertyDetailsModel> benefitsSearchResult = null;

	@Activate
	protected void activate() {
		log.debug("search Form servlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

		try {
			searchResultList = new ArrayList<>();

			log.debug("inside try");
			JSONObject js = new JSONObject();
			if (request.getParameter("modeType") != null) {

				String modeType = request.getParameter("modeType");
				if (StringUtils.equalsIgnoreCase(modeType, "city")) {
					String city = request.getParameter("cityName");
					String cityPath = request.getParameter("resourcePath") + tlcConstants.SLASH + city;
					String pagePath = request.getParameter("pagePath");
					searchResultList = searchservice.getPropertySearchResultsList(cityPath, pagePath);
					js.put("message", searchResultList);
				}
				if (StringUtils.equalsIgnoreCase(modeType, "participatingSearch")) {
				
					String city = request.getParameter("cityName");
					log.info("cityServlet 1 :: {} ",city);
					log.info(modeType);
					String cityPath = request.getParameter("resourcePath") + tlcConstants.SLASH + city;
					log.info("cityServlet 2 :: {} ",cityPath);
					String pagePath = request.getParameter("pagePath");
					benefitsSearchResult = promotionContent.getFeaturedHotelsList(cityPath, pagePath, "participatingHotel", city);
					js.put("message", benefitsSearchResult);
				}

			}
			log.debug("js ::{}" , js);
			response.setContentType("charset=UTF-8");
			js.put("status", "200");
			response.setStatus(200);
			out.println(js);
			out.close();

		} catch (Exception e) {
			log.debug("Exception in Property search :: {}", e);
		}
	}
}
