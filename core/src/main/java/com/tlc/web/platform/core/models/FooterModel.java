package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.Footer;

/**
 * 
 * Sling Implementation of Footer Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class FooterModel {
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(FooterModel.class);

	@Inject
	@Default(intValues = 1)
	private int footerLinksGridColumns;

	@Inject
	@Default(intValues = 1)
	private int hotelLogoColumns;
	
	@Inject
	private String brandLogoImage;

	@Inject
	private String copyrightText;
	
	@Inject
	@Default(values = "{}")
	private String[] listofItems;
	
	@Inject
	@Default(values = "{}")
	private String[] footerContactLinks;
	
	private List<String> footerLinksGridList;

	private List<String> topFooterLogoList;

	private List<Footer> footerContactLinksList;
	
	@PostConstruct
	public void init() {
	
		try{
			footerLinksGridList = new ArrayList<String>();

			for (int count = 1; count <= footerLinksGridColumns; count++) {
				footerLinksGridList.add("listOfLinks" + count);
			}

			topFooterLogoList = new ArrayList<>();
			
			for (int count = 1; count <= hotelLogoColumns; count++) {
				topFooterLogoList.add("listOfLogo" + count);
			}
			
			Gson gson = new Gson();
			if (footerContactLinks.length > 0) {
				footerContactLinksList = new LinkedList<>();
				
				for (String eachString : footerContactLinks) {
					Footer footerItem = gson.fromJson(eachString, Footer.class);
					footerContactLinksList.add(footerItem);
				}
			}		
		} catch (Exception e) {
			log.error("Exception in init method of FooterModelImpl : " , e);
			
		}
	}

	public String getBrandLogoImage() {
		return brandLogoImage;
	}

	public String getCopyrightText() {
		return copyrightText;
	}

	public List<String> getFooterLinksGridList() {
		return footerLinksGridList;
	}

	public List<String> getTopFooterLogoList() {
		return topFooterLogoList;
	}

	public List<Footer> getFooterContactLinksList() {
		return footerContactLinksList;
	}

}