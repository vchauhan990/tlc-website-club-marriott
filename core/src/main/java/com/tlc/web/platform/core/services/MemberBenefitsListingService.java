package com.tlc.web.platform.core.services;

import java.util.List;

import com.aem.tlc.core.models.CertificateModel;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;
import com.aem.tlc.core.models.CardBenefitsModel;
import com.aem.tlc.core.models.TermsConditionsModel;

public interface MemberBenefitsListingService {
	
	public void benefitsList(String propertyName,String pagePath,String membershipPath);
	public List<SignatureCollectionVO> signatureCollectionsList(String propertyName, String city,String signatureCollectionPath,String pagePath,String pageName)  ;
	public List<CertificateModel> getCertificateList();
	public List<CardBenefitsModel> getCardBenefitsList();
	public List<TermsConditionsModel> getMembershipTermsList(String propertyName,String membershipPath);

}
