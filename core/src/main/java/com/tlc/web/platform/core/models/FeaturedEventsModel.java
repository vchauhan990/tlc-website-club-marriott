package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.OfferEventsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Featured Events Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class FeaturedEventsModel {

	private static final Logger log = LoggerFactory.getLogger(FeaturedEventsModel.class);

	@Inject
	PromotionalContentFetchService promotionalContentFetchService;

	@Inject
	private List<OfferEventsModel> featuredEventsList = null;

	@Inject
	Page currentPage;

	String offersEventPath = "";

	String pagePath = "";

	@PostConstruct
	public void init() {

		try {

			pagePath = currentPage.getParent().getPath();

			String city = null;
			String outletType = null;
			ValueMap vm = currentPage.getContentResource().getValueMap();

			offersEventPath = vm.get("offersEventPath", String.class);

			String cityPath = vm.get("cityPath", String.class);
			featuredEventsList = promotionalContentFetchService.getOffersEventsList(tlcConstants.FEATURED_EVENTS, city, outletType,
					null, null, offersEventPath, pagePath, cityPath);

		} catch (Exception e) {
			log.error("exception in Featured Events Model ::{}", e);
		}
	}

	public void setOffersEventPath(String offersEventPath) {
		this.offersEventPath = offersEventPath;
	}

	public String getOffersEventPath() {
		return offersEventPath;
	}

	public String getPagePath() {
		return pagePath;
	}

	public List<OfferEventsModel> getFeaturedEventsList() {
		return featuredEventsList;
	}

}
