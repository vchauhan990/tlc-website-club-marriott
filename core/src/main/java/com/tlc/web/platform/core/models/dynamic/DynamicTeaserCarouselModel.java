package com.tlc.web.platform.core.models.dynamic;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.OfferEventsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;

/**
 * 
 * Sling Implementation of Dynamic Teaser Carousel Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class,
		Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DynamicTeaserCarouselModel {

	private static Logger log = LoggerFactory.getLogger(DynamicTeaserCarouselModel.class);

	@Inject
	PromotionalContentFetchService promotionalContentFetchService;

	List<OfferEventsModel> offersEventsList;

	@Inject
	Page currentPage;

	@ValueMapValue
	@Default(values = "false")
	String componentOnLoad;

	@ValueMapValue
	@Default(values = "component2")
	private String componentType;

	@ValueMapValue
	private String offerTeaserType2CtaText;
	
	@Inject
	RequestPathInfo requestPathInfo;

	String offersEventPath;
	String cityPath;

	String pagePath;

	@PostConstruct
	public void init() {
		try {
			log.debug("component Type :: {} ", componentType);
			pagePath = currentPage.getParent().getPath();
			log.debug("pagePath :: {} ", pagePath);
			String city = null;
			String outletType = null;
			String propertyName = null;
			String outletName = null;
			ValueMap vm = currentPage.getContentResource().getValueMap();

			offersEventPath = vm.get("offersEventPath", String.class);
			cityPath = vm.get("cityPath", String.class);
			log.debug("offersEventPath :: {} ", offersEventPath);
			offersEventsList = promotionalContentFetchService.getOffersEventsList("featuredOffers", city, propertyName,
					outletType, outletName, offersEventPath, pagePath, cityPath);

		} catch (Exception e) {
			log.error("Exception in DynamicTeaserCarouselModel  :: {}", e);
		}
	}

	public List<OfferEventsModel> getOffersEventsList() {
		return offersEventsList;
	}

	public String getOffersEventPath() {
		return offersEventPath;
	}

	public String getCityPath() {
		return cityPath;
	}

	public String getPagePath() {
		return pagePath;
	}

	public String getComponentOnLoad() {
		return componentOnLoad;
	}

	public String getComponentType() {
		return componentType;
	}

	public String getOfferTeaserType2CtaText() {
		return offerTeaserType2CtaText;
	}

}
