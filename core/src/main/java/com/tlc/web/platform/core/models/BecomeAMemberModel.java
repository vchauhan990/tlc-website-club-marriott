package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.BecomeMemberModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

/**
 * 
 * Sling Implementation of Become A Member Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class BecomeAMemberModel {

	private static Logger log = LoggerFactory.getLogger(BecomeAMemberModel.class);

	@Inject
	private SlingHttpServletRequest request;

	@Inject
	public SearchService searchservice;

	List<BecomeMemberModel> becomeMemberCard = null;

	@Inject
	Page currentPage;

	@Inject
	RequestPathInfo requestPathInfo;

	@PostConstruct
	public void init() {
		try {

			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo Suffix :: {} ", requestPathInfo.getSuffix());

			ValueMap vm = currentPage.getContentResource().getValueMap();

			String cityPath = vm.get("cityPath", String.class);
			becomeMemberCard = new LinkedList<>();
			String[] segments = requestPathInfo.getSuffix().split("/");
			String propertyName = segments[segments.length - 1];
			String city = segments[segments.length - 2];

			BecomeMemberModel becomeMemberObject = searchservice
					.getBecomeMemberDetails(cityPath + tlcConstants.SLASH + city + tlcConstants.SLASH + propertyName);
			becomeMemberCard.add(becomeMemberObject);
		}

		catch (Exception e) {
			log.error("Exception in Hotel Detail Model :: {}", e);
		}

	}

	public List<BecomeMemberModel> getBecomeMemberCard() {
		return becomeMemberCard;
	}
}
