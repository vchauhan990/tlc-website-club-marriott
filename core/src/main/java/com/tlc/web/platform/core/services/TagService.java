package com.tlc.web.platform.core.services;

import java.util.List;

import com.tlc.web.platform.core.vo.TagVO;

public interface TagService {

	public List<TagVO> getTagList(String tagCategoryPath);
	
}
