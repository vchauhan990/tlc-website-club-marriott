package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.BecomeMemberModel;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.SearchService;

/**
 * 
 * Sling Implementation of Hotel Detail Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class HotelDetailModel {

	private static Logger log = LoggerFactory.getLogger(HotelDetailModel.class);

	@Inject
	private SlingHttpServletRequest request;

	@Inject
	public SearchService searchservice;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	Page currentPage;

	List<PropertyDetailsModel> propertyDetailCard = null;

	List<BecomeMemberModel> becomeMemberCard = null;

	int count;

	@PostConstruct
	public void init() {
		try {

			ValueMap vm = currentPage.getContentResource().getValueMap();

			String cityPath = vm.get("cityPath", String.class);

			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo-12 :: {} ", requestPathInfo.getSuffix());

			String[] segments = requestPathInfo.getSuffix().split("/");
			String propertyName = segments[segments.length - 1];
			String city = segments[segments.length - 2];

			propertyDetailCard = new LinkedList<>();
			becomeMemberCard = new LinkedList<>();
			PropertyDetailsModel property = searchservice
					.getPropertyDetails(cityPath + "/" + city + "/" + propertyName);
			propertyDetailCard.add(property);

			BecomeMemberModel becomeMember = searchservice
					.getBecomeMemberDetails(cityPath + "/" + city + "/" + propertyName);
			becomeMemberCard.add(becomeMember);
		}

		catch (Exception e) {
			log.error("Exception in Hotel Detail Model :: {}", e);
		}

	}

	public List<PropertyDetailsModel> getPropertyDetailCard() {
		return propertyDetailCard;
	}

	public List<BecomeMemberModel> getBecomeMemberCard() {
		return becomeMemberCard;
	}
}
