package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.BannerModel;
import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class OffersEventsModel {
	private static final Logger log = LoggerFactory.getLogger(OffersEventsModel.class);

	@Inject
	SlingHttpServletRequest request;

	@Inject
	RequestPathInfo requestPathInfo ;
	
	@Inject
	SearchService searchservice;

	@Inject
	Page currentPage;
	
	List<OfferEventsModel> offereventList = null;

	BannerModel bannerModel;

	OfferEventsModel offerEvent;

	DetailPageLayout1Model detailPage;

	@PostConstruct
	public void init() {
		try {
			offereventList = new LinkedList<>();
			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());
			
			String[] segments = requestPathInfo.getSuffix().split("/");
			String offerName = segments[segments.length - 1];
			ValueMap vm = currentPage.getContentResource().getValueMap();
			String offersEventPath = vm.get("offersEventPath", String.class);
			String promotionContentPath = vm.get("promotionContentPath", String.class);
			
				if (StringUtils.isNotBlank(offerName)) {
				
				Resource offerResource = searchservice.getofferEventDetail(offersEventPath + tlcConstants.SLASH + offerName);
				
				detailPage = searchservice.getDetailPageLayout1Card(offerResource);
				offerEvent = searchservice.getOfferComponentDetails(offerResource);

				bannerModel = searchservice.getPromotionalContentBanner("offers-events-banner",promotionContentPath);
				log.info("banner model :: {}", bannerModel.getImagePath());
		}
		} catch (Exception e) {
			log.error("Exception in offers events Model ::{}", e);
		}

	}

	public OfferEventsModel getOfferEvent() {
		return offerEvent;
	}

	public DetailPageLayout1Model getDetailPage() {
		return detailPage;
	}

	public BannerModel getBannerModel() {
		return bannerModel;
	}
}