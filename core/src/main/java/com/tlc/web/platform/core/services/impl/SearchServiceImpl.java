package com.tlc.web.platform.core.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.BannerModel;
import com.aem.tlc.core.models.BecomeMemberModel;
import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.aem.tlc.core.models.OutletModel;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.Replicator;
import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PropertyVO;

/**
 * Service Implementation of Search Service
 * 
 * @author DWAO
 *
 */

@Component(immediate = true, service = { SearchService.class }, enabled = true, property = {
		Constants.SERVICE_DESCRIPTION + "= Search  Service Implementation" })
public class SearchServiceImpl implements SearchService {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Reference
	private QueryBuilder builder;

	ResourceResolver resourceResolver;

	@Reference
	Replicator replicationService;

	Resource resource;

	Session session;
	List<PropertyVO> propertyResourceList;
	PropertyVO property;
	List<PropertyVO> propertyFinalList;

	private static final Logger log = LoggerFactory.getLogger(SearchServiceImpl.class);

	@Activate
	protected final void activate(final Map<Object, Object> config) {
		log.debug("Activated Search Service");
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "contentJsonReader");
		try {
			log.debug("inside resource resolver try");
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			log.debug("Resource Resolver :: {}", resourceResolver);
			session = resourceResolver.adaptTo(Session.class);
			log.debug("Got Resource Resolver and Session for SearchServiceImpl :: {} ", session.getUserID());
		} catch (LoginException le) {
			log.error("LoginExcepion in activate method of SearchServiceImpl :: {} ", le.getMessage());
		} catch (Exception e) {
			log.error("Excepion in activate method of SearchServiceImpl :: {}", e.getMessage());
		}

	}

	@Override
	public Map<String,String> fetchCityList(String cityPath) {

		Map<String,String> cityList = new HashMap<>();
		Map<String, String> treeMap =null;
		try {

			log.debug("City List ");

			if (StringUtils.isNotBlank(cityPath)) {
				log.debug("==cityPath== :: {} ", cityPath);
				Resource cityResource = resourceResolver.getResource(cityPath);
				Iterator<Resource> cityResourceChild = cityResource.listChildren();

				cityResourceChild.forEachRemaining(res -> {
					if (!res.getName().equals(JcrConstants.JCR_CONTENT)) {
						Page pagecityResource = res.adaptTo(Page.class);
						log.debug("====pagecityResource==== :: {} ", pagecityResource.getTitle());
						/*cityList.add(pagecityResource.getTitle());*/
						cityList.put(pagecityResource.getTitle(), res.getName());
					}
				});
			}
			
			/*Collections.sort(cityList);*/
			treeMap = new TreeMap<String, String>(cityList);
			treeMap.entrySet().forEach(entry->{
			    log.info(entry.getKey() + " " + entry.getValue());  
			 });
		} catch (Exception e) {
			log.debug("Execption :: {} ", e);
		}
		return treeMap;

	}

	@Override
	public List<PropertyVO> getCityResources(String cityPath, String parentPagePath) {
		propertyResourceList = new ArrayList<>();
		propertyFinalList = new ArrayList<>();
		try {

			if (!StringUtils.equalsIgnoreCase(cityPath, null)) {
				Resource cityResource = resourceResolver.getResource(cityPath);
				Iterator<Resource> cityResourceChild = cityResource.listChildren();

				cityResourceChild.forEachRemaining(res -> {
					if (!res.getName().equals(JcrConstants.JCR_CONTENT)) {
						log.debug("Res Name::{}", res.getName());
						propertyResourceList = getPropertySearchResultsList(
								cityPath + tlcConstants.SLASH + res.getName(), parentPagePath);
						propertyFinalList.addAll(propertyResourceList);
					}

				});
			}
		} catch (Exception e) {
			log.error("error in getcity resources", e);

		}
		return propertyFinalList;
	}

	@Override
	public List<PropertyVO> getPropertySearchResultsList(String cityPath, String parentPagePath) {

		List<PropertyVO> propertyList = new ArrayList<>();
		try {
			Resource pageRes = resourceResolver.getResource(parentPagePath);
			String homePagePath = pageRes.getParent().getPath();
			Resource cityResource = resourceResolver.getResource(cityPath);

			Iterator<Resource> propertyResources = cityResource.listChildren();
			while (propertyResources.hasNext()) {

				Resource propertyResource = propertyResources.next();

				if (!propertyResource.getName().equals(JcrConstants.JCR_CONTENT)
						&& !propertyResource.getName().equals(tlcConstants.REP_POLICY)) {
					log.debug("parentPagePath::{}", homePagePath);
					property = new PropertyVO();
					property.setPropertyPageUrl(
							homePagePath + tlcConstants.SLASH + tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH
									+ tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT + tlcConstants.SLASH
									+ cityResource.getName() + tlcConstants.SLASH + propertyResource.getName());
					log.debug("benefits property page url ::{}", property.getPropertyPageUrl());
					Iterable<Resource> dataResources = propertyResource
							.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
							.getChildren();

					for (Resource dataResource : dataResources) {
						if (dataResource instanceof Resource
								&& StringUtils.containsIgnoreCase(dataResource.getName(), tlcConstants.PROPERTY)) {
							PropertyDetailsModel propertyDetail = dataResource.adaptTo(PropertyDetailsModel.class);

							if (propertyDetail != null) {
								property.setBgImage(propertyDetail.getBgImage());
								property.setBgImageAltText(propertyDetail.getBgImageAltText());
								property.setWebfacade(propertyDetail.getWebfacade());
								property.setPropertyName(propertyDetail.getPropertyName());
								property.setDescriptionSmall(propertyDetail.getDescriptionSmall());
								propertyList.add(property);

							}
							log.debug("PropertyList Size :: {}", propertyList.size());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in getPropertySearchResults Method :: {}", e);

		}
		return propertyList;
	}

	@Override
	public PropertyDetailsModel getPropertyDetails(String propertyPath) {

		PropertyDetailsModel propertyDetail = null;
		try {

			Resource propertyResource = resourceResolver.getResource(propertyPath);
			//log.debug("property name  :: {} ", propertyResource.getName());
			if (propertyResource != null) {
				Iterable<Resource> componentResources = propertyResource
						.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
						.getChildren();

				for (Resource componentResource : componentResources) {
					if (componentResource instanceof Resource
							&& StringUtils.containsIgnoreCase(componentResource.getName(), tlcConstants.PROPERTY)) {
						propertyDetail = componentResource.adaptTo(PropertyDetailsModel.class);

					}
				}
			}

		} catch (Exception e) {
			log.error("Exception in getPropertyDetails in search service :: {} ", e);
		}

		return propertyDetail;

	}

	@Override
	public BecomeMemberModel getBecomeMemberDetails(String propertyPath) {

		BecomeMemberModel becomeMemberModel = null;
		try {

			Resource propertyResource = resourceResolver.getResource(propertyPath);
			log.debug("propertyResource name :: {} ", propertyResource.getName());
			Iterable<Resource> componentResources = propertyResource
					.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
					.getChildren();

			for (Resource componentResource : componentResources) {
				if (componentResource instanceof Resource
						&& StringUtils.containsIgnoreCase(componentResource.getName(), tlcConstants.BECOME_A_MEMBER)) {
					becomeMemberModel = componentResource.adaptTo(BecomeMemberModel.class);

				}

			}
		} catch (Exception e) {
			log.error("Exception in getBecomeMemberDetails in search service :: {} ", e);
		}

		return becomeMemberModel;

	}

	@Override
	public Resource getofferEventDetail(String offersEventsPath) {
		Resource offersResource = null;
		try {
			log.debug("offersEventsPath :: {} ", offersEventsPath);
			offersResource = resourceResolver.getResource(offersEventsPath);
			log.debug("offersResource :: {} ", offersResource.getName());

		} catch (Exception e) {
			log.error("Exception in offereventModel :: {} ", e);
		}

		return offersResource;
	}

	@Override
	public OfferEventsModel getOfferComponentDetails(Resource offereventResource) {
		OfferEventsModel offerseventsmodel = null;
		try {
			offerseventsmodel = offereventResource.adaptTo(OfferEventsModel.class);

		} catch (Exception e) {
			log.error("Exception in getOfferComponentDetails :: {} ", e);
		}

		return offerseventsmodel;
	}

	@Override
	public BannerModel getPromotionalContentBanner(String modeType, String promotionContentPath) {
		BannerModel bannerModel = null;
		String bannerNodePath = "";
		switch (modeType) {

		case "offers-events-banner":

			bannerNodePath = promotionContentPath + tlcConstants.SLASH + tlcConstants.OFFERS_EVENTS_BANNER_PATH;
			break;
		case "unique-experience-banner":

			bannerNodePath = promotionContentPath + tlcConstants.SLASH + tlcConstants.UNIQUE_EXPERIENCES_BANNER_PATH;
			break;
		case "signature-collection-banner":

			bannerNodePath = promotionContentPath + tlcConstants.SLASH + tlcConstants.SIGNATURE_COLLECTIONS_BANNER_PATH;
			break;
		default:

		}

		try {

			log.debug("bannerPath :: {}  ", bannerNodePath);
			if (StringUtils.isNotEmpty(bannerNodePath)) {

				Resource offersResource = resourceResolver.getResource(bannerNodePath);
				Iterable<Resource> componentResources = offersResource
						.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
						.getChildren();

				for (Resource componentResource : componentResources) {
					if (StringUtils.containsIgnoreCase(componentResource.getName(), tlcConstants.BANNER_PATH)) {
						bannerModel = componentResource.adaptTo(BannerModel.class);

					}
				}
			}

		} catch (NullPointerException ne) {
			log.error("Exception in banner Model :: {} ", ne);
		} catch (Exception e) {
			log.error("Exception in banner Model :: {} ", e);
		}

		return bannerModel;

	}

	@Override
	public DetailPageLayout1Model getDetailPageLayout1Card(Resource outletResource) {

		DetailPageLayout1Model detailpagelayout1 = null;
		Iterable<Resource> componentResources = outletResource
				.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
				.getChildren();

		for (Resource componentResource : componentResources) {
			if (componentResource instanceof Resource
					&& StringUtils.containsIgnoreCase(componentResource.getName(), tlcConstants.DETAIL_PAGE_1)) {
				detailpagelayout1 = componentResource.adaptTo(DetailPageLayout1Model.class);

			}
		}

		return detailpagelayout1;

	}

	@Override
	public OutletModel getOutletCard(Resource outletResource) {

		OutletModel outletmodel = null;

		Iterable<Resource> componentResources = outletResource
				.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
				.getChildren();

		for (Resource componentResource : componentResources) {
			if (componentResource instanceof Resource
					&& StringUtils.containsIgnoreCase(componentResource.getName(), tlcConstants.OUTLET)) {
				outletmodel = componentResource.adaptTo(OutletModel.class);

			}

		}
		return outletmodel;
	}

	@Override
	public Resource outletResource(String outletPath) {
		log.debug("outletPath :: {} ", outletPath);

		Resource outletResource = null;
		try {
			outletResource = resourceResolver.getResource(outletPath);

		} catch (Exception e) {
			log.error("Exception in outletResource :: {} ", e);
		}
		return outletResource;

	}

	@Override
	public String dynamicImageonCity(String cityPath, String defaultImagePath) {
		log.info("cityPath :: {} ", cityPath);

		String imagePath = "";
		try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			if (pageManager != null) {
				Page cityPage = pageManager.getPage(cityPath);
				ValueMap properties = cityPage.getContentResource().getValueMap();

				imagePath = properties.get("dynamicBannerImagePath", String.class);
				log.info("imagePath");
				if (StringUtils.isBlank(imagePath)) {
					imagePath = defaultImagePath;
				}

			}
		} catch (Exception e) {
			log.info("Exception in Dynamic Banner :: {} ", e);
		}
		return imagePath;

	}

	@Deactivate
	protected void deactivate(ComponentContext ctx) {
		log.debug("Deactivated ContentReaderServiceImpl");
		session.logout();
		if (resourceResolver != null) {
			resourceResolver.close();
		}
	}

}
