package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.vo.PageVO;

/**
 * Sling Model Implementation of List Of Blog Layout Model
 *
 * @author DWAO
 *
 */

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = OPTIONAL)
public class BlogLayoutModel {

	private static Logger log = LoggerFactory.getLogger(BlogLayoutModel.class);

	@Inject
	private Page currentPage;

	@ValueMapValue
	private String layoutType;

	@ValueMapValue
	private String heading;

	@ValueMapValue
	private String location;

	@ValueMapValue
	private String date;

	@ValueMapValue
	private String author;

	@ValueMapValue
	private String photoCredits;

	@ValueMapValue
	private String description;

	@ValueMapValue
	private String[] tags;

	@ValueMapValue
	private String thumbnailImage;

	List<PageVO> titleCarouselList;

	PageVO pageObject;
	
	@ValueMapValue
	private String nextIcon;

	@ValueMapValue
	private String prevIcon;
	
	@PostConstruct
	public void init() {
		try {

			titleCarouselList = new ArrayList<>();
			String pagePath = currentPage.getPath();

			if (currentPage instanceof Page) {
				Page parentPage = currentPage.getParent();

				if (parentPage instanceof Page) {

					Iterator<Page> childPages = parentPage.listChildren();
					while (childPages.hasNext()) {
						Page childPage = childPages.next();

						if (!pagePath.equalsIgnoreCase(childPage.getPath())) {
							pageObject = new PageVO();
							pageObject.setTitle(childPage.getTitle());
							pageObject.setPageUrl(childPage.getPath());
							titleCarouselList.add(pageObject);
						}

					}
				}

			}

		} catch (Exception e) {
			log.error("Exception in BlogLayout Model", e);
		}
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public List<PageVO> getTitleCarouselList() {
		return titleCarouselList;
	}

	public String getHeading() {
		return heading;
	}

	public String getLocation() {
		return location;
	}

	public String getDate() {
		return date;
	}

	public String getAuthor() {
		return author;
	}

	public String getPhotoCredits() {
		return photoCredits;
	}

	public String getDescription() {
		return description;
	}

	public String[] getTags() {
		return tags;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public String getLayoutType() {
		return layoutType;
	}

	public String getPrevIcon() {
		return prevIcon;
	}

	public String getNextIcon() {
		return nextIcon;
	}

}