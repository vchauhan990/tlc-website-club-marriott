package com.tlc.web.platform.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import com.tlc.web.platform.core.util.LinkUtil;

public interface ListOfLinksItemModel {
	
	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class ListOfLinksFirst {
				
		@Inject
		public String text;
		
		@Inject
		public String url;
		
		@Inject
		public String logoPath;
		
		@Inject
		public String altText;
		
		@Inject
		public String countryCode;
		
		@Inject
		public String gender;
		
		@Inject
		public String contactHow;
		
	

		@Inject
		public String natureOfQuery;
		
		@PostConstruct
		public void init() {
			
			url = LinkUtil.getFormattedURL(url);
		}

		public String getLogoPath() {
			return logoPath;
		}

		public String getAltText() {
			return altText;
		}

		public String getText() {
			return text;
		}

		public String getUrl() {
			return url;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public String getGender() {
			return gender;
		}
		
		public String getContactHow() {
			return contactHow;
		}

		public String getNatureOfQuery() {
			return natureOfQuery;
		}
		
	}

}
