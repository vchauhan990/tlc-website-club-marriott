package com.tlc.web.platform.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PropertyVO;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "=" + "Dynamic Banner  servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/form/dynamicBanner" })
public class DynamicBannerServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(DynamicBannerServlet.class);

	@Reference
	SearchService searchservice;

	private String imagePath = null;

	@Activate
	protected void activate() {
		log.debug("search Form servlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try {

			log.debug("inside try");
			JSONObject js = new JSONObject();
			if ((request.getParameter("cityName") != null) && (request.getParameter("resourcePath") != null)) {

				String cityPath = request.getParameter("resourcePath") + "/"
						+ request.getParameter("cityName").toLowerCase();
				log.info("cityPath :: {}", cityPath);
				String defaultImagePath = request.getParameter("defaultImage");
				imagePath = searchservice.dynamicImageonCity(cityPath, defaultImagePath);

			}

			js.put("message", imagePath);
			js.put("status", "200");
			response.setStatus(200);
			out.println(js);
			out.close();

		} catch (Exception e) {
			log.debug("Exception in Property search :: {}", e);
		}
	}
}
