package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tlc.web.platform.core.models.NavigationItemModel.NavigationItemFirst;
import com.tlc.web.platform.core.util.LinkUtil;

/**
 * 
 * Sling Implementation of Navigation Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class NavigationModel {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Inject
	List<NavigationItemFirst> navigationItems;
	
	@Inject
	private String headerLogoUrl ;

	@PostConstruct
	public void init() {
		log.debug("Navigation Model invoked");
	}

	public List<NavigationItemFirst> getListitems() {
		return navigationItems;
	}

	public String getHeaderLogoUrl() {
		return LinkUtil.getFormattedURL(headerLogoUrl);
	}

}