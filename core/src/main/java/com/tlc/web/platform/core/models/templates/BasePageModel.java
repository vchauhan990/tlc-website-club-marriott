package com.tlc.web.platform.core.models.templates;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Base Page Sling Model 
 *
 * @author DWAO
 *
 */

@ConsumerType
public interface BasePageModel {
	
	public String getTitle();

}
