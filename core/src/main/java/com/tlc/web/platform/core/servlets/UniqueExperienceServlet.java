package com.tlc.web.platform.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.vo.ResponseDTO;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "=" + "Unique Experience servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/uniqueExperience" })

public class UniqueExperienceServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(UniqueExperienceServlet.class);

	@Reference
	PromotionalContentFetchService promotionalContentService;

	private List<SignatureCollectionVO> uniqueExperienceList = null;

	@Activate
	protected void activate() {
		log.debug("UniqueExperienceServlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		// Gson gson = new Gson();
		log.info("log 100");
		try {

			 JSONObject js=new JSONObject();
			ResponseDTO responseDTO = new ResponseDTO();
			String property = null;
			String cityName = request.getParameter("city");
			log.info("cityName :: {} ",cityName);
			String membershipTypePath = request.getParameter("resourcePath");
			String pagePath = request.getParameter("pagePath");
			String city = cityName.toLowerCase();
			log.info("city :: {} ",city);
			String modeType = "get-experiences-by-city";
			log.info("log 200");
			uniqueExperienceList = promotionalContentService.getExperiencesbyTag(city, property, modeType, pagePath,
					membershipTypePath);
			for(SignatureCollectionVO a:uniqueExperienceList) {
				
				log.info("Object is {}",a);
				log.info("desMed"+a.getDetailPageLayout1());
				log.info("desMed"+a.getDetailPageLayout2());
			}
			responseDTO.setCode(200);
			responseDTO.setStatus("OK");
			responseDTO.setData(uniqueExperienceList);
			log.info("log 1");
			//String desMed = uniqueExperienceList.get(0).getDetailPageLayout1().getDescriptionMedium();
			//log.info("log 1"+desMed);
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().disableHtmlEscaping().create();
			log.info("response is {}", responseDTO.getData());
//			 js.put("data", uniqueExperienceList);
//			 js.put("status", "200");
			log.info("jeyu");
			//Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
			log.info("log");
			String json = gson.toJson(responseDTO);
			log.info("log 3");
			//log.info("log 4"+json);
	
			//log.info("json is:::::::;;;" + responseDTO);
			// out.print(js);
			out.print(json);

		} catch (NullPointerException ne) {
			log.error("NullPointerException in PromotionContentServlet :: {}", ne);
		} catch (Exception e) {
			log.error("Exception in UniqueExperienceServlet :: {}", e);
		}

	}

}
