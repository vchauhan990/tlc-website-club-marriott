package com.tlc.web.platform.core.vo;

import com.tlc.web.platform.core.util.LinkUtil;

public class Socialitem {
	private String iconClass;
	private String url;
	private String openInNewWindow;

	public String getOpenInNewWindow() {
		return openInNewWindow;
	}

	public void setOpenInNewWindow(String openInNewWindow) {
		this.openInNewWindow = openInNewWindow;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getUrl() {
		return LinkUtil.getFormattedURL(url);
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
