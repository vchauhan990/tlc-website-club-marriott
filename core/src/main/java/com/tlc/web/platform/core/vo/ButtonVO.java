package com.tlc.web.platform.core.vo;

import com.tlc.web.platform.core.util.LinkUtil;

public class ButtonVO {

	private String btnText;
	private String btnUrl;
	private String openInNewWindow;
	private String btnImagePath;
	private String btnImageAltText;
	private String memberDrawer;

	
	public String getBtnText() {
		return btnText;
	}

	public String getBtnUrl() {
		return LinkUtil.getFormattedURL(btnUrl);
 
	}

	public void setBtnText(String btnText) {
		this.btnText = btnText;
	}

	public void setBtnUrl(String btnUrl) {
		this.btnUrl = btnUrl;
	}

	public String getOpenInNewWindow() {
		return openInNewWindow;
	}

	public void setOpenInNewWindow(String openInNewWindow) {
		this.openInNewWindow = openInNewWindow;
	}

	public String getBtnImagePath() {
		return btnImagePath;
	}

	public void setBtnImagePath(String btnImagePath) {
		this.btnImagePath = btnImagePath;
	}
	
	public String getBtnImageAltText() {
		return btnImageAltText;
	}

	public void setBtnImageAltText(String btnImageAltText) {
		this.btnImageAltText = btnImageAltText;
	}

	public String getMemberDrawer() {
		return memberDrawer;
	}

	public void setMemberDrawer(String memberDrawer) {
		this.memberDrawer = memberDrawer;
	}

}
