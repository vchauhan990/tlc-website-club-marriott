package com.tlc.web.platform.core.models.templates.Impl;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tlc.web.platform.core.models.templates.BasePageModel;

@Model(adaptables=Resource.class,adapters=BasePageModel.class)
public class BasePageModelImpl implements BasePageModel {

	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(BasePageModelImpl.class);
	
	private String title;
	
	@PostConstruct
	public void init() {
		log.info("Base Page Model Invoked");
	}

	@Override
	public String getTitle() {
		return title;
	}
}