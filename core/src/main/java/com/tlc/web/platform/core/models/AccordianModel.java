package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.AccordianVO;

/**
 * 
 * Sling Implementation of Accordian Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class AccordianModel {

	private static Logger log = LoggerFactory.getLogger(AccordianModel.class);

	@Inject
	@Optional
	@Default(values = "{}")
	private String[] faqItems;

	private List<AccordianVO> faqList;

	@PostConstruct
	public void init() {

		Gson gson = new Gson();

		try {
			if (faqItems.length > 0) {

				faqList = new ArrayList<>();
				int eachCount = 0;
				for (String eachString : faqItems) {

					AccordianVO eachItem = gson.fromJson(eachString, AccordianVO.class);
					List<AccordianVO> subEachitem = new ArrayList<>();
					subEachitem = eachItem.getAccordianItem();
					int subEachCount = 0;
					for (AccordianVO subAcc : subEachitem) {

						subAcc.setParsysId("parsys-" + eachCount + "-" + subEachCount);
						subEachCount++;
					}

					eachCount++;
					faqList.add(eachItem);
				}
			}

		} catch (Exception e) {
			log.error("Exception in accordian model:::{}", e);
		}
	}

	public List<AccordianVO> getFaqList() {
		return faqList;
	}

}