package com.tlc.web.platform.core.services;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.aem.tlc.core.models.OutletModel;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

public interface PromotionalContentFetchService {

	public List<SignatureCollectionVO> getExperiencesbyTag(String city, String property, String modeType,
			String pagePath, String membershipTypePath);

	public List<DetailPageLayout1Model> getUniqueExperiencesContent(String promotionName, String promotionType,
			String promotionContentPath);

	public Map<String, String> getExperienceHotelLocations(String promotionName, String promotionType,
			String promotionPath);

	public List<OfferEventsModel> getOffersEventsList(String modeType, String city, String outletType,
			String propertyName, String outletName, String offersEventPath, String pagePath, String propertyPath);

	public List<OfferEventsModel> getFeaturedOffersWithCityList();

	public LinkedHashMap<String, List<OutletModel>> getExperiencesOutletElements(String propertName, String city,
			String cityPath, String pagePathUrl);

	List<PropertyDetailsModel> getFeaturedHotelsList(String cityPath, String parentPagePath, String modetag,
			String city);
}