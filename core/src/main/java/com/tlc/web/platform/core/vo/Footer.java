package com.tlc.web.platform.core.vo;

import com.tlc.web.platform.core.util.LinkUtil;

public class Footer {
	private String contactInfoText;
	
	private String contactIcon;
	
	private String contactUrl;
	
	private String imagePathLink;

	public String getImagePathLink() {
		return imagePathLink;
	}

	public void setImagePathLink(String imagePathLink) {
		this.imagePathLink = imagePathLink;
	}

	public String getContactInfoText() {
		return contactInfoText;
	}

	public void setContactInfoText(String contactInfoText) {
		this.contactInfoText = contactInfoText;
	}

	public String getContactIcon() {
		return contactIcon;
	}

	public void setContactIcon(String contactIcon) {
		this.contactIcon = contactIcon;
	}

	public String getContactUrl() {
		return LinkUtil.getFormattedURL(contactUrl); 
	}

	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}
}
