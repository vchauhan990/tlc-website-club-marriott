package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.PromotionalContentFetchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;

/**
 * 
 * Sling Implementation of Unique Experience Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},defaultInjectionStrategy = OPTIONAL)
public class UniqueExperienceModel {
	
	private static final Logger log = LoggerFactory.getLogger(UniqueExperienceModel.class);
	
	@Inject
	Page currentPage;
	
	@Inject
	PromotionalContentFetchService promotionalContentFetchService ;
	
	String uniqueExperiencePath="";
	
	@ValueMapValue 
	@Default(values = "component3")
	private String componentType;

	private List<SignatureCollectionVO> expCarousel = null;
	
	@PostConstruct
	public void init() {
		try {
			String pagePath = currentPage.getParent().getPath();
			
			ValueMap vm = currentPage.getContentResource().getValueMap();
			uniqueExperiencePath = vm.get("uniqueExperiencePath", String.class);
			String property = null;
			String city = null;
			String modeType = "get-experiences-by-city";
			
			expCarousel = promotionalContentFetchService.getExperiencesbyTag(city,property,modeType,pagePath,uniqueExperiencePath);
		/*expCarousel = expCarousel.stream().filter(dpl -> dpl.getSliderImage().endsWith("gif"))
				.map(dpl -> {dpl.setSliderImage(dpl.getSliderImage()+"/jcr:content/renditions/webimage.gif");return dpl;})
					.collect(Collectors.toList());*/
			
		}catch(Exception e){
			log.info("exception in Unique Experience Model ::{}", e );
		}
	}
	
	public String getUniqueExperiencePath() {
		return uniqueExperiencePath;
	}

	public List<SignatureCollectionVO> getExpCarousel() {
		return expCarousel;
	}
	
	public String getComponentType() {
		return componentType;
	}
}
