package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.PropertyDetailsModel;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PageVO;

/**
 * 
 * Sling Implementation of Benefit Search Result Listing
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class BenefitsSearchResultListing {

	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(BenefitsSearchResultListing.class);

	@Self
	@Inject
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@ValueMapValue
	private String componentElement;

	@Inject
	Page currentPage;

	@ValueMapValue
	@Default(values = "{}")
	private String[] pageItems;

	List<PropertyDetailsModel> propertyList = null;

	@PostConstruct
	public void init() {
		try {
			String pagePath;
			String pageUrl ;
			propertyList = new LinkedList<>();

			Gson gson = new Gson();
			if (StringUtils.equals(currentPage.getName(), tlcConstants.HOME)) {
				pagePath = currentPage.getPath();
				log.debug("pagePath::{}" , pagePath);
			} else {
				pagePath = currentPage.getParent().getPath();
				log.debug("pagePath::{}" , pagePath);
			}

			for (String itemString : pageItems) {
				PageVO pageObj = gson.fromJson(itemString, PageVO.class);

				Resource propertyResources = resourceResolver.getResource(pageObj.getPagePath());
				Iterable<Resource> childResources = propertyResources
						.getChild(JcrConstants.JCR_CONTENT + tlcConstants.SLASH + tlcConstants.RESPONSIVE_GRID_PATH)
						.getChildren();
				for (Resource childResource : childResources) {
					if (StringUtils.equalsIgnoreCase(childResource.getName(), tlcConstants.PROPERTY)) {
						
						String[] page = pageObj.getPagePath().split(tlcConstants.CITY);
						log.debug("page[1]::{}" , page[1]);

						PropertyDetailsModel property = childResource.adaptTo(PropertyDetailsModel.class);
						pageUrl = pagePath + tlcConstants.SLASH + tlcConstants.HOTELS_AND_RESORTS + tlcConstants.SLASH
								+ tlcConstants.PROPERTY_PAGE + tlcConstants.HTML_EXT;
						property.setPageUrl(pageUrl + page[1]);
						propertyList.add(property);
					}
				}
			}

		} catch (Exception e) {
			log.error("Exception in Benefits Search Result Listing ", e);
		}
	}

	public String getComponentElement() {
		return componentElement;
	}

	public List<PropertyDetailsModel> getPropertyList() {
		return propertyList;
	}

}
