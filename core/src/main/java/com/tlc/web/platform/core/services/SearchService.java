package com.tlc.web.platform.core.services;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;

import com.aem.tlc.core.models.BecomeMemberModel;
import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.OfferEventsModel;
import com.aem.tlc.core.models.OutletModel;
import com.aem.tlc.core.models.PropertyDetailsModel;
import com.aem.tlc.core.models.BannerModel;
import com.tlc.web.platform.core.vo.PropertyVO;

public interface SearchService {

	public Map<String, String> fetchCityList(String cityPath);

	public List<PropertyVO> getPropertySearchResultsList(String cityPath, String parentPagePath);

	public List<PropertyVO> getCityResources(String cityPath, String parentPagePath);

	public PropertyDetailsModel getPropertyDetails(String propertyPath);

	public BecomeMemberModel getBecomeMemberDetails(String propertyPath);

	public Resource getofferEventDetail(String offersEventPath);

	public BannerModel getPromotionalContentBanner(String modeType, String path);

	public Resource outletResource(String outletPath);

	public OutletModel getOutletCard(Resource outletResource);

	public DetailPageLayout1Model getDetailPageLayout1Card(Resource outletResource);

	public OfferEventsModel getOfferComponentDetails(Resource offereventResource);

	public String dynamicImageonCity(String cityPath, String defaultImagePath);
}
