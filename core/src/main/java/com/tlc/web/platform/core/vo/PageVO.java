package com.tlc.web.platform.core.vo;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tlc.web.platform.core.util.LinkUtil;

public class PageVO {
	private String title;
	private String description;
	private String pageUrl;
	private String category;
	private String thumbnailImage;
	private String pagePath;	
	private String parsysId;
	private String titleId;
	private List<PageVO> sectionItems;
	private String sectionId;
	private String sectionText;
	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	public String getSectionText() {
		return sectionText;
	}

	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}

	public List<PageVO> getSectionItems() {
		return sectionItems;
	}

	public void setSectionItems(List<PageVO> sectionItems) {
		this.sectionItems = sectionItems;
	}

	public String getTitleId() {
	
		return StringUtils.replaceAll(title, "[^a-zA-Z0-9]", "-");
	
		
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	public String getParsysId() {
		return parsysId;
	}

	public void setParsysId(String parsysId) {
		this.parsysId = parsysId;
	}

	private List<PageVO> childPage;
	

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagepath) {
		this.pagePath = pagepath;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(String thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPageUrl() {
		return LinkUtil.getFormattedURL(pageUrl); 
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<PageVO> getChildPage() {
		return childPage;
	}

	public void setChildPage(List<PageVO> childPage) {
		this.childPage = childPage;
	}

}