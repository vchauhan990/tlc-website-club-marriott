package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.tlc.web.platform.core.vo.ButtonVO;

/**
 * Sling Model Implementation of List Of Button Model
 *
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class ButtonModel {

	private static Logger log = LoggerFactory.getLogger(ButtonModel.class);

	@Inject @Optional
	@Default(values = "{}")
	private String[] textBtnItems;
	
	@Inject @Optional
	@Default(values = "{}")
	private String[] imgBtnItems;

	private List<ButtonVO> textBtnList;
	
	private List<ButtonVO> imgBtnList;
	
	@PostConstruct
	protected void init() {
		
		Gson gson = new Gson();
		textBtnList = new LinkedList<>();
		imgBtnList = new LinkedList<>();

		for (String itemString : textBtnItems) {
			ButtonVO buttonVO = gson.fromJson(itemString, ButtonVO.class);
			textBtnList.add(buttonVO);
		}
		
		for (String itemString : imgBtnItems) {
			ButtonVO buttonVO = gson.fromJson(itemString, ButtonVO.class);
			imgBtnList.add(buttonVO);
		}
	}

	public List<ButtonVO> getTextBtnList() {
		return textBtnList;
	}

	public List<ButtonVO> getImgBtnList() {
		return imgBtnList;
	}	
	
}