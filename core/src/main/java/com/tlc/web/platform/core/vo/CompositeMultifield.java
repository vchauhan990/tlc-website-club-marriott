package com.tlc.web.platform.core.vo;
import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

public interface CompositeMultifield {

	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class FirstLevel {

		@Inject
		public String text;

		@Inject
		public String link;

		@Inject
		public String checkbox;

		@Inject
		public List<SecondLevel> secondLevel;

		public String getText() {
			return text;
		}

		public String getLink() {
			return link;
		}

		public String getCheckbox() {
			return checkbox;
		}

		public List<SecondLevel> getSecondLevel() {
			return secondLevel;
		}
	}

	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class SecondLevel {

		@Inject
		public String text;

		@Inject
		public String link;

		@Inject
		public String checkbox;

		@Inject
		public String textArea;

		@Inject
		public String image;

		@Inject
		public List<ThirdLevel> thirdLevel;

		public List<ThirdLevel> getThirdLevel() {
			return thirdLevel;
		}

		public String getText() {
			return text;
		}

		public String getLink() {
			return link;
		}

		public String getCheckbox() {
			return checkbox;
		}

		public String getTextArea() {
			return textArea;
		}

		public String getImage() {
			return image;
		}
	}

	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class ThirdLevel {

		@Inject
		public String text;

		@Inject
		public String link;

		@Inject
		public String checkbox;

		@Inject
		public String richText;

		public String getText() {
			return text;
		}

		public String getLink() {
			return link;
		}

		public String getCheckbox() {
			return checkbox;
		}

		public String getRichText() {
			return richText;
		}

	}

}
