package com.tlc.web.platform.core.models;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import com.tlc.web.platform.core.vo.PageVO;

/**
 * 
 * Sling Implementation of Terms and Conditions Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TermsandConditionsModel {

	private static Logger log = LoggerFactory.getLogger(TermsandConditionsModel.class);
	
	@Inject
	@Optional
	@Default(values = "{}")
	private String[] termsConditionsItems;
	
	private List<PageVO> termsCondList;

	@PostConstruct
	public void init() {
	
		try {
			Gson gson = new Gson();
			int count = 0;
			
			if (termsConditionsItems.length > 0) {
				termsCondList = new LinkedList<>();
				for (String eachString : termsConditionsItems) {
					PageVO eachItem = gson.fromJson(eachString, PageVO.class);
					count++;
					eachItem.setParsysId("terms-"+eachItem.getTitleId());
					termsCondList.add(eachItem);
				}
			}
		}  catch (Exception e) {
			log.error("Exception in termsConditions :: {} ",e);
		}
	}
	public List<PageVO> getTermsCondList() {
		return termsCondList;
	}
	
}