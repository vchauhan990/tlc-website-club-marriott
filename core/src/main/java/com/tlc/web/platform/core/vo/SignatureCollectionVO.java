package com.tlc.web.platform.core.vo;

import com.aem.tlc.core.models.DetailPageLayout1Model;
import com.aem.tlc.core.models.DetailPageLayout2Model;
import com.aem.tlc.core.models.DetailPageLayout3Model;
import com.aem.tlc.core.models.DetailPageLayout4Model;
import com.google.gson.annotations.Expose;

/**
 * Signature Collection VO
 *
 * @author DWAO
 *
 */

public class SignatureCollectionVO {
	
	@Expose
	private DetailPageLayout1Model detailPageLayout1;
	
	@Expose
	private DetailPageLayout2Model detailPageLayout2;
	
	@Expose
	private DetailPageLayout3Model detailPageLayout3;
	
	@Expose
	private DetailPageLayout4Model detailPageLayout4;
	
	@Expose
	private String subBrandTag;

	public DetailPageLayout1Model getDetailPageLayout1() {
		return detailPageLayout1;
	}

	public void setDetailPageLayout1(DetailPageLayout1Model detailPageLayout1) {
		this.detailPageLayout1 = detailPageLayout1;
	}

	public DetailPageLayout2Model getDetailPageLayout2() {
		return detailPageLayout2;
	}

	public void setDetailPageLayout2(DetailPageLayout2Model detailPageLayout2) {
		this.detailPageLayout2 = detailPageLayout2;
	}

	public DetailPageLayout3Model getDetailPageLayout3() {
		return detailPageLayout3;
	}

	public void setDetailPageLayout3(DetailPageLayout3Model detailPageLayout3) {
		this.detailPageLayout3 = detailPageLayout3;
	}

	public DetailPageLayout4Model getDetailPageLayout4() {
		return detailPageLayout4;
	}

	public void setDetailPageLayout4(DetailPageLayout4Model detailPageLayout4) {
		this.detailPageLayout4 = detailPageLayout4;
	}

	public String getSubBrandTag() {
		return subBrandTag;
	}

	public void setSubBrandTag(String subBrandTag) {
		this.subBrandTag = subBrandTag;
	}

}
