package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tlc.web.platform.core.models.TeaserItemModel.First;

/**
 * Sling Model Implementation of List Of Teasers Model
 *
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class ListOfTeasersModel {

	private static Logger log = LoggerFactory.getLogger(ListOfTeasersModel.class);

	@Inject
	List<First> teasers;

	@PostConstruct
	protected void init() {
		log.debug("List of Teasers Model Invoked");	
	}

	public List<First> getTeasers() {
		return teasers;
	}

}