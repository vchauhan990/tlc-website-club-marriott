package com.tlc.web.platform.core.vo;

import com.google.gson.annotations.Expose;

/**
 * Response DTO
 * 
 * @author lenovo
 *
 */
public class ResponseDTO {

	@Expose
	private int code;
	@Expose
	private String status;
	@Expose
	private Object data;
	@Expose
	private String message;

	public int getCode() {
		return code;
	}

	/*
	 * 200 - OK 201 - Submitted Successfully 202 - Data not Found
	 * 
	 * 400 - Bad Request 401 - Unauthorized
	 * 
	 * 500 - Internal Server Error
	 * 
	 */
	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
