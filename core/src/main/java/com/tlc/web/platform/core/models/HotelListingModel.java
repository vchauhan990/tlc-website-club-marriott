package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PropertyVO;

/**
 * 
 * Sling Implementation of Hotel Listing Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, defaultInjectionStrategy = OPTIONAL)
public class HotelListingModel {

	private static final Logger log = LoggerFactory.getLogger(HotelListingModel.class);

	List<PropertyVO> searchResultList = null;
	String searchResults = null;

	@Inject
	Page currentPage;

	String cityPath = "";

	@Inject
	SearchService searchservice;
	String pagePath = "";

	@PostConstruct
	public void init() {
		
		try {
			/*Hotel Listing for Hotel and Resorts Page*/
			if (StringUtils.equals(currentPage.getName(), tlcConstants.HOTELS_AND_RESORTS)) {
				pagePath = currentPage.getPath();
				log.debug("Hotels and Resorts Page Path :: {} ", pagePath);
			}
			/*Hotel Listing for Benefits Page*/
			if (StringUtils.equals(currentPage.getName(), tlcConstants.BENEFITS)) {
				String pageUrl = currentPage.getParent().getPath();
				pagePath = pageUrl + tlcConstants.SLASH + tlcConstants.HOTELS_AND_RESORTS;
				log.debug("Benefits Page Path :: {} ", pagePath);
			}

			ValueMap vm = currentPage.getContentResource().getValueMap();
			cityPath = vm.get("cityPath", String.class);
			Gson gson = new Gson();

			searchResultList = searchservice.getCityResources(cityPath, pagePath);
			log.debug("search Result List Size :: {}", searchResultList.size());
	
			searchResults = gson.toJson(searchResultList);

		} catch (Exception e) {
			log.error("Exception in init method of HotelListing ::  ", e);
		}
	}

	public List<PropertyVO> getSearchResultList() {
		return searchResultList;
	}

	public String getSearchResults() {
		return searchResults;
	}

	public String getCityPath() {
		return cityPath;
	}
}
