package com.tlc.web.platform.core.models;

import static org.apache.sling.models.annotations.DefaultInjectionStrategy.OPTIONAL;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.tlc.web.platform.core.services.TagService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.PageObject;
import com.tlc.web.platform.core.vo.TagVO;

/**
 * Sling Model Implementation of List Of Blog Listing Model 
 *
 * @author DWAO
 *
 */

@Model(adaptables = Resource.class, defaultInjectionStrategy = OPTIONAL)
public class BlogListingModel {

	private static Logger log = LoggerFactory.getLogger(BlogListingModel.class);

	@Inject
	@Optional
	@Default(values = "{}")
	private String tagsPath;

	private List<TagVO> sectionTags;

	@Inject
	private ResourceResolver resolver;

	private List<PageObject> pages;

	private String blogPagesJson;

	private String sectionTagsJson;

	@Inject
	TagService tagService;

	@Inject
	@Default(values = "{}")
	private String BlogParentPath;

	@PostConstruct
	protected void init() {
		try {
			sectionTags = new ArrayList<TagVO>();
			sectionTags = tagService.getTagList(tagsPath);

			/**** Page Listing Method ****/
			pages = new ArrayList<PageObject>();

			Resource parentResource = resolver.getResource(BlogParentPath);

			if (parentResource instanceof Resource) {
				Page parentPage = parentResource.adaptTo(Page.class);

				if (parentPage instanceof Page) {
					Iterable<Resource> childResources = parentResource.getChildren();

					for (Resource child : childResources) {
						PageManager pageManager = resolver.adaptTo(PageManager.class);
						Page blogChildPage = pageManager.getPage(child.getPath());

						if (blogChildPage instanceof Page) {
							Resource blogDetailResource = resolver
									.getResource(blogChildPage.getPath() + tlcConstants.BLOG_DETAIL_PATH);

							if (blogDetailResource instanceof Resource) {
								BlogLayoutModel blogLayoutModel = blogDetailResource.adaptTo(BlogLayoutModel.class);
								PageObject pageObject = new PageObject();
								pageObject.setHeading(blogLayoutModel.getHeading());
								pageObject.setDescription(blogLayoutModel.getDescription());
								pageObject.setDate(blogLayoutModel.getDate());
								pageObject.setPageUrl(blogChildPage.getPath());
								pageObject.setTags(blogLayoutModel.getTags());
								pageObject.setThumbnailImage(blogLayoutModel.getThumbnailImage());
								pages.add(pageObject);
							}

						}
					}
				}
			}

			Gson gson = new Gson();
			blogPagesJson = gson.toJson(pages);

		} catch (Exception e) {
			log.error("exception in Blog listing Model :: ", e);
		}
	}

	public List<TagVO> getSectionTags() {
		return sectionTags;
	}

	public String getBlogPagesJson() {
		return blogPagesJson;
	}

	public List<PageObject> getPages() {
		return pages;
	}

	public String getSectionTagsJson() {
		return sectionTagsJson;
	}
}
