package com.tlc.web.platform.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.tlc.core.models.CardBenefitsModel;
import com.aem.tlc.core.models.CertificateModel;
import com.aem.tlc.core.models.TermsConditionsModel;
import com.day.cq.wcm.api.Page;
import com.tlc.web.platform.core.services.MemberBenefitsListingService;
import com.tlc.web.platform.core.services.SearchService;
import com.tlc.web.platform.core.util.tlcConstants;
import com.tlc.web.platform.core.vo.SignatureCollectionVO;
import com.aem.tlc.core.models.PropertyDetailsModel;

/**
 * 
 * Sling Implementation of Member Benefits Model
 * 
 * @author DWAO
 *
 */

@Model(adaptables = { SlingHttpServletRequest.class,
		Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MemberBenefitsModel {

	private static Logger log = LoggerFactory.getLogger(MemberBenefitsModel.class);

	@SlingObject
	private SlingHttpServletRequest request;

	@Inject
	MemberBenefitsListingService memberBenefitsListingService;

	@Inject
	SearchService sarchService;

	@Inject
	RequestPathInfo requestPathInfo;

	@Inject
	Page currentPage;
	private List<CertificateModel> certificateList;
	private List<CardBenefitsModel> cardBenefitsList;
	private List<SignatureCollectionVO> signatureCollectionList;
	private List<TermsConditionsModel> membershipTermsList;
	
	PropertyDetailsModel propertyModel;

	@PostConstruct
	public void init() {
		try {

			requestPathInfo = request.getRequestPathInfo();
			log.debug("requestPathInfo suffix :: {} ", requestPathInfo.getSuffix());

			String[] segments = requestPathInfo.getSuffix().split("/");
			String propertyName = segments[segments.length - 1];
			String cityName = segments[segments.length - 2];
			String pagePath = currentPage.getParent().getPath();
			
			String signPagePath = currentPage.getParent().getPath();
					
			if(StringUtils.equals("home", currentPage.getParent().getPath())) {
				signPagePath = currentPage.getParent().getPath();
					} else {
				signPagePath = currentPage.getParent().getParent().getPath();
					}
					
			ValueMap vm = currentPage.getContentResource().getValueMap();

			String signatureCollectionPath = vm.get("signatureCollectionPath", String.class);
			String cityPath = vm.get("cityPath", String.class);
			String membershipPath = vm.get("membershipPath", String.class);
			String city = null;
			certificateList = new ArrayList<>();
			cardBenefitsList = new ArrayList<>();
			signatureCollectionList = new ArrayList<>();
			membershipTermsList = new ArrayList<>();

			memberBenefitsListingService.benefitsList(propertyName,pagePath,membershipPath);
			signatureCollectionList = memberBenefitsListingService.signatureCollectionsList(propertyName, city, signatureCollectionPath, signPagePath, tlcConstants.HOTELS_AND_RESORTS);
			membershipTermsList = memberBenefitsListingService.getMembershipTermsList(propertyName,membershipPath);
			certificateList = memberBenefitsListingService.getCertificateList();
			cardBenefitsList = memberBenefitsListingService.getCardBenefitsList();
			propertyModel = sarchService.getPropertyDetails(cityPath + tlcConstants.SLASH + cityName + tlcConstants.SLASH + propertyName);
		
		} catch (Exception e) {
			log.error("Exception in Member Benefits Model  :: ", e);
		}

	}

	public List<CertificateModel> getCertificateList() {
		return certificateList;
	}

	public List<CardBenefitsModel> getCardBenefitsList() {
		return cardBenefitsList;
	}

	public List<SignatureCollectionVO> getSignatureCollectionList() {
		return signatureCollectionList;
	}

	public List<TermsConditionsModel> getMembershipTermsList() {
		return membershipTermsList;
	}

	public PropertyDetailsModel getPropertyModel() {
		return propertyModel;
	}

}
