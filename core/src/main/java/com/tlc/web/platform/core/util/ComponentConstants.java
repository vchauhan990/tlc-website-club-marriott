package com.tlc.web.platform.core.util;

public class ComponentConstants {
	
	public static final String BANNER_COMP_NAME = "banner";
	public static final String MEMBESHIP_BENEFITS_COMP_NAME = "membershipbenefits";
	public static final String MEMBESHIP_TERMS_COMP_NAME = "membershipterms";
	public static final String UPGRADE_MEMBERSHIP_COMP_NAME = "upgrademembership";
	public static final String EVENTS_COMP_NAME = "events";
	public static final String MEETINGS_COMP_NAME = "meetings";
	public static final String CONTENT_FRAGMENT_COMP_NAME = "contentfragment";
	public static final String SECTION_HEADER_COMP_NAME = "sectionheader";
	public static final String PRIVACYSTATEMENT = "privacystatement";
	public static final String ACTIVATION_POPUP_COMP_NAME = "activationpopup";
	public static final String PROMOTIONAL_POPUP_COMP_NAME = "promotionalpopup";
	public static final String PROPERTY_COMP_NAME = "property";
	public static final String OUTLET_COMP_NAME = "outlet";
	public static final String OFFERS_EVENTS_COMP_NAME = "offersevents";
	public static final String CERTIFICATE_COMP_NAME = "certificate";
	public static final String PROMOTIONCONTENT_COMP_NAME = "promotioncontent";
	public static final String AT_THE_HOTEL_COMP_NAME = "atthehotel";
	public static final String CARDBENEFIT_COMP_NAME = "cardbenefit";
	public static final String HOME_CARD_COMP_NAME = "homecard";
	public static final String PLAN_WITH_COMP_NAME = "planwith";
	public static final String ACTIVITIES_COMP_NAME = "activities";
	public static final String DISPLAY_CONTROLS_COMP_NAME = "displaycontrols";
	public static final String PROMOTIONS_COMP_NAME = "promotions";
	public static final String DETAIL_PAGE_1_COMP_NAME = "detailpage1";
	public static final String DETAIL_PAGE_2_COMP_NAME = "detailpage2";
	public static final String DETAIL_PAGE_3_COMP_NAME = "detailpage3";
	public static final String DETAIL_PAGE_4_COMP_NAME = "detailpage4";
	public static final String EXTERNAL_LINK_COMP_NAME = "externallink";
	public static final String EXTERNAL_LINK_IMAGE_COMP_NAME = "externallinkimage";
	public static final String PROFILE_FAQ_COMP_NAME = "profilefaq";
	
	
	public static final String ACTIVITIES_LIST_NODE_NAME = "activitiesList";
	public static final String MEETINGS_LIST_NODE_NAME = "meetingsList";
	public static final String EVENTS_LIST_NODE_NAME = "eventsList";
	public static final String DINING_NODE_NAME = "dining";
	public static final String SPA_NODE_NAME = "spa";
	public static final String LUXURY_STAY_NODE_NAME = "luxury-stay";
	public static final String TAB_1_FEATURES_NODE_NAME = "tab1Features";
	public static final String TAB_2_FEATURES_NODE_NAME = "tab2Features";
	public static final String TAB_3_FEATURES_NODE_NAME = "tab3Features";
	public static final String MORE_FEATURES_NODE_NAME = "moreFeatures";
	public static final String KEY_FEATURES_NODE_NAME = "keyFeatures";
	public static final String APP_BENEFITS_NODE_NAME = "appBenefits";
	public static final String GROUP_1_FEATURES_NODE_NAME = "group1Features";
	public static final String GROUP_2_FEATURES_NODE_NAME = "group2Features";
	public static final String BENEFITS_NODE_NAME = "benefits";
	public static final String BLOGS_NODE_NAME = "blogs";
	public static final String BTN_LABELS_NODE_NAME = "btnLabels";
	public static final String MENU_NODE_NAME = "menu";
	public static final String ASSET_NODE_NAME = "asset";
	public static final String IMAGE_NODE_NAME = "image";
	public static final String FEATURE_NODE_NAME = "feature";
	public static final String TEXT_IMAGE_NODE_NAME = "textImage";
	public static final String FAQS_NODE_NAME = "faqs";
	
}
