package com.tlc.web.platform.core.vo;

import java.util.List;


public class AccordianVO {
	
	private String heading;
	private String text;
	private List<AccordianVO> accordianItem;
	private String parsysId;
	int count = 0;
	
	
	
	public String getParsysId() {
		return parsysId;
	}
	public void setParsysId(String parsysId) {
		
		this.parsysId = parsysId;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		count++;
		setParsysId("accord-par-"+count);
		
		this.text = text;
	}
	public List<AccordianVO> getAccordianItem() {
		return accordianItem;
	}
	public void setAccordianItem(List<AccordianVO> accordianItem) {
		this.accordianItem = accordianItem;
	}
	

}
