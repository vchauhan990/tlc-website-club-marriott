package com.tlc.web.platform.core.vo;

public class NavigationVO {

	private String navigationLink;
	private String navigationTitle;
	public String getNavigationLink() {
		return navigationLink;
	}
	public void setNavigationLink(String navigationLink) {
		this.navigationLink = navigationLink;
	}
	public String getNavigationTitle() {
		return navigationTitle;
	}
	public void setNavigationTitle(String navigationTitle) {
		this.navigationTitle = navigationTitle;
	}

}
