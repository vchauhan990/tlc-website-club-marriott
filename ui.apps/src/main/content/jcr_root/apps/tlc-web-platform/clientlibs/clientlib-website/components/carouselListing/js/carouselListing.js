var jsonx;
var rawTemplate;
function createHTML(commentsData) {

	var compiledTemplate = Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(commentsData);
	var cmtsContainer = document.getElementById("page-list");
	cmtsContainer.innerHTML = ourGeneratedHTML;
}

$(document).ready(function() {
	
    var filterValue = $("#cm-generic-carousel").attr("tagFilterValue");
	if(filterValue=="true"){
	
	rawTemplate = document.getElementById("handlebars-demo").innerHTML;
	jsonx = JSON.parse($("#listingCategory").attr('data-attr-pageList'));
	
        var tagVal = $("#listingCategory").attr("data-attr-tag");
		filterData(tagVal);
	}
    
		  for(var i = 0 ; i < $('.cm-carousel .carousel').length; i++){
		      $('.cm-carousel .carousel')[i].id = 'cm-carousel' + i;
		      $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-item')[0].classList.add('active') ;
		  
		      if($('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-item').length == 1){
		        $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-control-prev')[0].classList.add('is-hidden');
		        $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-control-next')[0].classList.add('is-hidden');
		        
		  
		      }else{
		        generateArrows('#'+ $('.cm-carousel .carousel')[i].id);
		      }
		  
            }
            
        
});


function generateArrows(id){
    $(id + ' .carousel-control-next').attr('href',id);
    $(id + ' .carousel-control-prev').attr('href',id)
  }

function filterData(keyz) {
    var newjsonObj = [];
	for ( var key in jsonx) {
		var obj = jsonx[key];
		var check = jQuery.inArray(keyz, obj["tags"]);
		if (check != -1) {
			newjsonObj.push(obj);
		}
    }
    createHTML(newjsonObj);
}