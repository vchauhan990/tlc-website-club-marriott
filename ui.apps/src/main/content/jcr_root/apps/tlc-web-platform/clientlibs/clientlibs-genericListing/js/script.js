(function (document, $, Coral) {
	"use strict";    
	var mode, defaultMode;

	$(document).on("foundation-contentloaded", function(e) {

		/**
		 * Initializing Variables and functions on Load time (Dialog-Ready Event)
		 */  
		Coral.commons.ready(function (component) {
			defaultMode = $(".mode-type").val();
            console.log(defaultMode);
			showHide(defaultMode);
        });

		/**
		 *	Change Method for the View Type Drop-down
		 */
		$('.mode-type').on('change', function(event) {
			mode = $(this).val();
			if(mode === "auto") {
				$(".manual-authoring").parent().hide();
                $(".manual-listing").parent().hide(); 
                $(".auto-listing").parent().show();
                $(".tagFilter").show();
                $(".tag-listing").parent().show();
			} else if(mode === "manual") {
                $(".auto-listing").parent().hide();
				$(".manual-authoring").parent().show();
                $(".manual-listing").parent().hide();
                $(".tagFilter").hide();
                $(".tag-listing").parent().hide();
            } else {
				$(".auto-listing").parent().hide();
				$(".manual-authoring").parent().hide();
                $(".manual-listing").parent().show();
                $(".tagFilter").show();
                $(".tag-listing").parent().show();
            }
		});

        function showHide(defaultMode) {
			if(defaultMode === "auto") {
				$(".manual-authoring").parent().hide();
                $(".manual-listing").parent().hide(); 
                $(".auto-listing").parent().show();
                $(".tagFilter").show();
                $(".tag-listing").parent().show();
			} else if(mode === "manual") {
                $(".auto-listing").parent().hide();
				$(".manual-authoring").parent().show();
                $(".manual-listing").parent().hide();
                $(".tagFilter").hide();
                $(".tag-listing").parent().hide();
            } else {
				$(".auto-listing").parent().hide();
				$(".manual-authoring").parent().hide();
                $(".manual-listing").parent().show();
                $(".tagFilter").show();
                $(".tag-listing").parent().show();
            }
        }

	});
})(document, Granite.$, Coral);