(function (document, $, Coral) {
	"use strict";    
	var mode, pageTitle;
	$(document).on("foundation-contentloaded", function(e) {

		/**
		 * Initializing Variables and functions on Load time (Dialog-Ready Event)
		 */  
		Coral.commons.ready(function (component) {
			pageTitle = $(".title").val();
            console.log("pageTitle" , pageTitle);
			showHide(pageTitle);
        });

        function showHide(pageTitle) {
			if(pageTitle === "Benefits") {
				$(".cityPath").parent().show();
                $(".membershipPath").parent().hide(); 
                $(".uniqueExpPath").parent().hide();
                $(".signCollPath").parent().hide();
                    $(".defaultImage").parent().hide();
			} else if(pageTitle === "Experiences") {
                $(".cityPath").parent().show();
                $(".membershipPath").parent().show(); 
                $(".uniqueExpPath").parent().show();
                $(".signCollPath").parent().show();
                    $(".defaultImage").parent().hide();
            } else if(pageTitle === "Hotels and Resorts") {
				$(".cityPath").parent().show();
				$(".membershipPath").parent().show();
                $(".uniqueExpPath").parent().show();
                $(".signCollPath").parent().show();
                 $(".defaultImage").parent().show();

            }else {
				$(".cityPath").parent().show();
				$(".membershipPath").parent().show();
                $(".uniqueExpPath").parent().show();
                $(".signCollPath").parent().show();
                    $(".defaultImage").parent().hide();
            }
        }

	});
})(document, Granite.$, Coral);