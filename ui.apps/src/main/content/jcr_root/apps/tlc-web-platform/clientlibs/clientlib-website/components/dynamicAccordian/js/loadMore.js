$(document).ready(function(){
  var totalCardBenefits = $(".card-benefits").length;
  var totalCertificates = $(".certificates").length;
  var totalSignatureCollections = $(".signatureCollections").length;
  $("#benefitsLoad").on("click",function(){
      var count = 0;
      $(".card-benefits").each(function(index){
          
          if(count < 4) {
              if($(this).hasClass("no-display")) {
                  $(this).removeClass("no-display");
                  count++;
                  if(index == (totalCardBenefits-1)){
                     
                      $("#benefitsLoad").hide();
                      
                  }
              }
          }
      });
  });

  $("#certificateLoad").on("click",function(){
      var count = 0;
      $(".certificates").each(function(index){
          if(count < 4) {
              if($(this).hasClass("no-display")) {
                  $(this).removeClass("no-display");
                  count++;
                  if(index == (totalCertificates-1)){
                      
                      $("#certificateLoad").hide();
                      
                  }
              }
          }
      });
  });

  $("#signatureLoad").on("click",function(){
      var count = 0;
      $(".signatureCollections").each(function(index){
          if(count < 4) {
              if($(this).hasClass("no-display")) {
                  $(this).removeClass("no-display");
                  count++;
                  if(index == (totalSignatureCollections-1)){
                      
                      $("#signatureLoad").hide();
                      
                  }
              }
          }
      });
  });
});