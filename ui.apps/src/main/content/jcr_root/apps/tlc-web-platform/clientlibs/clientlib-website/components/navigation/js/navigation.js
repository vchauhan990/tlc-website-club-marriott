$(document).ready(function(){
  
    var pathname = window.location.pathname;
  
    $('.cm-menu-link > a[href="'+pathname+'"]').parent().addClass('cm-header-menu-list-item-active');

    if(pathname.includes("unique-experience-catalogue") || pathname.includes("signature-catalogue") || pathname.includes("offer-event-catalogue")){

        $('.cm-menu-link').each(function(index) {
                       
           var tabName = $(this).attr("data-tab");
           
           if(tabName == "Experiences"){
            $(this).addClass('cm-header-menu-list-item-active');                        
           }
        });

    }
    
    if(pathname.includes("property-page") || pathname.includes("outlet-catalogue")){
        
        $('.cm-menu-link').each(function(index) {
        	
           var tabName = $(this).attr("data-tab");
          
           if(tabName == "Hotels & Resorts"){
            $(this).addClass('cm-header-menu-list-item-active');                       
           }
        });      
    }
})