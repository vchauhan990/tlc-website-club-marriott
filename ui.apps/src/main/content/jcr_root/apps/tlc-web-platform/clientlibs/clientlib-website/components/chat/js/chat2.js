(function(V) {
	var O = "embeddedServiceHelpButton";
	var aD = ".embeddedServiceSidebar";
	var ar = [ ".salesforce.com", ".force.com", ".sfdc.net" ];
	var ah = "esw_storage_iframe";
	var P = "snapins_invite";
	var ad = "esw-snapin-target";
	var l = "slds-scope";
	var E = "5.0";
	var aB = 100;
	var L = "__snapinsLoginCallback";
	var t = "__snapinsLogoutCallback";
	var R = "ESW_OAUTH_TOKEN";
	var aq = "ESW_BODY_SCROLL_POSITION";
	var e = "ESW_IS_MINIMIZED";
	var aA = "ESW_MINIMIZED_TEXT";
	var i = 'dir="rtl"';
	function B() {
		var aF = false;
		var aG;
		this.settings = {
			appendHelpButton : true,
			displayHelpButton : true,
			isExternalPage : true,
			devMode : false,
			targetElement : document.body,
			elementForOnlineDisplay : undefined,
			elementForOfflineDisplay : undefined,
			defaultMinimizedText : "",
			disabledMinimizedText : "",
			defaultAssistiveText : "",
			loadingText : "Loading",
			showIcon : undefined,
			enabledFeatures : [],
			entryFeature : "FieldService",
			storageDomain : document.domain,
			language : undefined,
			linkAction : {
				feature : undefined,
				name : undefined,
				valid : false
			},
			linkActionParameters : {},
			useCustomAuthentication : false,
			allowGuestUsers : false,
			requireSLDS : false
		};
		this.auth = {};
		this.validLinkActions = {};
		this.isMasterAndHasSlaves = false;
		Object.defineProperty(this.auth, "oauthToken", {
			get : function() {
				return aG;
			},
			set : function(aH) {
				if (this.validateHeaderValue(aH)) {
					aG = aH;
					if (aH) {
						this.setSessionData(R, aH);
						this.checkAuthentication();
					} else {
						this.deleteSessionData(R);
					}
				} else {
					this.error('"' + aH + '" is not a valid OAuth token.');
				}
			}.bind(this)
		});
		this.featureScripts = {};
		this.storedEventHandlers = {};
		this.messageHandlers = {};
		this.storageKeys = [ aq, e, aA, R ];
		this.defaultSettings = {};
		this.snippetSettingsFile = {};
		this.eswFrame = undefined;
		this.availableFeatures = [ "script", "session" ];
		this.outboundMessagesAwaitingIframeLoad = [];
		this.pendingMessages = {};
		this.iframeScriptsToLoad = [];
		this.domInitInProgress = false;
		this.componentInitInProgress = false;
		this.hasSessionDataLoaded = false;
		this.isIframeReady = false;
		this.isAuthenticationRequired = false;
		this.loginPendingSerializedData = undefined;
		Object.defineProperty(this, "isButtonDisabled", {
			get : function() {
				return aF;
			},
			set : function(aH) {
				aF = aH;
				this.onButtonStatusChange();
			}.bind(this),
			configurable : true
		});
		this.setupMessageListener();
		this.getLinkActionData();
	}
	function S() {
		return window.$A && typeof window.$A.get === "function"
				&& window.$A.get("$Site");
	}
	B.prototype.getLightningOutParamsObj = function() {
		var aF;
		if (embedded_svc.config && embedded_svc.config.additionalSettings
				&& embedded_svc.config.additionalSettings.labelsLanguage) {
			aF = {
				guestUserLang : embedded_svc.config.additionalSettings.labelsLanguage
			};
		} else {
			if (embedded_svc.settings.language
					&& embedded_svc.settings.language.trim() !== "") {
				aF = {
					guestUserLang : embedded_svc.settings.language
				};
			}
		}
		return aF;
	};
	B.prototype.adjustCommunityStorageDomain = function() {
		if (this.isCommunityDomain(this.settings.storageDomain)) {
			this.settings.storageDomain = this.settings.storageDomain + "/"
					+ window.location.pathname.split("/")[1];
		}
	};
	B.prototype.loadLightningOutScripts = function(aF) {
		if (typeof Promise !== "function") {
			this.loadScriptFromDirectory("common", "promisepolyfill",
					function() {
						return this.loadLightningOutScripts(aF);
					}.bind(this), true);
		} else {
			return new Promise(
					function(aK, aJ) {
						var aG;
						var aI;
						try {
							aG = aF && aF.baseCoreURL ? aF.baseCoreURL
									: embedded_svc.settings.baseCoreURL;
							if (window.$Lightning) {
								aK("Lightning Out is already loaded on this page.");
							} else {
								if (S()) {
									aK("Communities context does not require Lightning Out to use Embedded Service.");
								} else {
									if (aG) {
										aI = document.createElement("script");
										aI.type = "text/javascript";
										aI.src = aG
												+ "/lightning/lightning.out.js";
										aI.onload = function() {
											aK("Lightning Out scripts loaded.");
										};
										document.getElementsByTagName("head")[0]
												.appendChild(aI);
									}
								}
							}
						} catch (aH) {
							aJ(aH);
						}
					});
		}
	};
	B.prototype.instantiateLightningOutApplication = function(aF) {
		if (typeof Promise !== "function") {
			this.loadScriptFromDirectory("common", "promisepolyfill",
					function() {
						return this.instantiateLightningOutApplication(aF);
					}.bind(this), true);
		} else {
			return new Promise(
					function(aK, aJ) {
						var aI;
						var aH;
						var aL;
						try {
							aI = aF && aF.communityEndpointURL ? aF.communityEndpointURL
									: embedded_svc.settings.communityEndpointURL;
							aH = aF && aF.oauthToken ? aF.oauthToken
									: embedded_svc.settings.oauthToken;
							aL = aF && aF.paramsObj ? aF.paramsObj
									: embedded_svc.getLightningOutParamsObj()
											|| undefined;
							if (S()) {
								aK("Communities context already has an Aura context.");
							} else {
								if (window.$Lightning) {
									$Lightning
											.use(
													"embeddedService:sidebarApp",
													function() {
														aK("Lightning Out application request complete.");
													}, aI, aH, aL);
								}
							}
						} catch (aG) {
							aJ(aG);
						}
					});
		}
	};
	B.prototype.createEmbeddedServiceComponent = function(aF) {
		if (typeof Promise !== "function") {
			this.loadScriptFromDirectory("common", "promisepolyfill",
					function() {
						return this.createEmbeddedServiceComponent(aF);
					}.bind(this), true);
		} else {
			return new Promise(
					function(aK, aJ) {
						var aI;
						var aH;
						try {
							aI = aF && aF.attributes ? aF.attributes : {
								configurationData : embedded_svc.settings
							};
							aH = aF && aF.locator ? aF.locator
									: embedded_svc.settings.targetElement;
							embedded_svc.preparePageForSidebar();
							if (window.$Lightning
									&& !document.querySelector(aD)) {
								$Lightning
										.ready($Lightning.createComponent
												.bind(
														this,
														"embeddedService:sidebar",
														aI,
														aH,
														function(aN, aL, aM) {
															if (aL === "SUCCESS") {
																embedded_svc.utils
																		.addEventHandler(
																				"afterInitialization",
																				function() {
																					aK("Embedded Service component created.");
																				});
															} else {
																aJ(aM);
															}
														}));
							} else {
								if (S()) {
									window.dispatchEvent(new CustomEvent(
											"embeddedServiceCreateSidebar", {
												detail : {
													componentAttributes : aI,
													resolve : aK,
													reject : aJ
												}
											}));
								} else {
									typeof window.$Lightning === "undefined" ? aK("Lightning Out should be loaded on this page before creating the Embedded Service component.")
											: aK("Embedded Service component already exists.");
								}
							}
						} catch (aG) {
							aJ(aG);
						}
					});
		}
	};
	B.prototype.bootstrapEmbeddedService = function(aF) {
		if (typeof Promise !== "function") {
			this.loadScriptFromDirectory("common", "promisepolyfill",
					function() {
						return this.bootstrapEmbeddedService(aF);
					}, true);
		} else {
			return new Promise(
					function(aI, aH) {
						try {
							embedded_svc
									.loadLightningOutScripts(aF)
									.then(
											function() {
												embedded_svc
														.instantiateLightningOutApplication(
																aF)
														.then(
																function() {
																	embedded_svc
																			.createEmbeddedServiceComponent(
																					aF)
																			.then(
																					function() {
																						window
																								.requestAnimationFrame(function() {
																									aI("Embedded Service application and component bootstrapped.");
																								});
																					});
																});
											});
						} catch (aG) {
							aH(aG);
						}
					});
		}
	};
	B.prototype.isInternetExplorer = function() {
		return "ActiveXObject" in window;
	};
	B.prototype.outputToConsole = function N(aH, aG, aF) {
		if ((aF || this.settings.devMode) && console && console[aH]) {
			console[aH]("[Snap-ins] "
					+ (Array.isArray(aG) ? aG.join(", ") : aG));
		}
	};
	B.prototype.log = function f() {
		this.outputToConsole("log", [].slice.apply(arguments));
	};
	B.prototype.error = function ag(aG, aF) {
		if (aG) {
			this.outputToConsole("error", aG, aF);
		} else {
			this.outputToConsole("error",
					"esw responed with an unspecified error.", aF);
		}
		embedded_svc.utils.fireEvent("error");
	};
	B.prototype.warning = function Q(aG, aF) {
		if (aG) {
			this.outputToConsole("warn", "Warning: " + aG, aF);
		} else {
			this.outputToConsole("warn", "esw sent an anonymous warning.", aF);
		}
	};
	B.prototype.deprecated = function r(aF) {
		this.warning(aF + " is deprecated in version " + Number(E).toFixed(1)
				+ " and will be removed in version "
				+ (Number(E) + 1).toFixed(1));
	};
	B.prototype.getCookie = function p(aH) {
		var aI = document.cookie;
		var aG;
		var aF;
		if (aI) {
			aG = aI.indexOf(aH + "=");
			if (aG !== -1) {
				aG += (aH + "=").length;
				aF = aI.indexOf(";", aG);
				if (aF === -1) {
					aF = aI.length;
				}
				return aI.substring(aG, aF);
			}
		}
		return undefined;
	};
	B.prototype.setCookie = function am(aH, aJ, aF) {
		var aI = aH + "=" + aJ + ";";
		var aG;
		if (aF) {
			aG = new Date();
			aG.setFullYear(aG.getFullYear() + 10);
			aI += "expires=" + aG.toUTCString() + ";";
		}
		aI += "path=/;";
		document.cookie = aI;
	};
	B.prototype.mergeSettings = function az(aF) {
		Object.keys(aF).forEach(function(aG) {
			if (this.settings[aG] === undefined) {
				this.settings[aG] = aF[aG];
			}
		}.bind(this));
	};
	B.prototype.loadFeatureScript = function M(aF, aH) {
		var aG = decodeURI(aF).toLowerCase();
		if (aF.indexOf("..") === -1) {
			this.loadScriptFromDirectory("client", aG + ".esw", function() {
				this.featureScripts[aF](this);
				this.availableFeatures.push(aG);
				embedded_svc.utils.fireEvent("featureLoaded", undefined, aF);
				if (aH) {
					aH();
				}
				this.processPendingMessages(aG);
			}.bind(this));
		} else {
			this.error('"' + aF + '" is not a valid feature name.');
		}
	};
	B.prototype.fireEvent = function Z(aG, aF) {
		var aH = [].slice.apply(arguments).slice(2);
		if (window.embedded_svc && embedded_svc.utils) {
			return embedded_svc.utils.fireEvent(aG, aF, aH);
		}
		this.error("fireEvent should not be called before calling init!");
		return true;
	};
	B.prototype.isValidEntityId = function at(aF) {
		return typeof aF === "string" && (aF.length === 18 || aF.length === 15);
	};
	B.prototype.getKeyPrefix = function A(aF) {
		if (this.isValidEntityId(aF)) {
			return aF.substr(0, 3);
		}
		return undefined;
	};
	B.prototype.isOrganizationId = function an(aF) {
		return this.getKeyPrefix(aF) === "00D";
	};
	B.prototype.getESWFrame = function aC() {
		var aF = document.getElementById(ah);
		if (!this.eswFrame && aF) {
			this.eswFrame = aF.contentWindow;
		}
		return this.eswFrame;
	};
	B.prototype.isFrameStorageEnabled = function c() {
		this.deprecated("isFrameStorageEnabled");
		return true;
	};
	B.prototype.processPendingMessages = function a(aF) {
		if (this.pendingMessages[aF]) {
			this.pendingMessages[aF].forEach(function(aG) {
				this.handleMessage(aG.payload);
			}.bind(this));
			this.pendingMessages[aF] = undefined;
		}
	};
	B.prototype.loadCSS = function F() {
		var aF = document.createElement("link");
		var aG = this.settings.gslbBaseURL ? this.settings.gslbBaseURL
				: this.settings.baseCoreURL;
		aF.href = aG + "/embeddedservice/" + this.settings.releaseVersion
				+ "/esw" + (this.settings.devMode ? "" : ".min") + ".css";
		aF.type = "text/css";
		aF.rel = "stylesheet";
		document.getElementsByTagName("head")[0].appendChild(aF);
	};
	B.prototype.appendHelpButton = function Y(aG) {
		var aH = document.createElement("div");
		var aF = "";
		aH.className = O;
		if (this.isLanguageRtl(this.settings.language) && this.isDesktop()) {
			aF = i;
		}
		aH.innerHTML = '<div class="helpButton"'
				+ aF
				+ ">"
				+ '<button class="helpButtonEnabled uiButton" href="javascript:void(0)">'
				+ '<span class="embeddedServiceIcon" aria-hidden="true" data-icon="&#59648;"></span>'
				+ '<span class="helpButtonLabel" id="helpButtonSpan" aria-live="polite" aria-atomic="true">'
				+ '<span class="assistiveText">'
				+ (this.settings.defaultAssistiveText || "") + "</span>"
				+ '<span class="message"></span>' + "</span>" + "</button>"
				+ "</div>";
		if (!aG) {
			aH.style.display = "none";
		}
		this.settings.targetElement.appendChild(aH);
		this.setHelpButtonText(this.settings.defaultMinimizedText);
		if ("ontouchstart" in document.documentElement) {
			[].slice
					.apply(
							document
									.querySelectorAll(".embeddedServiceHelpButton .uiButton"))
					.forEach(function(aI) {
						aI.classList.add("no-hover");
					});
		}
		this.onButtonStatusChange();
	};
	B.prototype.appendIFrame = function J() {
		var aG = document.createElement("iframe");
		var aF = {};
		aG.id = ah;
		aG.src = this.settings.iframeURL;
		aG.style.display = "none";
		aG.onload = function() {
			var aH = this.getESWFrame();
			this.isIframeReady = true;
			this.outboundMessagesAwaitingIframeLoad.forEach(function(aI) {
				aH.postMessage(aI, this.settings.iframeURL);
			}.bind(this));
			this.outboundMessagesAwaitingIframeLoad = [];
			this.iframeScriptsToLoad.forEach(function(aI) {
				this.loadStorageScript(aI);
			}.bind(this));
			aF.deploymentId = this.settings.deploymentId;
			aF.isSamePageNavigation = this.isSamePageNavigation();
			aF.isRefresh = window.performance.navigation.type === 1;
			this.postMessage("session.updateStorage", aF);
			this.iframeScriptsToLoad = [];
		}.bind(this);
		this.settings.targetElement.appendChild(aG);
		window
				.addEventListener(
						"beforeunload",
						function(aH) {
							var aI = "You might lose the active chat session if you close this tab. Are you sure?";
							if (this.isInternetExplorer()) {
								aG.src = "about:blank";
							}
							if (this.isMasterAndHasSlaves) {
								embedded_svc.utils
										.fireEvent("snapinsCloseSessionWarning");
								if (this.settings.closeSessionWarning
										&& typeof this.settings.closeSessionWarning === "function") {
									this.settings.closeSessionWarning();
								} else {
									(aH || window.event).returnValue = aI;
									return aI;
								}
							}
							if (!this.settings.__synchronous_decrement_tab) {
								this.postMessage(
										"chasitor.decrementActiveChatSession",
										this.settings.deploymentId);
							}
						}.bind(this), false);
	};
	B.prototype.preparePageForSidebar = function j() {
		var aF = {};
		if (document.getElementById(P) && embedded_svc.inviteAPI) {
			embedded_svc.inviteAPI.inviteButton.setOnlineState(false);
		}
		embedded_svc.utils.fireEvent("beforeCreate");
		Object.keys(this.settings).forEach(function(aG) {
			aF[aG] = this.settings[aG];
		}.bind(this));
		this.mergeSettings(this.defaultSettings);
	};
	B.prototype.createLightningComponent = function w(aF) {
		this.preparePageForSidebar();
		this.createEmbeddedServiceComponent({
			attributes : {
				configurationData : this.settings,
				serializedSessionData : aF
			},
			locator : this.settings.targetElement
		}).then(function() {
			this.hideHelpButton();
			this.componentInitInProgress = false;
			this.setHelpButtonText(this.settings.defaultMinimizedText);
			embedded_svc.utils.fireEvent("ready");
		}.bind(this));
	};
	B.prototype.loadLightningApp = function ap(aG) {
		var aF;
		var aH;
		if (this.settings.isExternalPage
				&& typeof this.settings.communityEndpointURL !== "string") {
			throw new Error("communityEndpointURL String property not set");
		}
		aF = document.getElementsByClassName("helpButton")[0];
		if (aF) {
			aH = aF.getBoundingClientRect().width;
			if (aH > 0) {
				aF.style.width = aH + "px";
			}
		}
		this.setHelpButtonText(this.settings.loadingText, false);
		this.instantiateLightningOutApplication({
			communityEndpointURL : this.settings.communityEndpointURL,
			oauthToken : this.auth.oauthToken
		}).then(this.createLightningComponent.bind(this, aG));
	};
	B.prototype.initLightningOut = function aa(aF) {
		if (this.hasSessionDataLoaded) {
			if (typeof Promise !== "function") {
				this.loadScriptFromDirectory("common", "promisepolyfill",
						function() {
							this.initLightningOut(aF);
						}.bind(this), true);
			} else {
				this.loadLightningOutScripts().then(
						this.loadLightningApp.bind(this, aF));
			}
		}
	};
	B.prototype.setHelpButtonText = function m(aJ, aL) {
		var aK = this.settings.showIcon === undefined ? true
				: this.settings.showIcon;
		var aF = aL === undefined ? aK : aL;
		var aI = document.getElementById("helpButtonSpan");
		var aG;
		var aH;
		if (aI) {
			aG = aI.querySelector(".message");
			aG.innerHTML = aJ;
			aH = aI.parentElement.querySelector(".embeddedServiceIcon");
			if (aH) {
				aH.style.display = aF ? "inline-block" : "none";
			}
		}
	};
	B.prototype.prepareDOM = function ab() {
		if (this.domInitInProgress) {
			return;
		}
		this.domInitInProgress = true;
		this.appendIFrame();
	};
	B.prototype.addSessionHandlers = function aE() {
		this.addMessageHandler("session.onLoad", function() {
			this.postMessage("session.get", this.storageKeys);
		}.bind(this));
		this.addMessageHandler("session.sessionData", function(aF) {
			this.resumeInitWithSessionData(aF);
		}.bind(this));
		this.addMessageHandler("session.deletedSessionData", function(aF) {
			if (aF.indexOf("CHASITOR_SERIALIZED_KEY") > -1) {
				this.loginPendingSerializedData = undefined;
			}
		}.bind(this));
		this.addMessageHandler("session.updateMaster", function(aF) {
			if (aF) {
				if (aF.isMaster) {
					sessionStorage.setItem(this.settings.storageDomain
							+ "MASTER_DEPLOYMENT_ID",
							this.settings.deploymentId);
				} else {
					sessionStorage.removeItem(this.settings.storageDomain
							+ "MASTER_DEPLOYMENT_ID");
				}
				this.isMasterAndHasSlaves = aF.activeChatSessions > 1
						&& aF.isMaster;
				if (embedded_svc && embedded_svc.liveAgentAPI) {
					embedded_svc.liveAgentAPI.browserSessionInfo = aF;
				}
			}
		}.bind(this));
	};
	B.prototype.addMetaTag = function(aF, aH) {
		var aG = document.createElement("meta");
		aG.name = aF;
		aG.content = aH;
		document.head.appendChild(aG);
	};
	B.prototype.init = function T(aH, aJ, aG, aF, aK, aI) {
		this.settings.baseCoreURL = aH;
		this.settings.communityEndpointURL = aJ;
		this.settings.gslbBaseURL = aG ? aG : aH;
		this.settings.orgId = aF;
		this.settings.releaseVersion = E;
		this.settings.eswConfigDevName = aK;
		this.settings.disableDeploymentDataPrefetch = true;
		this.mergeSettings(aI || {});
		this.adjustCommunityStorageDomain();
		if (typeof this.settings.baseCoreURL !== "string") {
			throw new Error("Base core URL value must be a string.");
		}
		if (!this.isOrganizationId(this.settings.orgId)) {
			throw new Error("Invalid OrganizationId Parameter Value: "
					+ this.settings.orgId);
		}
		if (embedded_svc.utils) {
			this.finishInit();
		} else {
			this.loadScriptFromDirectory("utils", "common", this.finishInit
					.bind(this));
		}
	};
	B.prototype.finishInit = function aw() {
		if (this.storedEventHandlers) {
			Object.getOwnPropertyNames(this.storedEventHandlers).forEach(
					function(aF) {
						this.storedEventHandlers[aF].forEach(function(aG) {
							embedded_svc.utils.addEventHandler(aF, aG);
						});
					}.bind(this));
			this.storedEventHandlers = {};
		}
		if (embedded_svc.utils.fireEvent("validateInit", function(aF) {
			return aF.indexOf(false) !== -1;
		}, this.settings)) {
			return;
		}
		this.checkForNativeFunctionOverrides();
		if (this.settings.appendHelpButton) {
			this.loadCSS();
		}
		if (!this.settings.targetElement) {
			throw new Error("No targetElement specified");
		}
		this.settings.iframeURL = this.settings.gslbBaseURL
				+ "/embeddedservice/" + this.settings.releaseVersion
				+ (this.settings.devMode ? "/eswDev.html" : "/esw.html")
				+ "?parent=" + document.location.href;
		this.addSessionHandlers();
		this.loadFeatures(this.onFeatureScriptsLoaded.bind(this));
		embedded_svc.utils.fireEvent("afterInit", undefined, this.settings);
	};
	B.prototype.onFeatureScriptsLoaded = function U() {
		if (document.readyState === "complete") {
			setTimeout(this.prepareDOM.bind(this), 1);
		} else {
			if (document.addEventListener) {
				document.addEventListener("DOMContentLoaded", this.prepareDOM
						.bind(this), false);
				window.addEventListener("load", this.prepareDOM.bind(this),
						false);
			} else {
				if (window.attachEvent) {
					window.attachEvent("onload", this.prepareDOM.bind(this));
				} else {
					this.log("No available event model. Exiting.");
				}
			}
		}
	};
	B.prototype.checkForNativeFunctionOverrides = function ak() {
		var aG = [ "addEventListener", "createAttribute", "createComment",
				"createDocumentFragment", "createElementNS", "createTextNode",
				"createRange", "getElementById", "getElementsByTagName",
				"getElementsByClassName", "querySelector", "querySelectorAll",
				"removeEventListener" ];
		var aH = [ "addEventListener", "clearTimeout", "dispatchEvent", "open",
				"removeEventListener", "requestAnimationFrame", "setInterval",
				"setTimeout" ];
		var aF = [ {
			name : "document",
			object : document,
			functions : aG
		}, {
			name : "window",
			object : window,
			functions : aH
		} ];
		aF
				.forEach(function(aI) {
					aI.functions
							.forEach(function(aJ) {
								if (aJ in aI.object
										&& !this
												.isNativeFunction(aI.object, aJ)) {
									this
											.warning(
													"Embedded Service Chat may not function correctly with this native JS function modified: "
															+ aI.name
															+ "."
															+ aJ, true);
								}
							}.bind(this));
				}.bind(this));
	};
	B.prototype.isNativeFunction = function ax(aF, aG) {
		return Function.prototype.toString.call(aF[aG])
				.match(/\[native code\]/);
	};
	B.prototype.onHelpButtonClick = function ai() {
		if (!this.componentInitInProgress
				&& !document.getElementsByClassName("embeddedServiceSidebar").length) {
			this.componentInitInProgress = true;
			try {
				this.checkAuthentication();
				embedded_svc.utils.fireEvent("onHelpButtonClick");
			} catch (aF) {
				this.componentInitInProgress = false;
				throw aF;
			}
		}
	};
	B.prototype.resumeInitWithSessionData = function W(aH) {
		var aG = embedded_svc.utils.fireEvent("sessionDataRetrieved", function(
				aJ) {
			return aJ.indexOf(true) !== -1;
		}, aH);
		var aF = false;
		var aI = false;
		if (this.settings.linkAction.valid) {
			aF = true;
		} else {
			if (aG) {
				this.log("Existing session found. Continuing with data: " + aH);
				aF = true;
				aI = true;
				if (embedded_svc.menu) {
					embedded_svc.menu.hideTopContainer();
				}
			} else {
				if (this.componentInitInProgress) {
					aF = true;
				}
			}
		}
		this.hasSessionDataLoaded = true;
		if (aH[R]) {
			this.auth.oauthToken = aH[R];
		}
		this.loginPendingSerializedData = aI ? aH : undefined;
		if (aF) {
			this.componentInitInProgress = true;
			this.checkAuthentication();
		}
		if (this.settings.appendHelpButton) {
			this.appendHelpButton(this.settings.displayHelpButton && !aG);
		}
	};
	B.prototype.checkAuthentication = function h() {
		if (this.isAuthenticationRequired && !this.settings.allowGuestUsers) {
			if (this.auth.oauthToken) {
				if (this.loginButtonPressed || this.componentInitInProgress) {
					this.initLightningOut(this.loginPendingSerializedData);
				}
			} else {
				embedded_svc.utils.fireEvent("requireauth");
			}
		} else {
			if (this.loginButtonPressed || this.componentInitInProgress) {
				this.initLightningOut(this.loginPendingSerializedData);
			}
		}
	};
	B.prototype.postMessage = function d(aI, aG) {
		var aF = {
			domain : this.settings.storageDomain,
			data : aG,
			method : aI
		};
		var aH = this.getESWFrame();
		if (aH) {
			aH.postMessage(aF, this.settings.iframeURL);
		} else {
			this.outboundMessagesAwaitingIframeLoad.push(aF);
		}
	};
	B.prototype.setSessionData = function o(aH, aF) {
		var aG;
		if (typeof aH === "object") {
			aG = aH;
		} else {
			aG = {};
			aG[aH] = aF;
		}
		this.postMessage("session.set", aG);
	};
	B.prototype.deleteSessionData = function ay(aF) {
		var aG;
		if (Array.isArray(aF)) {
			aG = aF;
		} else {
			aG = [ aF ];
		}
		this.postMessage("session.delete", aG);
	};
	B.prototype.defineFeature = function D(aF, aG) {
		this.featureScripts[aF] = aG;
	};
	B.prototype.registerStorageKeys = function y(aF) {
		if (typeof aF === "string") {
			this.storageKeys.push(aF);
		} else {
			aF.forEach(function(aG) {
				this.storageKeys.push(aG);
			}.bind(this));
		}
	};
	B.prototype.addMessageHandler = function av(aF, aG) {
		if (this.messageHandlers[aF]) {
			this
					.warning("Replacing an existing handler for message type "
							+ aF);
		}
		this.messageHandlers[aF] = aG;
	};
	B.prototype.loadStorageScript = function G(aF) {
		if (this.isIframeReady) {
			this.postMessage("script.load", aF);
		} else {
			this.iframeScriptsToLoad.push(aF);
		}
	};
	B.prototype.loadScriptFromDirectory = function X(aF, aH, aK, aL) {
		var aI = aH.toLowerCase();
		var aG = document.createElement("script");
		var aJ = this.settings.gslbBaseURL;
		aG.type = "text/javascript";
		aG.src = [ aJ, "embeddedservice",
				aL ? undefined : this.settings.releaseVersion, aF,
				aI + (this.settings.devMode ? "" : ".min") + ".js" ].filter(
				function(aM) {
					return Boolean(aM);
				}).join("/");
		if (aK) {
			aG.onload = aK;
		}
		document.body.appendChild(aG);
	};
	B.prototype.loadFeatures = function v(aF) {
		this.settings.enabledFeatures
				.forEach(function(aG) {
					if (aG !== "base"
							&& this.availableFeatures.indexOf(aG.toLowerCase()) === -1) {
						this.loadFeatureScript(aG, aF);
					}
				}.bind(this));
	};
	B.prototype.addEventHandler = function s(aG, aF) {
		if (window.embedded_svc && embedded_svc.utils) {
			embedded_svc.utils.addEventHandler(aG, aF);
		} else {
			if (!this.storedEventHandlers[aG]) {
				this.storedEventHandlers[aG] = [];
			}
			this.storedEventHandlers[aG].push(aF);
		}
	};
	B.prototype.setupMessageListener = function u() {
		window.addEventListener("message", function(aI) {
			var aK = aI.data;
			var aF = aI.origin.split(":")[1].replace("//", "");
			var aH;
			var aJ;
			var aG;
			if (aK && aK.method
					&& embedded_svc.isMessageFromSalesforceDomain(aF)) {
				if (aK.method === "session.onLoad"
						&& this.settings.iframeURL.indexOf(aF) === -1) {
					aJ = this.settings.iframeURL.split("/")[2];
					aG = aI.origin.split("/")[2];
					this.settings.iframeURL = this.settings.iframeURL.replace(
							aJ, aG);
				}
				aH = aK.method.split(".")[0].toLowerCase();
				if (this.availableFeatures.indexOf(aH) === -1) {
					if (!this.pendingMessages[aH]) {
						this.pendingMessages[aH] = [];
					}
					this.pendingMessages[aH].push({
						direction : "incoming",
						payload : aK
					});
				} else {
					this.handleMessage(aK);
				}
			}
		}.bind(this), false);
	};
	B.prototype.handleMessage = function H(aF) {
		if (this.messageHandlers[aF.method]) {
			this.messageHandlers[aF.method](aF.data);
		} else {
			this.log("Unregistered method " + aF.method + " received.");
		}
	};
	B.prototype.isMessageFromSalesforceDomain = function C(aG) {
		var aF;
		if (S() && aG === document.domain) {
			return true;
		}
		aF = function(aI, aH) {
			return aI.indexOf(aH, aI.length - aH.length) !== -1;
		};
		return ar.some(function(aH) {
			return aF(aG, aH);
		});
	};
	B.prototype.isCommunityDomain = function k(aF) {
		return aF.substr(-".force.com".length) === ".force.com";
	};
	B.prototype.isSamePageNavigation = function af() {
		var aF;
		var aG = document.domain;
		if (this.isCommunityDomain(document.domain)) {
			aG = aG + "/" + window.location.pathname.split("/")[1];
		}
		aF = aG.substr(-this.settings.storageDomain.length) === this.settings.storageDomain;
		return aF;
	};
	B.prototype.addDefaultSetting = function ao(aG, aF) {
		this.defaultSettings[aG] = aF;
	};
	B.prototype.onButtonStatusChange = function q() {
		var aF = document.querySelector(".embeddedServiceHelpButton button");
		var aG;
		if (embedded_svc.menu) {
			embedded_svc.menu.onAgentAvailabilityChange();
		}
		if (aF) {
			aG = aF.querySelector(".message");
			if (aG) {
				if (this.isButtonDisabled) {
					aF.onclick = function() {
					};
					aF.classList.remove("helpButtonEnabled");
					aF.classList.add("helpButtonDisabled");
					aG.innerHTML = this.settings.disabledMinimizedText;
				} else {
					aF.onclick = this.onHelpButtonClick.bind(this);
					aF.classList.remove("helpButtonDisabled");
					aF.classList.add("helpButtonEnabled");
					aG.innerHTML = this.settings.defaultMinimizedText;
				}
			}
		}
	};
	B.prototype.hideHelpButton = function ae() {
		var aF = document.querySelector("." + O);
		if (aF) {
			aF.style.display = "none";
		}
	};
	B.prototype.showHelpButton = function b() {
		var aF = document.querySelector("." + O);
		if (aF) {
			aF.style.display = "";
		}
	};
	B.prototype.setDefaultButtonText = function aj(aI, aH, aG, aF) {
		if (this.settings.entryFeature === aI) {
			this.settings.defaultMinimizedText = this.settings.defaultMinimizedText
					|| aH;
			this.settings.disabledMinimizedText = this.settings.disabledMinimizedText
					|| aG;
			this.settings.defaultAssistiveText = this.settings.defaultAssistiveText
					|| aF || "";
		}
	};
	B.prototype.setDefaultShowIcon = function ac(aG, aF) {
		if (this.settings.entryFeature === aG
				&& this.settings.showIcon === undefined) {
			this.settings.showIcon = aF;
		}
	};
	B.prototype.registerLinkAction = function x(aG, aF) {
		var aH = this.settings.linkAction;
		if (!this.validLinkActions[aG]) {
			this.validLinkActions[aG] = [];
		}
		if (this.validLinkActions[aG].indexOf(aF) === -1) {
			this.validLinkActions[aG].push(aF);
		}
		if (aH.feature && aH.name
				&& aH.feature.toLowerCase() === aG.toLowerCase()
				&& aH.name.toLowerCase() === aF.toLowerCase()) {
			aH.valid = true;
			aH.feature = aG;
			this.settings.entryFeature = aG;
		}
	};
	B.prototype.setLinkAction = function al(aG, aI, aH) {
		var aF = Object.keys(this.validLinkActions).filter(function(aJ) {
			return aJ.toLowerCase() === aG.toLowerCase();
		})[0];
		if (aF) {
			this.settings.linkAction.feature = aF;
			this.settings.linkAction.name = this.validLinkActions[aF]
					.filter(function(aJ) {
						return aJ.toLowerCase() === aI.toLowerCase();
					})[0];
			this.settings.linkAction.valid = this.settings.linkAction.name !== undefined;
			this.settings.linkActionParameters = aH;
		} else {
			this.settings.linkAction.valid = false;
		}
	};
	B.prototype.getLinkActionData = function g() {
		window.location.search.replace(/([a-zA-Z0-9._]+)=([^&\s]+)/g, function(
				aG, aH, aJ) {
			var aI = aH.toLowerCase();
			var aK;
			var aF;
			if (aI.indexOf("snapins.") === 0) {
				aF = aI.replace("snapins.", "");
				if (aF === "action") {
					aK = aJ.split(".");
					if (aK.length === 2) {
						this.settings.linkAction.feature = aK[0];
						this.settings.linkAction.name = aK[1];
					}
				} else {
					this.settings.linkActionParameters[aF.toLowerCase()] = aJ;
				}
			}
		}.bind(this));
	};
	B.prototype.requireAuthentication = function au() {
		var aF = document.createElement("script");
		var aH = document.createElement("style");
		var aG = document.querySelector(this.settings.loginTargetQuerySelector);
		this.isAuthenticationRequired = true;
		if (window.location.protocol !== "https:" && !this.settings.devMode) {
			this.settings.displayHelpButton = false;
			throw new Error("Snap-in authentication requires HTTPS.");
		}
		if (!this.settings.useCustomAuthentication) {
			if (!this.settings.loginClientId || !this.settings.loginRedirectURL
					|| !this.settings.loginTargetQuerySelector) {
				throw new Error(
						"Authentication in Snap-ins requires these valid settings params: loginClientId, loginRedirectURL, loginTargetQuerySelector.");
			}
			if (aG) {
				this.loginButtonPressed = false;
				aG.addEventListener("click", function() {
					this.loginButtonPressed = true;
				}.bind(this));
			} else {
				throw new Error(
						"loginTargetQuerySelector is not a valid DOM element.");
			}
			this.addMetaTag("salesforce-community",
					this.settings.communityEndpointURL);
			this
					.addMetaTag("salesforce-client-id",
							this.settings.loginClientId);
			this.addMetaTag("salesforce-redirect-uri",
					this.settings.loginRedirectURL);
			this.addMetaTag("salesforce-mode", "popup");
			this.addMetaTag("salesforce-target",
					this.settings.loginTargetQuerySelector);
			this.addMetaTag("salesforce-login-handler", L);
			this.addMetaTag("salesforce-logout-handler", t);
			embedded_svc.utils.addEventHandler("requireauth", function() {
				var aI = setInterval(function() {
					if (window.SFIDWidget) {
						clearInterval(aI);
						if (window.SFIDWidget.openid_response) {
							window[L]();
						} else {
							window.SFIDWidget.login();
						}
					}
				}, aB);
			});
			embedded_svc.utils.addEventHandler("autherror", function(aJ) {
				var aI;
				if (window.SFIDWidget) {
					this.loginButtonPressed = true;
					window.SFIDWidget.logout();
					aI = setInterval(function() {
						if (window.SFIDWidget.config) {
							clearInterval(aI);
							embedded_svc.utils.fireEvent("requireauth");
						}
					}.bind(this, aI), aB);
				}
			}.bind(this));
			window[L] = function() {
				var aJ = document
						.querySelector(this.settings.loginTargetQuerySelector);
				var aI = document.createElement("button");
				if (this.loginButtonPressed || this.componentInitInProgress) {
					aJ.innerHTML = "";
				}
				aI.className = "authenticationStart";
				aI.innerHTML = this.settings.authenticationStartLabel;
				aI.addEventListener("click", this.onHelpButtonClick.bind(this));
				aJ.appendChild(aI);
				this.auth.oauthToken = window.SFIDWidget.openid_response.access_token;
			}.bind(this);
			window[t] = function() {
				this.auth.oauthToken = undefined;
				window.SFIDWidget.init();
			}.bind(this);
			document.head.appendChild(aH);
			aH.sheet.insertRule(".sfid-logout { display: none; }", 0);
			aF.type = "text/javascript";
			aF.src = this.settings.communityEndpointURL
					+ "/servlet/servlet.loginwidgetcontroller?type=javascript_widget"
					+ (embedded_svc.settings.devMode ? "&min=false" : "");
			document.head.appendChild(aF);
		}
	};
	B.prototype.requireSLDS = function z() {
		var aG;
		var aF;
		var aH;
		this.settings.requireSLDS = true;
		if (this.settings.targetElement === document.body) {
			aG = document.createElement("div");
			aG.id = ad;
			document.body.appendChild(aG);
			this.settings.targetElement = aG;
		}
		this.settings.targetElement.classList.add(l);
		aF = document.createElement("link");
		aH = this.settings.gslbBaseURL ? this.settings.gslbBaseURL
				: this.settings.baseCoreURL;
		aF.href = aH + "/embeddedservice/" + this.settings.releaseVersion
				+ "/esw-slds" + (this.settings.devMode ? "" : ".min") + ".css";
		aF.type = "text/css";
		aF.rel = "stylesheet";
		document.getElementsByTagName("head")[0].appendChild(aF);
	};
	B.prototype.validateHeaderValue = function n(aF) {
		return /^[0-9a-zA-Z!#$%&'*+-.^_`|~" ]*$/g.test(aF);
	};
	B.prototype.isLanguageRtl = function K(aF) {
		if (!aF || aF.trim() === "") {
			return undefined;
		}
		switch (aF.substring(0, 2)) {
		case "ar":
		case "fa":
		case "he":
		case "iw":
		case "ji":
		case "ur":
		case "yi":
			return true;
		default:
			return false;
		}
	};
	B.prototype.isDesktop = function I() {
		return navigator.userAgent.indexOf("Mobi") === -1;
	};
	window.embedded_svc = new B();
	Object.getOwnPropertyNames(V).forEach(function(aF) {
		var aG = V[aF];
		if (aG === "object") {
			window.embedded_svc[aF] = {};
			Object.keys(aG).forEach(function(aH) {
				window.embedded_svc[aF][aH] = aG[aH];
			});
		} else {
			window.embedded_svc[aF] = aG;
		}
	});
})(window.embedded_svc || {});