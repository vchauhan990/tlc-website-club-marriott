var dynamicImagePath = "";
$(document).ready(
		function() {

			var pagenoSelected = $('#cm-hotel-category').val();
			var resourcePath = $("#noidea").attr("data-resourcePath");
			var pagePath = $("#noidea").attr("data-pagePath");
			var defaultImage = $("#hotelBanner").attr("data-default-src");

			var dataInput = {};
			var pageNo = $('.custom-combobox').val();

			var jsonList = JSON.parse($("#not-found-container").attr(
					"data-list"));

			var rawtemplate = document.getElementById("search").innerHTML;
			var compiledTemplate = Handlebars.compile(rawtemplate);

			populatingData(jsonList);

			$('.benefits-search-button').click(
					function(e) {

						/*var citySearchValue = $('#tags').val();*/
						console.log("citySearchValue" , citySearchValue);
						if (citySearchValue.length > 0) {

							dataInput.modeType = "city";
							dataInput.cityName = citySearchValue.split(',')[0];
							dataInput.resourcePath = resourcePath;
							dataInput.pagePath = pagePath;
							// bannerChangebasisCity(dataInput);

							$.ajax({
								type : "GET",
								url : "/bin/form/citySearch",
								data : dataInput, // serializes the form's elements.
								contentType : 'application/json; charset=UTF-8',
								success : function(dataResult) {
									var json = JSON.parse(dataResult); // show response from the php script.
									var datamessage = json.message;
									jsonList = datamessage;
									populatingData(datamessage)
								},
								failure : function(error) {
									console.log(error);

								}
							});
						}
					});
			$(document).on(
					'click',
					'.ui-menu-item',
					function() {
						let newData = $(this).children(".ui-menu-item-wrapper")
								.children().attr('data-placeholder');
						console.log("newData", newData);
						if (newData) {

							dataInput.cityName = newData;
							dataInput.resourcePath = resourcePath;
							dataInput.defaultImage = defaultImage;

							$.ajax({
								type : "GET",
								url : "/bin/form/dynamicBanner",
								data : dataInput, // serializes the form's elements.
								contentType : 'application/json',
								success : function(dataResult) {
									var json = JSON.parse(dataResult); // show response from the php script.
									var datamessage = json.message;
									dynamicImagePath = datamessage;
									$('#hotelBanner').attr("src",
											dynamicImagePath);

								},
								failure : function(error) {
									console.log(error);

								}
							});
						}
					});

			function populatingData(jsonList) {

				$('#paginationId').pagination(
						{
							dataSource : jsonList,
							pageSize : pagenoSelected,
							showPrevious : true,
							showNext : true,
							className : "Page navigation col-12",
							callback : function(data, pagination) {

								var ourGeneratedHTML = compiledTemplate(data);
								var cmtsContainer = document
										.getElementById("hotel-list-card");
								cmtsContainer.innerHTML = ourGeneratedHTML;

							}
						})
			}

			$('#cm-hotel-category').autocomplete({

				select : function(event, ui) {
					// here I need the input element that have triggered the event
					alert($(this).attr('placeholder'));
				}
			});
			$(document).on('click', '.ui-menu-item', function() {
				pagenoSelected = $('#cm-hotel-category').val();
				populatingData(jsonList);
			});

		});
