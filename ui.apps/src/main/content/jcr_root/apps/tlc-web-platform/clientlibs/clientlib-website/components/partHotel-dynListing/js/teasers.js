$(document).ready(function(){
	$(".cm-teasers-list-block").each(function(){
		var dotValue = "false";
		var owlNavValue = "false";		
		dotValue = $(this).children('.teaser-owl-carousel').attr("data-attr-dots");
		owlNavValue = $(this).children('.teaser-owl-carousel').attr("data-attr-nav");

        var isdotValue = (dotValue == 'true');
        var isTrueowlNavValue = (owlNavValue == 'true');
       
        if($(this).children('.teaser-owl-carousel').owlCarousel){
		
        	$(this).children('.teaser-owl-carousel').owlCarousel({
			margin : 30,
			nav : isTrueowlNavValue,
			items : 1,
			dots : isdotValue,
			responsive : {
				0 : {
					loop : true,
					margin : 0,
					items : 1
				},
			/*	579 : {
					center : false,
					items : 3,
					loop : false
				},*/
				768 : {
					center : false,
					items : 4,
					loop : false
				}
			}
		})
        
        }
	})
})