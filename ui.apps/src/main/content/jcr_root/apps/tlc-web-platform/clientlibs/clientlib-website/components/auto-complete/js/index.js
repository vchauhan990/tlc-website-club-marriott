 var citySelected = " ";
 var citySearchValue = " ";
var citySearchPlace = " " ;

 $(function() {
	 /*below line is added to reset the search input on returning back*/
	 $("#tags").val("");
     
	 jQuery.ui.autocomplete.prototype._resizeMenu = function() {
       var ul = this.menu.element;
       console.log("ul" , ul);
       ul.outerWidth(this.element.outerWidth());
     };   
     // function that structures each autocomplete item
   });
   var newcity = [];
   $(document).ready(function() {
	   
 var resourcePath = $("#nono").attr("data-resourceP");

     $.ajax({
       type: "GET",
       url: "/bin/form/hotelSearch",
       data: {
         modeType: "cities",
           cityPath : resourcePath
       },
      /* dataType:"json", */
       /* contentType: "application/json", */

       success: function(result) {
    	 var response = JSON.parse(result);
    	 function sortObject(o) {
 		    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
 		  }
    	 var res = sortObject(response.message);
         var keys = Object.keys(res);
        
         keys.forEach(key=>{
           newcity.push({'label':key,'value':key,'place':res[key]});
         });     
         } ,
        error: function(e){
        	console.log("error", e);
        }
     });
   
     if ($("#tags").length > 0) {
       $("#tags")
         .autocomplete({
           minLength: 0,
           source: newcity,
           focus: function(event, ui) {
             return false;
           },
           select : function (event, ui) {
        	   $(".cm-autocomplete-input").prop("disabled", true);
              citySelected = ui.item.label;
               citySearchValue = ui.item.place;

      		console.log("citySearchValue" ,citySearchValue );
           }
         })
         .focus(function() {
           $(this).autocomplete("search");
         })
         .autocomplete("instance")._renderItem = function(ul, item) {
         return $("<li>")
           .append("<div><b data-val=" + item.value + " " + "data-placeholder="+ item.place + ">" + item.label + "</b></div>")
           .appendTo(ul);
       };
     }
     
     // fucntion to display cross and make input uneditable
     $(".cm-autocomplete-input").on("autocompleteselect", function() {
       this.blur();
       this.disabled = false;
       $(".cm-autocomplete").addClass("active");
     });
    
     // Removes cross and reactivates inputs
     $(".cm-autocomplete-cross").on("click", function() {
    	 
         $(".cm-autocomplete-input").focus();
         $(".cm-autocomplete-input").val("");
         $(".cm-autocomplete-input").prop("disabled", false);
         $(".cm-autocomplete").removeClass("active");

         $('.cm-autocomplete-input').val('');
         $('.cm-autocomplete-input').focus();

         $('.cm-autocomplete-input').prop('disabled',false);
         $('.cm-autocomplete').removeClass('active');

         $(".autocomplete-dropdown-link").addClass('active');
         $('.cm-autocomplete').removeClass('active');
         $(this).parent().find('input').autocomplete("search");
     });
   
     $(".autocomplete-dropdown-link").on("click", function() {
    	
       if ($(this).hasClass("active")) {
         $(this).parent().find("input").autocomplete("close");
         $(this).removeClass("active");
       } else {
         $(this).parent().find("input").autocomplete("search");
         $(this).addClass("active");
       }
     });

   });