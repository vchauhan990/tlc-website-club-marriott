$(document).ready(function() {
					var dataInput = {};
					var resourcePath = $("#city-path").attr("data-cityPath");
					var pagePath = $("#city-path").attr("data-pageP");

					//var jsonList = JSON.parse($("#searchRes").attr("data-list"));

					var rawtemplate = document.getElementById("search").innerHTML;
					var compiledTemplate = Handlebars.compile(rawtemplate);

					function scrollToResults() {
						const pos = document
								.querySelector('.benefits-container').offsetTop;
						$('html, body').animate({
							scrollTop : pos
						}, 800);
					}

					var loadAllResults = JSON.parse($("#benefits-search-result").attr(
									"data-loadAllResults"));

					$(".benefits-search-button").click(function(e) {
								$("#dynlisting").hide();
								//var citySearchValue = $("#tags").val();

								if (citySearchValue.length > 0) {
									dataInput.modeType = "participatingSearch";
									dataInput.cityName = citySearchValue;

									dataInput.resourcePath = resourcePath;
									dataInput.pagePath = pagePath;

									$.ajax({
										type : "GET",
										url : "/bin/form/citySearch",
										data : dataInput, // serializes the
															// form's elements.
										contentType : "application/json",
										success : function(dataResult) {
											var json = JSON.parse(dataResult);
											var datamessage = json.message;
											
											if(datamessage == "" || datamessage== null){
												console.log("datamessage is null" ,datamessage );
												  
												   $("#hotel-list-card").hide();
												   $("#noResults").removeAttr("hidden");
												   console.log($("#hotel-list-card")
															.owlCarousel("destroy"));
											}else{
												
												$("#noResults").attr("hidden", "true");
												 $("#hotel-list-card").show();
												populatingData(datamessage);
												
											}
											
										
										},
										failure : function(error) {
											console.log(error);
										}
									});
								}
								scrollToResults();
							});

					function populatingData(jsonList) {
						var dotValue;
						var owlNavValue;
						dotValue = JSON.parse($("#benefits-search-result")
								.attr("data-attr-dots"));
						owlNavValue = JSON.parse($("#benefits-search-result")
								.attr("data-attr-nav"));

						var ourGeneratedHTML = compiledTemplate(jsonList);
						var cmtsContainer = document
								.getElementById("hotel-list-card");

						cmtsContainer.innerHTML = ourGeneratedHTML;

						console.log($("#hotel-list-card")
								.owlCarousel("destroy"));

						var owlLength = $('#hotel-list-card').find(
								".cm-owl-carousel-item").length;

						/* var centerStatus = (owlLength >= 3); */

						if (owlLength < 3) {
							$("#hotel-list-card").addClass('not-centered');
						} else {
							$("#hotel-list-card").removeClass('not-centered');
						}

						$("#hotel-list-card").owlCarousel({
							center : true,
							items : 2,
							loop : false,
							margin : 30,
							nav : owlNavValue,
							dots : dotValue,
							startPosition : 1,
							responsive : {
								0 : {
									center : true,
									items : 2,
									autoWidth : false,
									nav : false
								},
								/*
								 * 579 : { center : true, items:2, autoWidth:
								 * false, nav: false center : false, items : 3,
								 * loop : false, startPosition : 2, autoWidth:
								 * true },
								 */
								768 : {
									center : false,
									items : 3,
									loop : false,
									startPosition : 0,
									autoWidth : false,
									nav : owlNavValue
								},
								1240 : {
									center : false,
									items : 3,
									loop : false,
									startPosition : 0,
									autoWidth : false,
									nav : owlNavValue
								}
							}
						});

						console.log($("#hotel-list-card").owlCarousel());
					}
				});
