$(document).ready(function() {
    var count = 0;
    $(".questions-list").each(function() {
      var children = $(this).children("li");
      var index ;
      for (let i = 0; i < children.length; i++) {
          count++;
        const element = children[i];
        element.children[0].innerHTML = i + 1 + ".";
        element.id = "general-list-item-" + (count);
        
      }
     
    });
  
  
    $(".cm-accordion-link").on("click", function() {
     
      $(".cm-accordion-item:not(#" + this.parentElement.id + ")").find(".cm-accordion-content").css("height", "0px");
      $(".cm-accordion-item:not(#" + this.parentElement.id + ")").removeClass(
        "active"
      );
      var heightOfContent = $(this)
        .parent()
        .find(".cm-accordion-content p")
        .height() + 10 ;
       
      if (this.parentElement.classList.contains("active")) {
        $(this)
          .parent()
          .find(".cm-accordion-content")
          .css("height", "0px");
      } else {
        $(this)
          .parent()
          .find(".cm-accordion-content")
          .css("height", heightOfContent);
      }
      $(this)
        .parent()
        .toggleClass("active");
    });
  });
  