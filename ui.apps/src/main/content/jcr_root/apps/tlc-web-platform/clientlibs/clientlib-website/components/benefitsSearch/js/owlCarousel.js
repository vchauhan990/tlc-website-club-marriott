$(document).ready(function(){
	var dotValue = '';
	var owlNavValue = '';
	dotValue =   JSON.parse($('#benefits-search-result').attr("data-attr-dots"));
	owlNavValue = JSON.parse($('#benefits-search-result').attr("data-attr-nav"));
   
/*	isdotValue = (dotValue == 'true');
	isowlNavValue = (owlNavValue == 'true');*/
    
	var owlLength = $('.cm-owl-carousel').find(".cm-owl-carousel-item").length;
    /*var centerStatus = (owlLength >= 3);*/
    if(owlLength < 3){
		$(this).children('.cm-owl-carousel').addClass('not-centered');
	}else {
        $("#owl-carousel-id").removeClass('not-centered');
    }
   if( $('.cm-owl-carousel').owlCarousel){
   
	   $('.cm-owl-carousel').owlCarousel({
		   center: true,
		   items:2, 
		   loop:false,
		   margin:30,
		   nav: owlNavValue,
		   dots: dotValue,
		   startPosition: 0,
		   responsive:{
		    0: {
				center : true,
				items:2,
				autoWidth: false,
				nav: true
				},
			/*579 : {
				center : true,
				items:2,
				autoWidth: false,
				nav: false
				center : false,
				items : 3,
				loop : false,
				startPosition : 2,
				autoWidth: true
			},*/
			768 : {
				center : false,
				items : 3,
				loop : false,
				startPosition : 0,
				autoWidth: false,
				nav: owlNavValue
			},
			1240 : {
				center : false,
				items : 3,
				loop : false,
				startPosition : 0,
				autoWidth: false,
				nav: owlNavValue
			}
      }
  });
   }
  });