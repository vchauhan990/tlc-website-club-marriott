$(document).ready(function(){
	$(".benefits-carousel-container").each(function(){
		var dotValue;
		var owlNavValue;		
		dotValue = JSON.parse($(this).children('.cm-owl-carousel').attr("data-attr-dots"));
		owlNavValue = JSON.parse($(this).children('.cm-owl-carousel').attr("data-attr-nav"));
		var owlLength = $('.cm-owl-carousel').find(".cm-owl-carousel-item").length;
    	var centerStatus = (owlLength >= 3);
    	if(owlLength < 3){
    		$(this).children('.cm-owl-carousel').addClass('not-centered');
    	}
    
    	if($(this).children('.cm-owl-carousel').owlCarousel){
		
    		$(this).children('.cm-owl-carousel').owlCarousel({
			margin : 30,
			nav : owlNavValue,
			items : 2,
			dots : dotValue, 
            center : true,
            startPosition : 1,
			responsive : {
			    0: {
					center : true,
					items:2,
					autoWidth: false,
					nav: false
					},
				/*579 : {
					center : true,
					items:2,
					autoWidth: false,
					nav: false
					center : false,
					items : 3,
					loop : false,
					startPosition : 2,
					autoWidth: true
				},*/
				768 : {
					center : false,
					items : 3,
					loop : false,
					startPosition : 0,
					autoWidth: false,
					nav: false
				},
				1240 : {
					center : false,
					items : 3,
					loop : false,
					startPosition : 0,
					autoWidth: false,
					nav: owlNavValue
				}
			}
		})
		
    	}
	})
})