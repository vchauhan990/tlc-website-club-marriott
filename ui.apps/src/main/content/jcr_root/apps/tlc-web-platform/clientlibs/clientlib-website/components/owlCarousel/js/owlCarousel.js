$(document).ready(function(){
	$(".benefits-carousel-container").each(function(){
	
		var owlLength = $('.cm-owl-carousel').find(".cm-owl-carousel-item").length;
    	var centerStatus = (owlLength >= 3);
    	if(owlLength < 3){
    		$(this).children('.cm-owl-carousel').addClass('not-centered');
    	}else{
    		$(this).children('.cm-owl-carousel').removeClass('not-centered');
    	}
		$(this).children('.cm-owl-carousel').owlCarousel({
            center:true,
			margin : 30,
			nav : true,
			items : 1,
			dots : true,
			responsive : {
			    0: {
					center : true,
					items:2,
					autoWidth: false,
					nav: false
					},
				579 : {
					center : true,
					items:2,
					autoWidth: false,
					nav: false
					/*center : false,
					items : 3,
					loop : false,
					startPosition : 2,
					autoWidth: true*/
				},
				768 : {
					center : false,
					items : 3,
					loop : false,
					startPosition : 0,
					autoWidth: false,
					nav: false
				},
				1240 : {
					center : false,
					items : 3,
					loop : false,
					startPosition : 0,
					autoWidth: false,
					nav: true
				}
			}
		})
	})
})