$(document).ready(function() {
    var resourcePath = $("#uniqueExpId").attr("data-unique-path");
    var pagePath = $("#uniqueExpId").attr("data-path");

/*     expereince carousel new  
    $('.owl-carousel.owl-exp').owlCarousel({
    	  margin:10,
    	  loop:true,
    	  autoWidth:true,
    	  items:2,
    	  center:true,
    	  nav:true,
    	  smartSpeed: 1000
    	})
    expereince carousel new */
    	
    /*Handlebars Helper - if*/
    Handlebars.registerHelper("indexPosition", function(index) {
        if (index == 0) {
            return "active";
        } else {
            return "";
        }
    });
    /*Handlebars Helper - if*/

    $('.benefits-search-button').click(function(e) {

        $.ajax({
            url: "/bin/uniqueExperience?city=" + citySearchValue + "&resourcePath=" + resourcePath + "&pagePath=" + pagePath,
            type: "GET",
            contentType: "application/json",
            success: function(result) {
                var uniqueExperienceList = result;

                if (uniqueExperienceList.data.length == 1) {
                    $(".navigationButton ").hide();
                } else {
                    $(".navigationButton ").show();
                }
                if (uniqueExperienceList.data.length == 0) {

                    $(".uniqueExperiences").hide();
                } else {
                    $(".uniqueExperiences").show();
                    uniqueExperienceArray = uniqueExperienceList.data;
                    populatingData(uniqueExperienceArray);
                }
            },

            error: function() {
                console.log("fail");
            }
        });
    });

    function populatingData(jsonList) {

        var template = $("#handlebars-exp").html();
        var templateScript = Handlebars.compile(template);
        var html = templateScript(jsonList);

        $("#uniqueExpid").html(html);
    }
});