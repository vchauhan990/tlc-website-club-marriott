var jsonx;
var rawTemplate;
function createHTML(commentsData) {
    
	var compiledTemplate = Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(commentsData);
	var cmtsContainer = document.getElementById("page-list");
	cmtsContainer.innerHTML = ourGeneratedHTML;
}

$(document).ready(function(){

    var dotValue ;
	var owlNavValue ;	
    var filterValue = $("#tagFilterId").attr("tagFilterValue");
    
    dotValue = $('.teaser-owl-carousel').attr("data-attr-dots");
	owlNavValue = $('.teaser-owl-carousel').attr("data-attr-nav");
	 var isdotValue = (dotValue == 'true');
     var isTrueowlNavValue = (owlNavValue == 'true');
     
	if(filterValue=="true"){
		rawTemplate = document.getElementById("handlebars-teaser").innerHTML;
        jsonx = JSON.parse($("#listingCategory").attr('data-attr-pageList'));
       
            var tagVal = $("#listingCategory").attr("data-attr-tag");
            filterData(tagVal);
    }
   
	$('.teaser-owl-carousel').owlCarousel({
			  margin:30,
			  nav:isTrueowlNavValue,
			  items:1,
			  dots:isdotValue,
			  responsive:{
			      0:{
			          
			          loop:true,
			          margin:0,
			          items:1
			      },
			      768:{
			        center: false,
			        items:3,
			        loop:false
			      },
			      1000:{
			          center: false,
			          items:3,
			          loop:false
			      }
			  }
			})
});


function filterData(keyz) {
    var newjsonObj = [];
	for ( var key in jsonx) {
		var obj = jsonx[key];
		var check = jQuery.inArray(keyz, obj["tags"]);
		if (check != -1) {
			newjsonObj.push(obj);
		}
    }
    createHTML(newjsonObj);
}