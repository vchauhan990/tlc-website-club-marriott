$(document).ready(function(){
    var offersEventPath = $("#teaserfeatureid").attr("offerEventPath");
    var pagePath = $("#teaserfeatureid").attr("pagePath");
    var cityPath = $("#teaserfeatureid").attr("cityPath");
    var featuredOffersAtText = $(".cityN").attr("headingText");
    /*var offerTeaserCta = $("#teaserfeatureid").attr("data-offer-teaser-cta");*/
    $('.benefits-search-button').click(function(e) {
  
    var cityName = citySelected;
   
    if(featuredOffersAtText){
    	$(".cityN").text(featuredOffersAtText + " " + cityName);
    	}
    $(".cityN").css("text-transform","capitalize");
   
    $.ajax({
        url: "/bin/dynamicTeaserCarousel?city="+citySearchValue+"&offersEventPath="+offersEventPath+"&pagePath="+pagePath+"&cityPath="+cityPath,
        type: "GET",
          contentType: "application/json",
          success: function(result) {
           var featuredOffersList = JSON.parse(result);
           if(featuredOffersList.data.length == 0){
            
           $(".offersEventsContainer").hide();
        }else{
          $(".offersEventsContainer").show();
          var featuredOffersArray = featuredOffersList.data;
           populatingData(featuredOffersArray);
           
        /*   $(".cm-teaser-link").text(offerTeaserCta);*/
        }
           },
          error: function() {
            console.log("fail");
          }
        });
  });
  
  function populatingData(jsonList) {

      var template = $("#handlebars-featuredOffers").html();
      var templateScript = Handlebars.compile(template); 
      var html = templateScript(jsonList);
  
     $("#featuredOffersid").html(html);
     $("#featuredOffersHeading").show();
     
     console.log($('#featuredOffersid').owlCarousel('destroy'));
  
     $("#featuredOffersid").owlCarousel({
          margin : 30,
          nav : false,
          items : 1,
          dots : true,
          responsive : {
              0 : {
                  // center: true,
                  loop : true,
                  margin : 0,
                  items : 1
              },
              579 : {
                  center : false,
                  items : 3,
                  loop : false
              },
              1000 : {
                  center : false,
                  items : 3,
                  loop : false
              }
          }
      })

      $("#featuredOffersid").removeClass("owl-hidden");
  }
  });