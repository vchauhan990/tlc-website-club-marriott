	$("#noResults").hide();
$(document).ready(function() {

	var dotValue = '';
    var owlNavValue = '';
/*  	dotValue =   $('#benefits-search-result').attr("data-attr-dots");
    owlNavValue = $('#benefits-search-result').attr("data-attr-nav");
    
    isdotValue = (dotValue == 'true');
  isowlNavValue = (owlNavValue == 'true');*/
  
  var resourcePath = $("#uniquePageId").attr("data-signature-path");
  var pagePath = $("#uniquePageId").attr("data-path");

  var owlCarouselArray = [];
  var signatureCollectionPath = $("#owl-carousel-id").attr("signatureCollectionPath");
  var pagePath = $("#owl-carousel-id").attr("pagePath");
  $('.benefits-search-button').click(function(e) {

      $.ajax({
          url: "/bin/owlCarousel?city=" + citySearchValue + "&resourcePath=" + signatureCollectionPath + "&pagePath=" + pagePath,
          type: "GET",
          contentType: "application/json",
          success: function(result) {
              var owlCarouselList = result;

              if (owlCarouselList.data.length == 0) {

                  $(".owlCarouselConatiner").hide();
                  $("#noResults").show();
              } else {
                  $(".owlCarouselConatiner").show();
                  owlCarouselArray = owlCarouselList.data;
                  populatingData(owlCarouselArray);
                 
              }
          },

          error: function() {
              console.log("fail");
          }
      });

  });

  function populatingData(jsonList) {

      var template = $("#handlebars-owl").html();
      var templateScript = Handlebars.compile(template);
      var html = templateScript(jsonList);

      $("#owl-carousel-id").html(html);
      console.log($('#owl-carousel-id').owlCarousel('destroy'));
    
/*       console.log("searchowl carousel- dot/nav"  +  dotValue + owlNavValue)*/
      var owlLength = $('#owl-carousel-id').find(".cm-owl-carousel-item").length;
      var centerStatus = (owlLength >= 3);
      if (owlLength < 3) {
          $("#owl-carousel-id").addClass('not-centered');
      } else {
          $("#owl-carousel-id").removeClass('not-centered');
      }
      $('#owl-carousel-id').owlCarousel({
          center: true,
          items: 2,
          loop: false,
          margin: 30,
          nav: true,
          dots: true,
          startPosition: 1,
          responsive: {
              0: {
                  center: true,
                  items: 2,
                  autoWidth: false,
                  nav: true
              },
              /*579 : {
                  center : true,
                  items:2,
                  autoWidth: false,
                  nav: false
                  center : false,
                  items : 3,
                  loop : false,
                  startPosition : 2,
                  autoWidth: true
              },*/
              768: {
                  center: false,
                  items: 3,
                  loop: false,
                  startPosition: 0,
                  autoWidth: false,
                  nav: false
              },
              1240: {
                  center: false,
                  items: 3,
                  loop: false,
                  startPosition: 0,
                  autoWidth: false,
                  nav: true
              }
          }
      });
      $("#owl-carousel-id").removeClass("owl-hidden")
      console.log($('#owl-carousel-id').owlCarousel());
  }
});