(function (document, $, Coral) {
	"use strict";    
	var type, defaultMode;
	$(document).on("foundation-contentloaded", function(e) {

		/**
		 * Initializing Variables and functions on Load time (Dialog-Ready Event)
		 */  
		Coral.commons.ready(function (component) {
			defaultMode = $(".btnType").val();
			showHide(defaultMode);
        });

		/**
		 *	Change Method for the View Type Drop-down
		 */
		$('.btnType').on('change', function(event) {
			type = $(this).val();
			if(type === "text") {
				$(".ImageCtaText").parent().hide();
                $(".imgBtnItems").parent().hide(); 
                $(".textBtnItems").parent().show();
			} else if(type === "link") {
                $(".ImageCtaText").parent().hide();
				$(".textBtnItems").parent().show();
                $(".imgBtnItems").parent().hide();
            } else {
				  $(".textBtnItems").parent().hide();
				$(".ImageCtaText").parent().show();
                $(".imgBtnItems").parent().show();
            }
		});

        function showHide(defaultMode) {
			if(defaultMode === "text") {
				$(".ImageCtaText").parent().hide();
                $(".imgBtnItems").parent().hide(); 
                $(".textBtnItems").parent().show();
			} else if(type === "link") {
                $(".ImageCtaText").parent().hide();
				$(".textBtnItems").parent().show();
                $(".imgBtnItems").parent().hide();
            } else {
				  $(".textBtnItems").parent().hide();
				$(".ImageCtaText").parent().show();
                $(".imgBtnItems").parent().show();
            }
        } 

	});
})(document, Granite.$, Coral);