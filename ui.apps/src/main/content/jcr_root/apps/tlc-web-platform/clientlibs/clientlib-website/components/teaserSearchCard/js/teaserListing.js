var jsonx;
var rawTemplate;
function createHTML(commentsData) {

	var compiledTemplate = Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(commentsData);
	var cmtsContainer = document.getElementById("page-list");
	cmtsContainer.innerHTML = ourGeneratedHTML;
}

$(document).ready(function(){
	
	$('.teaser-owl-carousel').owlCarousel({
		  margin:30,
		  nav:true,
		  items:1,
		  dots:false,
		  responsive:{
		      0:{
	              loop:true,
		          margin:0,
		          items:1
		      },
		      579:{
		        center: false,
		        items:3,
		        loop:false
		      },
		      1000:{
		          center: false,
		          items:3,
		          loop:false
		      }
		  }
		})

	rawTemplate = document.getElementById("handlebars-demo").innerHTML;
	jsonx = JSON.parse($("#listingCategory").attr('data-attr-pageList'));
	
        var tagVal = $("#listingCategory").attr("data-attr-tag");
		filterData(tagVal);
	



});


function filterData(keyz) {
    var newjsonObj = [];
	for ( var key in jsonx) {
		var obj = jsonx[key];
		var check = jQuery.inArray(keyz, obj["tags"]);
		if (check != -1) {
			newjsonObj.push(obj);
		}
    }
    createHTML(newjsonObj);
}