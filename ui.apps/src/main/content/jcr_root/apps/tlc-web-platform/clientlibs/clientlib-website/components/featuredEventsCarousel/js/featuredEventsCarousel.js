$(document).ready(function(){
  var offersEventPath = $("#featuredEventsid").attr("offersEventPath");
  var pagePath = $("#featuredEventsid").attr("pagePath");
 
   /*Handlebars Helper - if*/
   Handlebars.registerHelper("indexPosition", function(index) {
      if (index == 0) {
        return "active";
      }else{
          return "";
      }
    });
    /*Handlebars Helper - if*/

  $('.benefits-search-button').click(function(e) {

   $.ajax({
      
     url: "/bin/featuredEvents?city="+citySearchValue+"&resourcePath="+offersEventPath+"&pagePath="+pagePath,
     type: "GET",
       contentType: "application/json",
       success: function(result) {
        var featuredEventsList = JSON.parse(result);
       
       if(featuredEventsList.data.length == 1){
          $(".navButton ").hide();
        }else{
          $(".navButton ").show();
        }
        if(featuredEventsList.data.length == 0){
            
           $(".featuredEvents-teaser").hide();
        }else{
          $(".featuredEvents-teaser").show();
          featuredEventsArray = featuredEventsList.data;
        populatingData(featuredEventsArray);
        }
           
        },
   
       error: function() {
         console.log("fail");
       }
     });

   
});

function populatingData(jsonList) {


   var template = $("#handlebars-featuredEvents").html();
   var templateScript = Handlebars.compile(template); 
   var html = templateScript(jsonList);


  $("#featuredEventsid").html(html);
  


}


})

