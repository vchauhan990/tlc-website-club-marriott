$(document).ready(function(){
  var bannerType = $(".cm-carousel").attr('data-bannerType');
     
  for(var i = 0 ; i < $('.cm-carousel .carousel').length; i++){
    $('.cm-carousel .carousel')[i].id = 'cm-carousel' + i;
    $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-item')[0].classList.add('active')

    if($('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-item').length == 1){
      $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-control-prev')[0].classList.add('is-hidden')
      $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-control-next')[0].classList.add('is-hidden')
      $('#'+ $('.cm-carousel .carousel')[i].id + ' .carousel-indicators')[0].classList.add('is-hidden')

    }else{ 
   	 if(bannerType == 'cm-type-1' ){
         generateDots('#'+ $('.cm-carousel .carousel')[i].id);
	 }
      generateArrows('#'+ $('.cm-carousel .carousel')[i].id);
    }
  }
})

function generateDots(id) {
 
  for (let i = 0; i < $(id + " .carousel-item").length; i++) {
    if(i == 0 ){
      $(id + ' .carousel-indicators').append('<li data-target="' + id + '" data-slide-to="'+ i + '" class="round active"></li>')
    }else{
      $(id + ' .carousel-indicators').append('<li data-target="' + id + '" data-slide-to="'+ i + '" class="round"></li>')
    }
  }
  // carousel-indicators
}
function generateArrows(id){
$(id).parent().find('.carousel-control-next').attr('href',id)
$(id).parent().find('.carousel-control-prev').attr('href',id)
 /* $(id + ' .carousel-control-next').attr('href',id);
  $(id + ' .carousel-control-prev').attr('href',id)*/
}