$(document).ready(function(){
$('.benefits-search-button').on('click', function(e,el){
	const input = $(this).parent().parent().find('input').val();
	let event = new CustomEvent('do-search',{"detail": input});
	window.dispatchEvent(event);
	})
		  
	window.addEventListener('do-search', function (e) {

	      $('.search-results-section-active').removeClass('search-results-section-active');
	      $('#total-results').addClass('search-results-section-active');
	      $('#city-offers').addClass('search-results-section-active');

	      $('#total-results .hotels-listed .hotel-column').not(":first").remove();
	      $('#result-title').html('Featured Hotels');
	      $('#selected-city').html(e.detail.split(',')[0]);
	      scrollToResults();

	  }, false);
	})

	function scrollToResults() {
	  const pos = document.querySelector('.search-results-container').offsetTop;
	  $('html, body').animate({
	    scrollTop: pos
	}, 800);

}