var jsonx;
var rawTemplate;
function createHTML(commentsData) {

	var compiledTemplate = Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(commentsData);
	var cmtsContainer = document.getElementById("page-list");
	cmtsContainer.innerHTML = ourGeneratedHTML;
}

$(document).ready(function() {
	rawTemplate = document.getElementById("handlebars-demo").innerHTML;
	jsonx = JSON.parse($("#listingCategory").attr('data-attr-pageList'));
	createHTML(jsonx);

	$('.blog-category').on('click', function() {
		$('.blog-category').removeClass('blog-category-active');
		$(this).addClass('blog-category-active');
        var tagVal= $(this).attr("data-attr-val");
        if (tagVal == 'tlc:blog/all'){
        createHTML(jsonx);
        }else{
		filterData(tagVal);
        }
	})
})

function filterData(keyz) {
    var newjsonObj = [];
	for ( var key in jsonx) {
		var obj = jsonx[key];
		var check = jQuery.inArray(keyz, obj["tags"]);
		if (check != -1) {
			newjsonObj.push(obj);
		}
    }
    createHTML(newjsonObj);
}